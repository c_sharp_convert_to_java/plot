
//===============================================
//Импортирование библиотек:
//===============================================

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent; // импортирование библиотеки для обработки событий компонентов JButton и JTextField
import java.awt.event.ActionListener; // импортирование библиотеки для реагирования на события компонентов
import java.awt.event.MouseEvent; // импортирование библиотеки для работы с событиями мыши
import java.awt.event.MouseListener; // импортирование библиотеки для прослушивания событий мыши
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton; // импортирование библиотеки для работы с компонентом JButton
import javax.swing.JComboBox; // импортирование библиотеки для работы с компонентом JComboBox
import javax.swing.JFrame; // импортирование библиотеки для работы с компонентом JFrame
import javax.swing.JOptionPane;
import javax.swing.JPanel; // импортирование библиотеки для работы с компонентом JPanel
import javax.swing.JTextField; // импортирование библиотеки для работы с компонентом JTextField
import javax.swing.SwingConstants; // импортирование библиотеки для расположения и ориентации компонентов на экране

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.DefaultXYDataset;




/*
 * Класс окна ввода пользовательской функции
 */

public class UserFunctionJFrame_Create {
	
	private static JFrame inputUserFunction_JFrame;
	private static JButton cancelButton_JButton;
	private static JButton okButton_JButton;
	private static JPanel functionNamePanel_JPanel;
	private static JTextField functionNameTextField_JTextField;
	private static String userFunction = "";

	public static JFrame getInputUserFunction_JFrame() {
		return inputUserFunction_JFrame;
	}
	
	public UserFunctionJFrame_Create() {
		initialize();
	}
	
	public void runBlockMainFrame() {
		blockMainFrame();
	}
	
	public static String getUserFunction() {
		return userFunction;
	}
	
	
	private void initialize() {
		inputUserFunction_JFrame = new JFrame(); // инициализация элемента showDialog типа
		inputUserFunction_JFrame.setTitle("Пользовательская функция"); // задание заголовка окна
		inputUserFunction_JFrame.setBounds(400,200,500,125); // установка расположения и размеров окна
		inputUserFunction_JFrame.setVisible(true);
		inputUserFunction_JFrame.setEnabled(true); // установка "активации" окна
		//inputUserFunction_JFrame.setAlwaysOnTop(true); // установка расположения "всегда поверх остальных окон"
		inputUserFunction_JFrame.addWindowListener(Handlers.SetHandlerJFrameUserFunctionExitButton()); // установка обработчика событий окна
		//inputUserFunction_JFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		inputUserFunction_JFrame.setResizable(false);
		inputUserFunction_JFrame.setFocusable(true);
		inputUserFunction_JFrame.setAutoRequestFocus(true);
		inputUserFunction_JFrame.setFocusableWindowState(true);
		inputUserFunction_JFrame.addWindowListener(HandlerUserFunctionFrameSetFocus);
		//
		// GridBagLayout "режим размещения элементов"
		//
		GridBagLayout gbc_user_function = new GridBagLayout(); // инициализация режима размещения элементов Layout Manager
		gbc_user_function.columnWidths = new int[]{50, 50, 50, 50, 50, 50, 50, 50, 50}; // задание количества и ширины столбцов
		gbc_user_function.rowHeights = new int[]{25,25,25,25}; // задание количества и высоты строк
		gbc_user_function.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}; // задание параметра автоматического растяжения для каждого столбца
		gbc_user_function.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0}; // задание параметра автоматического растяжения для каждой строки
		inputUserFunction_JFrame.getContentPane().setLayout(gbc_user_function); // установка Layout формы как GridBagLayout
		//
		// JPanel "имя файла"
		//
		functionNamePanel_JPanel = new JPanel();
		GridBagConstraints gbc_file_name_panel = new GridBagConstraints();
		gbc_file_name_panel.insets = new Insets(0, 6, 0, 2);
		gbc_file_name_panel.fill = GridBagConstraints.BOTH;
		gbc_file_name_panel.gridx = 0;
		gbc_file_name_panel.gridy = 1;
		gbc_file_name_panel.gridwidth = 9;
		inputUserFunction_JFrame.getContentPane().add(functionNamePanel_JPanel, gbc_file_name_panel);
		//
		// JTextField "имя файла"
		//
		functionNameTextField_JTextField = new JTextField("Введите функцию");
		functionNameTextField_JTextField.setFont(new Font("Lucida Grande", Font.PLAIN, 12)); // установка шрифта, типа шрифта и его размера
		functionNameTextField_JTextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
		// Создание ограничений для поля:
		GridBagConstraints gbc_file_name = new GridBagConstraints(); // переменная для хранения ограничений
		gbc_file_name.insets = new Insets(0, 6, 0, 2); // установка отступов
		gbc_file_name.fill = GridBagConstraints.BOTH;
		//gbc_file_name.anchor = GridBagConstraints.WEST; // установка типа заполнения элементом ячейки
		gbc_file_name.gridx = 0; // установка координаты X, занимаемой ячейки
		gbc_file_name.gridy = 1; // установка координаты Y, занимаемой ячейки
		gbc_file_name.gridheight = 2;
		gbc_file_name.gridwidth = 9;
		functionNameTextField_JTextField.setToolTipText("введите функцию\n"); // установка сообщения, выводящегося при наведении на элемент
		functionNameTextField_JTextField.setColumns(43); // установка количества колонок в элементе
		functionNameTextField_JTextField.addMouseListener(new mouseClickedTextField());
		functionNamePanel_JPanel.add(functionNameTextField_JTextField, gbc_file_name); // добавление элемента с его ограничениями на форму
		
		//
		// JButton "cancel"
		//
		cancelButton_JButton = new JButton("Cancel");
		cancelButton_JButton.setFocusable(false);
		cancelButton_JButton.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
		// Создание ограничений для кнопки:
		GridBagConstraints gbc_cancel_button = new GridBagConstraints();
		gbc_cancel_button.anchor = GridBagConstraints.CENTER;
		gbc_cancel_button.fill = GridBagConstraints.BOTH;
		gbc_cancel_button.insets = new Insets(0, 0, 20, 0);
		gbc_cancel_button.gridx = 5;
		gbc_cancel_button.gridy = 3;
		gbc_cancel_button.gridwidth = 2;
		cancelButton_JButton.setToolTipText("нажмите, чтобы отменить операцию сохранения\n"); // установка сообщения, 
																			// выводящегося при 
																			// наведении на элемент
		cancelButton_JButton.addActionListener(new cancelButton());
		cancelButton_JButton.addMouseListener(new mouseClickedCancelButton());
		inputUserFunction_JFrame.getContentPane().add(cancelButton_JButton, gbc_cancel_button);
		//
		// JButton "ок"
		//
		okButton_JButton = new JButton("ОК");
		okButton_JButton.setFocusable(false);
		okButton_JButton.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
		// Создание ограничений для кнопки:
		GridBagConstraints gbc_ok_button = new GridBagConstraints();
		gbc_ok_button.anchor = GridBagConstraints.CENTER;
		gbc_ok_button.fill = GridBagConstraints.BOTH;
		gbc_ok_button.insets = new Insets(0, 0, 20, 3);
		gbc_ok_button.gridx = 7;
		gbc_ok_button.gridy = 3;
		gbc_ok_button.gridwidth = 2;
		okButton_JButton.setToolTipText("нажмите, чтобы сохранить файл\n"); // установка сообщения, 
																			// выводящегося при 
																			// наведении на элемент
		okButton_JButton.addActionListener(new okButton());
		okButton_JButton.addMouseListener(new mouseClickedOkButton());
		inputUserFunction_JFrame.getContentPane().add(okButton_JButton, gbc_ok_button);
	
	}
	
	private void blockMainFrame() {
		//frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
		//frame.getRootPane().setFocusable(false);
		//frame.getRootPane().setEnabled(false);
		//frame.getContentPane().setFocusable(false);
		//frame.getContentPane().setEnabled(false);
		Window_GUI.getFrame_JFrame().setEnabled(false);
		//frame.addMouseListener(new CustomListener());
		//Window_GUI.getFrame_JFrame().addMouseMotionListener(Window_GUI.getMyMove());
		Window_GUI.getFrame_JFrame().setFocusableWindowState(false);
		//frame.setUndecorated(true);
	}

	// Обработчик кнопки "Ок":											// вернуть полученное имя файла и расширение
		private static class okButton implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent event) {
				
				if(functionNameTextField_JTextField.getText().isEmpty() 
						|| functionNameTextField_JTextField.getText().equals("Введите функцию")) {
					String message  = "Внимание!\nПроверьте заполненность поля.";
					JOptionPane.showMessageDialog(inputUserFunction_JFrame, message, "Ошибка", JOptionPane.ERROR_MESSAGE);

				}
				else {
					userFunction = functionNameTextField_JTextField.getText();
					Window_GUI.getFrame_JFrame().setEnabled(true);
					Window_GUI.getFrame_JFrame().setFocusable(true);
					Window_GUI.getFrame_JFrame().setFocusableWindowState(true); 
					inputUserFunction_JFrame.dispose();
				}
				
			}
		} 
		
		// Обработчик кнопки "Cancel":											// закрыть окно
		private static class cancelButton implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent event) {
				Window_GUI.getFrame_JFrame().setEnabled(true);
				Window_GUI.getFrame_JFrame().setFocusable(true);
				Window_GUI.getFrame_JFrame().setFocusableWindowState(true); 
				inputUserFunction_JFrame.dispose();
				
			}
		} 
		
		private static class mouseClickedOkButton implements MouseListener {
			@Override 
	        public void mouseClicked(MouseEvent e) {
				okButton_JButton.setSelected(false);
	        }

	        public void mouseEntered(MouseEvent e) {
	        	okButton_JButton.setSelected(false);
	        }

	        public void mouseExited(MouseEvent e) {
	        	okButton_JButton.setSelected(false);
	        }

	        public void mousePressed(MouseEvent e) {
	        	okButton_JButton.setSelected(true);
	        }

	        public void mouseReleased(MouseEvent e) {
	        	okButton_JButton.setSelected(false);
	        }
	   }
		private static class mouseClickedCancelButton implements MouseListener {
			@Override 
	        public void mouseClicked(MouseEvent e) {
				cancelButton_JButton.setSelected(false);
	        }

	        public void mouseEntered(MouseEvent e) {
	        	cancelButton_JButton.setSelected(false);
	        }

	        public void mouseExited(MouseEvent e) {
	        	cancelButton_JButton.setSelected(false);
	        }

	        public void mousePressed(MouseEvent e) {
	        	cancelButton_JButton.setSelected(true);
	        }

	        public void mouseReleased(MouseEvent e) {
	        	cancelButton_JButton.setSelected(false);
	        }
	   }
		
		private static class mouseClickedTextField implements MouseListener {
			@Override 
	        public void mouseClicked(MouseEvent e) {
				functionNameTextField_JTextField.setText(null);
	        	inputUserFunction_JFrame.addMouseListener(new mouseClickedInputUserForm());
	        }

	        public void mouseEntered(MouseEvent e) {
	        	functionNameTextField_JTextField.setFocusable(true);
	        }

	        public void mouseExited(MouseEvent e) {
	        	
	        }

	        public void mousePressed(MouseEvent e) {
	        	functionNameTextField_JTextField.setText(null);
	        	inputUserFunction_JFrame.addMouseListener(new mouseClickedInputUserForm());
	        }

	        public void mouseReleased(MouseEvent e) {
	        	
	        }
	   }
		
		private static class mouseClickedInputUserForm implements MouseListener {
			 
	        public void mouseClicked(MouseEvent e) {
	        	if(functionNameTextField_JTextField.getText().isEmpty()) {
	        		functionNameTextField_JTextField.setText("Введите функцию");
	        	}
	        	functionNameTextField_JTextField.setFocusable(false);
	        }

	        public void mouseEntered(MouseEvent e) {

	        }

	        public void mouseExited(MouseEvent e) {

	        }

	        public void mousePressed(MouseEvent e) {
	        	if(functionNameTextField_JTextField.getText().isEmpty()) {
	        		functionNameTextField_JTextField.setText("Введите функцию");
	        	}
	        	functionNameTextField_JTextField.setFocusable(false);
	        	
	        }

	        public void mouseReleased(MouseEvent e) {

	        }
	   }	
		
		private static WindowAdapter HandlerUserFunctionFrameSetFocus = new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent event) {
				inputUserFunction_JFrame.setVisible(true);
				inputUserFunction_JFrame.requestFocusInWindow();
			}
		};
}
