import java.util.Timer;
import java.util.TimerTask;

public class Animation {
static Timer timer;
public static void startAnimation(FunctionData data)
    {
	    timer = new Timer();
	    timer.schedule(new TimerTask() {
	    @Override
	    public void run() {
        int t1 = Integer.parseInt(Window_GUI.getTimeLeft_TextField());
        int t2 = Integer.parseInt(Window_GUI.getTimeRight_TextField());
        t1++;
        t2++;
        Window_GUI.setTimeLeft_TextField(Integer.toString(t1));
        Window_GUI.setTimeRight_TextField(Integer.toString(t2));
        Calc.process(data);
        data.updateTime();
	    }
        }, 150,150);
    }
public static void stopAnimation()
	{
		timer.cancel();
		timer.purge();
	}
}
