import javax.swing.JOptionPane;

/* Класс, использующийся для вывода окна ошибки.
 * В классе есть основные коды ошибок.
 * При создании обработчиков ошибок в блоке catch перед выполнением соответствующих ошибок разработчики должны вызвать соответствуюшее сообщение.
 * В случае необходимости возможно использование перегрузки метода с параметром String для вывода сообщения.
 * Использование кодов ошибок рекомендуется для обработки исключений и ошибок, вызванных действиями пользователя ПО.
 * Использование сообщений рекомендуется для обработки непредвиденных результатов работы программы.
 * 
 * При вызове ошибки в результате возврата методом значения FALSE необходимо вызывать окно непосредственно в этом методе перед выходом.
 */
public class CustomMessage {
	static void errorMessage (int code) {
		String message;
		switch (code) {
		
		case 1: 
			message = "Все поля должны быть заполнены числами.";
			break;
		case 2:
			message = "Ошибка ввода/вывода при создании файла отчета";
			break;
		case 3:
			message = "Ошибка ввода/вывода при записи отчета";
			break;
		case 4:
			message = "Ошибка при вычислении пользовательской функции";
			break;
		default:
			message = "Неизвестная ошибка";
		}
		
		JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
	}
	
	static void errorMessage(String message) {
		JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
	}
	
	static boolean yesNoMessage(String message) {
		int result = JOptionPane.showConfirmDialog(null, message,"Ошибка", JOptionPane.YES_NO_OPTION);
		if (result == JOptionPane.YES_OPTION)
			return true;
		return false;
	}
}
