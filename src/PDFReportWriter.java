import java.io.File;
import java.io.IOException;

import org.jfree.chart.ChartUtilities;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.TextAlignment;


public class PDFReportWriter {
	static boolean writeReportPDF(File[] files, FunctionData function) {
		try {
			PdfWriter writer = new PdfWriter(files[0]);
			PdfDocument pdfDoc = new PdfDocument(writer);
			pdfDoc.addNewPage(); 
			Document document = new Document(pdfDoc); 
			PdfFont font = PdfFontFactory.createFont("fonts/7454.ttf", "CP1251", true);
			Paragraph pr = new Paragraph ("Отчет сгенерирован: " + CurrentTime.getCurrentTime()).setFont(font).setFontSize(16).setBold().setItalic();
			Paragraph pr1 = new Paragraph ("Информация о функции:").setFont(font).setFontSize(14);
			Paragraph pr2_1 = new Paragraph().setFont(font).setFontSize(14).setUnderline();Paragraph pr2 = new Paragraph().setFont(font).setFontSize(14).setUnderline();
			
			if (function.getCheckMethod())
				pr2.add("Модель следования за лидером");
			else 
				pr2.add("Модель пастуха");
			if (function.getFunc() != null) {
				 pr2_1.add("Пользовательская функция: " + function.getFunc());
			}
			Paragraph pr3 = new Paragraph ("Число частиц").setFont(font).setFontSize(14);
			float [][] columnWidths = {{150F}, {150F, 150F}, {150F, 150F, 150F}};
			Table table = new Table(columnWidths[0]); 
			table.addCell("N"); 
			table.addCell(Double.toString(function.getN()));
			
			Paragraph pr4 = new Paragraph("Начальные условия").setFont(font).setFontSize(14);
			Table table2 = new Table(columnWidths[1]);
			table2.addCell("t0"); 
			table2.addCell("y0"); 
			
			table2.addCell(Double.toString(function.getT0()));
			table2.addCell(Double.toString(function.getY0()));

			Paragraph pr5 = new Paragraph("Ограничения").setFont(font).setFontSize(14);
			Table table3 = new Table(columnWidths[2]);
			table3.addCell("M1"); 
			table3.addCell("M2"); 
			table3.addCell("M3");
			
			table3.addCell(Double.toString(function.getM1()));
			table3.addCell(Double.toString(function.getM2()));
			table3.addCell(Double.toString(function.getM3()));
					
			Paragraph pr6 = new Paragraph("Ограничения").setFont(font).setFontSize(14);
			Table table4 = new Table(columnWidths[2]);
			table4.addCell("C0"); 
			table4.addCell("C1"); 
			table4.addCell("C2");
			
			table4.addCell(Double.toString(function.getC0()));
			table4.addCell(Double.toString(function.getC1()));
			table4.addCell(Double.toString(function.getC2()));
			
			ChartUtilities.saveChartAsPNG(files[1], function.getPanel_1().getChart(), 600, 200);
			ChartUtilities.saveChartAsPNG(files[2], function.getPanel_2().getChart(), 600, 200);
			ChartUtilities.saveChartAsPNG(files[3], function.getPanel_3().getChart(), 600, 200);
			
			String imageFile1 = (files[1].getPath());
			String imageFile2 = (files[2].getPath());
			String imageFile3 = (files[3].getPath());
			
			ImageData data1 = ImageDataFactory.create(imageFile1);
			ImageData data2 = ImageDataFactory.create(imageFile2);
			ImageData data3 = ImageDataFactory.create(imageFile3);
			
			Image img1 = new Image(data1);
			Image img2 = new Image(data2);
			Image img3 = new Image(data3);
			
			Paragraph pr7 = new Paragraph("График координат").setFont(font).setFontSize(14).setTextAlignment(TextAlignment.CENTER);
			Paragraph pr8 = new Paragraph("График скорости").setFont(font).setFontSize(14).setTextAlignment(TextAlignment.CENTER);
			Paragraph pr9 = new Paragraph("График ускорения").setFont(font).setFontSize(14).setTextAlignment(TextAlignment.CENTER);
			
			document.add(pr)
				.add(pr1)
				.add(pr2)
				.add(pr2_1)
				.add(pr3)
				.add(table)
				.add(pr4)
				.add(table2)
				.add(pr5)
				.add(table3)
				.add(pr6)
				.add(table4)
				.add(img1)
				.add(pr7)
				.add(img2)
				.add(pr8)
				.add(img3)
				.add(pr9);
				
			pr = new Paragraph ();
			pr = new Paragraph ();
			document.close();
		}
		catch (IOException e) {
			CustomMessage.errorMessage(3);
			return false;
		}
		return true;
	};
	
}
