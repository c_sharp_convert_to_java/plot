import java.awt.Color;
import java.awt.Component;


import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.SystemColor;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;



import javax.swing.AbstractListModel;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import javax.swing.JPanel;
import javax.swing.JProgressBar;


import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import javax.swing.border.LineBorder;


import org.jfree.chart.*;


import java.awt.FlowLayout;

// Класс параметров отрисовки для ОС Windows
public class WindowsParam {
	
		/**
		 * @wbp.parser.entryPoint
		 */
		private static JFrame SetFrameParam(JFrame frame, GridBagLayout gridBagLayout) {
			// JFrame "форма"
			//
			frame = new JFrame("GUI");
			frame.setBounds(150, 30, 1000, 720); // установка расположения формы (отступы от границ экрана) и её размеров
			frame.setResizable(false); // запрет на изменение размеров формы
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // установка закрывающей операции по умолчанию (закрытие по кнопке окна "крестик")


			//frame.addMouseListener(new CustomListener());


			//
			// GridBagLayout "режим размещения элементов"
			//
			gridBagLayout = new GridBagLayout(); // инициализация режима размещения элементов Layout Manager
			gridBagLayout.columnWidths = new int[]{146, 20, 146, 146}; // задание ширины столбцов
			//gridBagLayout.rowHeights = new int[]{67, 16, 0, 0, 21, 49, 16, 58, 25, 0, 27, 22, 12, 37, -18, 0, 0, 0, 0, 0, 0}; // задание высоты строк
			gridBagLayout.rowHeights = new int[] {5, 5, 10, 10, 15, 15, 15, 15, 10, 10, 5, 10, 10, 10, 10, 10, 10, 5, 10, 10}; // задание высоты строк
			gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 1.0}; // задание параметра автоматического растяжения для каждого столбца
			gridBagLayout.rowWeights = new double[]{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
					0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 0.0}; // задание параметра автоматического растяжения для каждой строки
			frame.getContentPane().setLayout(gridBagLayout); // установка Layout формы как GridBagLayout
			return frame;
		}
		public static JFrame FrameParameters(JFrame frame, GridBagLayout gridBagLayout) {
			return SetFrameParam(frame, gridBagLayout);
		}
		
		private static JPanel SetModelsPanelParam(JFrame frame, JPanel modelsPanel_JPanel) {
			//
			// JPanel "модель панелей"
			//
			modelsPanel_JPanel = new JPanel(); // инициализация панели для объединения checkBox элементов
			modelsPanel_JPanel.setBorder(new LineBorder(new Color(0, 0, 0))); // установка обводки и её цвета
			//modelsPanel_JPanel.setForeground(SystemColor.windowText); // установка цвета текста внутри панели
			modelsPanel_JPanel.setBackground(SystemColor.window); // установка цвета фона панели
			modelsPanel_JPanel.setLayout(new GridLayout(2, 1, 0, 0)); // установка Layout панели как GridBagLayout с 2 строками и 1 столбцом, 
			  														  // без зазоров по горизонтали и вертикали
			// Создание ограничений для панели:
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.insets = new Insets(3, 2, 5, 5);
			gbc_panel.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_panel.gridy = 0; // установка координаты Y, занимаемой ячейки
			frame.getContentPane().add(modelsPanel_JPanel, gbc_panel); // добавление панели с её ограничениями на форму
			return modelsPanel_JPanel;
		}
		public static JPanel ModelsPanelParameters(JFrame frame, JPanel modelsPanel_JPanel) {
			return SetModelsPanelParam(frame, modelsPanel_JPanel);
		}
		
		private static JCheckBox SetLeaderCheckBoxParam(JPanel modelsPanel_JPanel, JCheckBox leader_CheckBox, ButtonGroup modelsGroup_ButtonGroup) {
			//
			// JCheckBox ""флажок" метода следования за лидером"
			//					
			leader_CheckBox = new JCheckBox("Модель следования за лидером"); // инициализация элемента "флажок"
			leader_CheckBox.setToolTipText("выберите модель"); // установка текста, выводимого при наведении
			leader_CheckBox.setSelected(true); // установка базового значения выбора (выбран по умолчанию)
			modelsGroup_ButtonGroup.add(leader_CheckBox); // добавление элемента в группу, чтобы значение "выбора" элемента зависело от остальных 
														  // значений "выбора" элементов в группе и наоборот
			modelsPanel_JPanel.add(leader_CheckBox); // добавление элемента на панель
			return leader_CheckBox;
		}
		public static JCheckBox LeaderCheckBoxParameters(JPanel modelsPanel_JPanel, JCheckBox leader_CheckBox, ButtonGroup modelsGroup_ButtonGroup) {
			return SetLeaderCheckBoxParam(modelsPanel_JPanel, leader_CheckBox, modelsGroup_ButtonGroup);
		}
		
		private static JCheckBox SetShepherdCheckBoxParam(JPanel modelsPanel_JPanel, JCheckBox shepherd_CheckBox, ButtonGroup modelsGroup_ButtonGroup) {
			//
			// JCheckBox ""флажок" метода пастуха"
			//
			shepherd_CheckBox = new JCheckBox("Модель пастуха"); // инициализация элемента "флажок"
			shepherd_CheckBox.setToolTipText("выберите модель"); // установка текста, выводимого при наведении
			modelsPanel_JPanel.add(shepherd_CheckBox); // добавление элемента в группу, чтобы значение "выбора" элемента зависело от остальных 
			  										   // значений "выбора" элементов в группе и наоборот
			modelsGroup_ButtonGroup.add(shepherd_CheckBox); // добавление элемента на панель
			return shepherd_CheckBox;
		}
		public static JCheckBox ShepherdCheckBoxParameters(JPanel modelsPanel_JPanel, JCheckBox shepherd_CheckBox, ButtonGroup modelsGroup_ButtonGroup) {
			return SetShepherdCheckBoxParam(modelsPanel_JPanel, shepherd_CheckBox, modelsGroup_ButtonGroup);
		}
		
		private static JPanel SetUserPanelParam(JFrame frame, JPanel userPanel_JPanel) {
			//
			// JPanel "модель панелей"
			//
			userPanel_JPanel = new JPanel(); // инициализация панели для объединения checkBox элементов
			userPanel_JPanel.setBorder(new LineBorder(new Color(0, 0, 0))); // установка обводки и её цвета
			//modelsPanel_JPanel.setForeground(SystemColor.windowText); // установка цвета текста внутри панели
			userPanel_JPanel.setBackground(SystemColor.window); // установка цвета фона панели
			userPanel_JPanel.setLayout(new GridLayout(1, 1, 0, 0)); // установка Layout панели как GridBagLayout с 2 строками и 1 столбцом, 
		      														  // без зазоров по горизонтали и вертикали
			// Создание ограничений для панели:
			GridBagConstraints gbc_panel = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_panel.fill = GridBagConstraints.BOTH; // установка типа заполнения элементом ячейки
			gbc_panel.insets = new Insets(5, 2, 0, 5); // установка отступов
			gbc_panel.anchor = GridBagConstraints.WEST; // установка якоря
			gbc_panel.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_panel.gridy = 1; // установка координаты Y, занимаемой ячейки
			frame.getContentPane().add(userPanel_JPanel, gbc_panel); // добавление панели с её ограничениями на форму
			return userPanel_JPanel;
		}
		public static JPanel UserPanelParameters(JFrame frame, JPanel userPanel_JPanel) {
			return SetUserPanelParam(frame, userPanel_JPanel);
		}
		
		private static JCheckBox SetUserCheckBoxParam(JPanel userPanel_JPanel, JCheckBox user_CheckBox /*ButtonGroup modelsGroup_ButtonGroup*/) {
			//
			// JCheckBox ""флажок" пользовательская функция"
			//
			user_CheckBox = new JCheckBox("Пользовательская функция"); // инициализация элемента "флажок"
			user_CheckBox.setToolTipText("выберите функцию"); // установка текста, выводимого при наведении
			userPanel_JPanel.add(user_CheckBox); // добавление элемента в группу, чтобы значение "выбора" элемента зависело от остальных 
			  										   // значений "выбора" элементов в группе и наоборот
			//modelsGroup_ButtonGroup.add(user_CheckBox); // добавление элемента на панель
			return user_CheckBox;
		}
		public static JCheckBox UserCheckBoxParameters(JPanel userPanel_JPanel, JCheckBox user_CheckBox /*ButtonGroup modelsGroup_ButtonGroup*/) {
			return SetUserCheckBoxParam(userPanel_JPanel, user_CheckBox /*modelsGroup_ButtonGroup*/);
		}

		private static JSeparator SetSeparatorTopParam(JFrame frame, JSeparator separatorTop_Separator) {
			//
			// JSeparator "верхняя разделительная черта"
			//
			separatorTop_Separator = new JSeparator(); // инициализация элемента "разделительная черта"
			separatorTop_Separator.setForeground(new Color(255, 140, 0)); // установка цвета 
			// Создание ограничений для "разделительной черты":
			GridBagConstraints gbc_separator_top = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_separator_top.fill = GridBagConstraints.HORIZONTAL; // установка типа заполнения элементом ячейки
			gbc_separator_top.insets = new Insets(0, 0, 0, 5); // установка отступов
			gbc_separator_top.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_separator_top.gridy = 2; // установка координаты Y, занимаемой ячейки
			frame.getContentPane().add(separatorTop_Separator, gbc_separator_top); // добавление "разделительной черты" с её ограничениями на форму
			return separatorTop_Separator;
		}
		public static JSeparator SeparatorTopParam(JFrame frame, JSeparator separatorTop_Separator) {
			return SetSeparatorTopParam(frame, separatorTop_Separator);
		}

		private static JLabel SetNumberParticlesLabelParam(JFrame frame, JLabel numberParticles_JLabel) {
			//
			// JLabel "число частиц"
			//
			numberParticles_JLabel = new JLabel("Число частиц:"); // инициализация поля JLabel
			numberParticles_JLabel.setFont(new Font("Lucida Grande", Font.BOLD, 13)); // установка шрифта, типа шрифта и его размера
			// Создание ограничений для поля:
			GridBagConstraints gbc_number_particles = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_number_particles.insets = new Insets(3, 12, 3, 5); // установка отступов
			gbc_number_particles.anchor = GridBagConstraints.WEST; // установка типа заполнения элементом ячейки
			gbc_number_particles.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_number_particles.gridy = 3; // установка координаты Y, занимаемой ячейки
			frame.getContentPane().add(numberParticles_JLabel, gbc_number_particles); // добавление "разделительной черты" с её ограничениями на форму
			return numberParticles_JLabel;
		}
		public static JLabel NumberParticlesLabelParameters(JFrame frame, JLabel numberParticles_JLabel) {
			return SetNumberParticlesLabelParam(frame, numberParticles_JLabel);
		}
		
		private static JPanel SetNumberParticlesPanelParam(JFrame frame, JPanel numberParticles_JPanel) {
			//
			// JPanel "панель числа частиц"
			//
			numberParticles_JPanel = new JPanel(); // инициализация панели для объединения элементов раздела "число частиц"
			// Создание ограничений для панели:
			GridBagConstraints gbc_n_panel = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_n_panel.fill = GridBagConstraints.HORIZONTAL; // установка типа заполнения элементом ячейки
			gbc_n_panel.insets = new Insets(0, 30, 0, 5);
			gbc_n_panel.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_n_panel.gridy = 4; // установка координаты Y, занимаемой ячейки
			numberParticles_JPanel.setLayout(new GridLayout(1, 1, 0, 0)); // установка Layout панели как GridBagLayout с 1 строками и 1 столбцом, 
			  												              // с зазорами авными 1 по горизонтали и вертикали
			frame.getContentPane().add(numberParticles_JPanel, gbc_n_panel); // добавление панели с её ограничениями на форму
			return numberParticles_JPanel;
		}
		public static JPanel NumberParticlesPanelParameters(JFrame frame, JPanel numberParticles_JPanel) {
			return SetNumberParticlesPanelParam(frame, numberParticles_JPanel);
		}
		
		private static JLabel SetNNumberParticlesLabelParam(JPanel numberParticles_JPanel, JLabel nNumberParticlesLabel_JLabel) {
			//
			// JLabel "числа частиц" 
			//
			nNumberParticlesLabel_JLabel = new JLabel("N:  "); // инициализация элемента nNumberParticlesLabel_JLabel
			nNumberParticlesLabel_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			numberParticles_JPanel.add(nNumberParticlesLabel_JLabel); // добавление элемента nNumberParticlesLabel_JLabel на панель
			return nNumberParticlesLabel_JLabel;
		}
		public static JLabel NNumberParticlesLabelParameters(JPanel numberParticles_JPanel, JLabel nNumberParticlesLabel_JLabel) {
			return SetNNumberParticlesLabelParam(numberParticles_JPanel, nNumberParticlesLabel_JLabel);
		}

		private static JTextField SetNTextFieldParam(JPanel numberParticles_JPanel, JTextField n_TextField) {
			//
			// JTextField "числа частиц"
			//
			n_TextField = new JTextField(); // инициализация поля ввода числа частиц
			n_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			n_TextField.setToolTipText("введите значение\n"); // установка вывода информации при наведении на элемент
			n_TextField.setColumns(3); // установка количества колонок в элементе
			numberParticles_JPanel.add(n_TextField); // добавление элемента на панель
			return n_TextField;
		}
		public static JTextField NTextFieldParameters(JPanel numberParticles_JPanel, JTextField n_TextField) {
			return SetNTextFieldParam(numberParticles_JPanel, n_TextField);
		}
		
		private static Component SetNumberParticlesStrutFirstParam(JPanel numberParticles_JPanel, Component numberParticles_strut_first) {
			//
			// Strut "распорка числа частиц"
			//
			numberParticles_strut_first = Box.createHorizontalStrut(20); // инициализация распорки
			numberParticles_JPanel.add(numberParticles_strut_first); // добавление распорки на панель
			return numberParticles_strut_first;
		}
		public static Component NumberParticlesStrutFirstParameters(JPanel numberParticles_JPanel, Component numberParticles_strut_first) {
			return SetNumberParticlesStrutFirstParam(numberParticles_JPanel, numberParticles_strut_first);
		}
		
		private static Component SetNumberParticlesStrutSecondParam(JPanel numberParticles_JPanel, Component numberParticles_strut_second) {
			//
			// Strut "распорка числа частиц"
			//
			numberParticles_strut_second = Box.createHorizontalStrut(20); // инициализация распорки
			numberParticles_JPanel.add(numberParticles_strut_second); // добавление распорки на панель
			return numberParticles_strut_second;
		}
		public static Component NumberParticlesStrutSecondParameters(JPanel numberParticles_JPanel, Component numberParticles_strut_second) {
			return SetNumberParticlesStrutSecondParam(numberParticles_JPanel, numberParticles_strut_second);
		}
		
		private static Component SetNumberParticlesStrutThirdParam(JPanel numberParticles_JPanel, Component numberParticles_strut_third) {
			//
			// Strut "распорка числа частиц"
			//
			numberParticles_strut_third = Box.createHorizontalStrut(20); // инициализация распорки
			numberParticles_JPanel.add(numberParticles_strut_third); // добавление распорки на панель
			return numberParticles_strut_third;
		}
		public static Component NumberParticlesStrutThirdParameters(JPanel numberParticles_JPanel, Component numberParticles_strut_third) {
			return SetNumberParticlesStrutThirdParam(numberParticles_JPanel, numberParticles_strut_third);
		}
		
		private static JLabel SetEntryConditionsLabelParam(JFrame frame, JLabel entryConditions_JLabel) {
			//
			// JLabel "начальные условия"
			//
			entryConditions_JLabel = new JLabel("Начальные условия:"); // инициализация элемента entryConditions_JLabel типа JLabel
			entryConditions_JLabel.setFont(new Font("Lucida Grande", Font.BOLD, 13)); // установка шрифта, типа шрифта и его размера
			// Создание ограничений для поля:
			GridBagConstraints gbc_entry_conditions_label = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_entry_conditions_label.insets = new Insets(3, 12, 3, 5); // установка отступов
			gbc_entry_conditions_label.anchor = GridBagConstraints.WEST; // установка типа заполнения элементом ячейки
			gbc_entry_conditions_label.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_entry_conditions_label.gridy = 5; // установка координаты Y, занимаемой ячейки
			frame.getContentPane().add(entryConditions_JLabel, gbc_entry_conditions_label); // добавление элемента с его ограничениями на форму
			return entryConditions_JLabel;
		}
		public static JLabel EntryConditionsLabelParameters(JFrame frame, JLabel entryConditions_JLabel) {
			return SetEntryConditionsLabelParam(frame, entryConditions_JLabel);
		}
		
		private static JPanel SetEntryConditionsPanelParam(JFrame frame, JPanel entryConditions_JPanel) {
			//
			// JPanel "начальные условия"
			//
			entryConditions_JPanel = new JPanel(); // инициализация элемента entryConditions_JPanel типа JPanel
			// Создание ограничений для панели:
			GridBagConstraints gbc_entry_conditions_panel = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_entry_conditions_panel.fill = GridBagConstraints.HORIZONTAL; // установка типа заполнения элементом ячейки
			gbc_entry_conditions_panel.insets = new Insets(0, 30, 0, 5);
			gbc_entry_conditions_panel.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_entry_conditions_panel.gridy = 6; // установка координаты Y, занимаемой ячейки
			entryConditions_JPanel.setLayout(new GridLayout(2, 1, 0, 0)); // установка Layout панели как GridBagLayout с 2 строками и 1 столбцом, 
			  															  // без зазоров по горизонтали и вертикали
			frame.getContentPane().add(entryConditions_JPanel, gbc_entry_conditions_panel); // добавление панели с её ограничениями на форму
			return entryConditions_JPanel;
		}
		public static JPanel EntryConditionsPanelParameters(JFrame frame, JPanel entryConditions_JPanel) {
			return SetEntryConditionsPanelParam(frame, entryConditions_JPanel);
		}
		
		private static JLabel SetT0EntryConditionsLabelParam(JPanel entryConditions_JPanel, JLabel t0EntryConditions_JLabel) {
			//
			// JLabel "начальные условия"
			//
			t0EntryConditions_JLabel = new JLabel("t0:"); // инициализация элемента c0EntryConditions_JLabel
			t0EntryConditions_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			entryConditions_JPanel.add(t0EntryConditions_JLabel); // добавление элемента на панель
			return t0EntryConditions_JLabel;
		}
		public static JLabel T0EntryConditionsLabelParameters(JPanel entryConditions_JPanel, JLabel t0EntryConditions_JLabel) {
			return SetT0EntryConditionsLabelParam(entryConditions_JPanel, t0EntryConditions_JLabel);
		}
		
		private static JTextField SetT0EntryConditionsTextFieldParam(JPanel entryConditions_JPanel, JTextField t0_TextField) {
			//
			// JTextField "начальные условия"
			//
			t0_TextField = new JTextField(); // инициализация элемента t0_TextField
			t0_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			t0_TextField.setToolTipText("введите значение"); // установка сообщения, выводящегося при наведении на элемент
			t0_TextField.setColumns(3); // установка количества колонок в элементе
			entryConditions_JPanel.add(t0_TextField); // добавление элемента на панель
			return t0_TextField;
		}
		public static JTextField T0EntryConditionsTextFieldParameters(JPanel entryConditions_JPanel, JTextField t0_TextField) {
			return SetT0EntryConditionsTextFieldParam(entryConditions_JPanel, t0_TextField);
		}
		
		private static Component SetEntryConditionsStrutFirstParam(JPanel entryConditions_JPanel, Component entry_conditions_strut_first) {
			//
			// Strut "начальные условия"
			//
			entry_conditions_strut_first = Box.createHorizontalStrut(20); // инициализация распорки
			entryConditions_JPanel.add(entry_conditions_strut_first); // добавление распорки на панель
			return entry_conditions_strut_first;
		}
		public static Component EntryConditionsStrutFirstParameters(JPanel entryConditions_JPanel, Component entry_conditions_strut_first) {
			return SetEntryConditionsStrutFirstParam(entryConditions_JPanel, entry_conditions_strut_first);
		}
		
		private static Component SetEntryConditionsStrutSecondParam(JPanel entryConditions_JPanel, Component entry_conditions_strut_second) {
			//
			// Strut "начальные условия"
			//
			entry_conditions_strut_second = Box.createHorizontalStrut(20); // инициализация распорки
			entryConditions_JPanel.add(entry_conditions_strut_second); // добавление распорки на панель
			return entry_conditions_strut_second;
		}
		public static Component EntryConditionsStrutSecondParameters(JPanel entryConditions_JPanel, Component entry_conditions_strut_second) {
			return SetEntryConditionsStrutSecondParam(entryConditions_JPanel, entry_conditions_strut_second);
		}
		
		private static Component SetEntryConditionsStrutThirdParam(JPanel entryConditions_JPanel, Component entry_conditions_strut_third) {
			//
			// Strut "начальные условия"
			//
			entry_conditions_strut_third = Box.createHorizontalStrut(20); // инициализация распорки
			entryConditions_JPanel.add(entry_conditions_strut_third); // добавление распорки на панель
			return entry_conditions_strut_third;
		}
		public static Component EntryConditionsStrutThirdParameters(JPanel entryConditions_JPanel, Component entry_conditions_strut_third) {
			return SetEntryConditionsStrutThirdParam(entryConditions_JPanel, entry_conditions_strut_third);
		}
		
		private static JLabel SetY0EntryConditionsLabelParam(JPanel entryConditions_JPanel, JLabel y0EntryConditions_JLabel) {
			//
			// JLabel "начальные условия"
			//
			y0EntryConditions_JLabel = new JLabel("y0:"); // инициализация элемента y0EntryConditions_JLabel
			y0EntryConditions_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			entryConditions_JPanel.add(y0EntryConditions_JLabel); // добавление элемента на панель
			return y0EntryConditions_JLabel;
		}
		public static JLabel Y0EntryConditionsLabelParameters(JPanel entryConditions_JPanel, JLabel y0EntryConditions_JLabel) {
			return SetY0EntryConditionsLabelParam(entryConditions_JPanel, y0EntryConditions_JLabel);
		}
		
		private static JTextField SetY0EntryConditionsTextFieldParam(JPanel entryConditions_JPanel, JTextField y0_TextField) {
			//
			// JTextField "начальные условия"
			//
			y0_TextField = new JTextField(); // инициализация элемента y0_TextField
			y0_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			y0_TextField.setToolTipText("введите значение\n"); // установка сообщения, выводящегося при наведении на элемент
			y0_TextField.setColumns(3); // установка количества колонок в элементе
			entryConditions_JPanel.add(y0_TextField); // добавление элемента на панель
			return y0_TextField;
		}
		public static JTextField Y0EntryConditionsTextFieldParameters(JPanel entryConditions_JPanel, JTextField y0_TextField) {
			return SetY0EntryConditionsTextFieldParam(entryConditions_JPanel, y0_TextField);
		}
		
		private static Component SetEntryConditionsStrutFourthParam(JPanel entryConditions_JPanel, Component entry_conditions_strut_fourth) {
			//
			// Strut "начальные условия"
			//
			entry_conditions_strut_fourth = Box.createHorizontalStrut(20); // инициализация распорки
			entryConditions_JPanel.add(entry_conditions_strut_fourth); // добавление распорки на панель
			return entry_conditions_strut_fourth;
		}
		public static Component EntryConditionsStrutFourthParameters(JPanel entryConditions_JPanel, Component entry_conditions_strut_fourth) {
			return SetEntryConditionsStrutFourthParam(entryConditions_JPanel, entry_conditions_strut_fourth);
		}
		
		private static Component SetEntryConditionsStrutFifthParam(JPanel entryConditions_JPanel, Component entry_conditions_strut_fifth) {
			//
			// Strut "начальные условия"
			//
			entry_conditions_strut_fifth = Box.createHorizontalStrut(20); // инициализация распорки
			entryConditions_JPanel.add(entry_conditions_strut_fifth); // добавление распорки на панель
			return entry_conditions_strut_fifth;
		}
		public static Component EntryConditionsStrutFifthParameters(JPanel entryConditions_JPanel, Component entry_conditions_strut_fifth) {
			return SetEntryConditionsStrutFifthParam(entryConditions_JPanel, entry_conditions_strut_fifth);
		}
		
		private static Component SetEntryConditionsStrutSixthParam(JPanel entryConditions_JPanel, Component entry_conditions_strut_sixth) {
			//
			// Strut "начальные условия"
			//
			entry_conditions_strut_sixth = Box.createHorizontalStrut(20); // инициализация распорки
			entryConditions_JPanel.add(entry_conditions_strut_sixth); // добавление распорки на панель
			return entry_conditions_strut_sixth;
		}
		public static Component EntryConditionsStrutSixthParameters(JPanel entryConditions_JPanel, Component entry_conditions_strut_sixth) {
			return SetEntryConditionsStrutSixthParam(entryConditions_JPanel, entry_conditions_strut_sixth);
		}
		
		private static JLabel SetRestrictionsLabelParam(JFrame frame, JLabel restrictions_JLabel) {
			//
			// JLabel "ограничения"
			//
			restrictions_JLabel = new JLabel("Ограничения:"); // инициализация элемента restrictions_JLabel типа JLabel
			restrictions_JLabel.setFont(new Font("Lucida Grande", Font.BOLD, 13)); // установка шрифта, типа шрифта и его размера
			restrictions_JLabel.setHorizontalAlignment(SwingConstants.LEFT); // установка расположения по горизонтали
			// Создание ограничений для поля:
			GridBagConstraints gbc_restrictions = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_restrictions.insets = new Insets(3, 12, 3, 5); // установка отступов
			gbc_restrictions.anchor = GridBagConstraints.WEST; // установка типа заполнения элементом ячейки
			gbc_restrictions.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_restrictions.gridy = 7; // установка координаты Y, занимаемой ячейки
			frame.getContentPane().add(restrictions_JLabel, gbc_restrictions); // добавление элемента с его ограничениями на форму
			return restrictions_JLabel;
		}
		public static JLabel RestrictionsLabelParameters(JFrame frame, JLabel restrictions_JLabel) {
			return SetRestrictionsLabelParam(frame, restrictions_JLabel);
		}
		
		private static JPanel SetRestrictionsPanelParam(JFrame frame, JPanel restrictions_JPanel) {
			//
			// JPanel "ограничения"
			//
			restrictions_JPanel = new JPanel(); // инициализация элемента restrictions_JPanel типа JPanel
			// Создание ограничений для панели:
			GridBagConstraints gbc_restrictions_panel = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_restrictions_panel.fill = GridBagConstraints.HORIZONTAL; // установка типа заполнения элементом ячейки
			gbc_restrictions_panel.insets = new Insets(0, 30, 0, 5);
			gbc_restrictions_panel.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_restrictions_panel.gridy = 8; // установка координаты Y, занимаемой ячейки
			restrictions_JPanel.setLayout(new GridLayout(3, 1, 0, 0)); // установка Layout панели как GridBagLayout с 3 строками и 1 столбцом, 
			  														   // а также без зазоров по горизонтали и вертикали
			frame.getContentPane().add(restrictions_JPanel, gbc_restrictions_panel); // добавление элемента с его ограничениями на форму
			return restrictions_JPanel;
		}
		public static JPanel RestrictionsPanelParameters(JFrame frame, JPanel restrictions_JPanel) {
			return SetRestrictionsPanelParam(frame, restrictions_JPanel);
		}
		
		private static JLabel SetM1RestrictionsLabelParam(JPanel restrictions_JPanel, JLabel m1Restrictions_JLabel) {
			//
			// JLabel "ограничения"
			//
			m1Restrictions_JLabel = new JLabel("M1:"); // инициализация элемента m1Restrictions_JLabel типа JLabel
			m1Restrictions_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			restrictions_JPanel.add(m1Restrictions_JLabel); // добавление элемента на панель
			return m1Restrictions_JLabel;
		}
		public static JLabel M1RestrictionsLabelParameters(JPanel restrictions_JPanel, JLabel m1Restrictions_JLabel) {
			return SetM1RestrictionsLabelParam(restrictions_JPanel, m1Restrictions_JLabel);
		}
		
		private static JTextField SetM1RestrictionsTextFieldParam(JPanel restrictions_JPanel, JTextField m1_TextField) {
			//
			// JTextField "ограничения"
			//
			m1_TextField = new JTextField(); // инициализация элемента m1_TextField типа JTextField
			m1_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			m1_TextField.setToolTipText("введите значение"); // установка сообщения, выводящегося при наведении на элемент
			m1_TextField.setColumns(3); // установка количества колонок в элементе
			restrictions_JPanel.add(m1_TextField);  // добавление элемента на панель
			return m1_TextField;
		}
		public static JTextField M1RestrictionsTextFieldParameters(JPanel restrictions_JPanel, JTextField m1_TextField) {
			return SetM1RestrictionsTextFieldParam(restrictions_JPanel, m1_TextField);
		}
		
		private static Component SetRestrictionsStrutFirstParam(JPanel restrictions_JPanel, Component restrictions_strut_first) {
			//
			// Strut "ограничения"
			//
			restrictions_strut_first = Box.createHorizontalStrut(20); // инициализация распорки
			restrictions_JPanel.add(restrictions_strut_first); // добавление распорки на панель
			return restrictions_strut_first;
		}
		public static Component RestrictionsStrutFirstParameters(JPanel restrictions_JPanel, Component restrictions_strut_first) {
			return SetRestrictionsStrutFirstParam(restrictions_JPanel, restrictions_strut_first);
		}
		
		private static Component SetRestrictionsStrutSecondParam(JPanel restrictions_JPanel, Component restrictions_strut_second) {
			//
			// Strut "ограничения"
			//
			restrictions_strut_second = Box.createHorizontalStrut(20); // инициализация распорки
			restrictions_JPanel.add(restrictions_strut_second); // добавление распорки на панель
			return restrictions_strut_second;
		}
		public static Component RestrictionsStrutSecondParameters(JPanel restrictions_JPanel, Component restrictions_strut_second) {
			return SetRestrictionsStrutSecondParam(restrictions_JPanel, restrictions_strut_second);
		}
		
		private static Component SetRestrictionsStrutThirdParam(JPanel restrictions_JPanel, Component restrictions_strut_third) {
			//
			// Strut "ограничения"
			//
			restrictions_strut_third = Box.createHorizontalStrut(20); // инициализация распорки
			restrictions_JPanel.add(restrictions_strut_third); // добавление распорки на панель
			return restrictions_strut_third;
		}
		public static Component RestrictionsStrutThirdParameters(JPanel restrictions_JPanel, Component restrictions_strut_third) {
			return SetRestrictionsStrutThirdParam(restrictions_JPanel, restrictions_strut_third);
		}
		
		private static JLabel SetM2RestrictionsLabelParam(JPanel restrictions_JPanel, JLabel m2Restrictions_JLabel) {
			//
			// JLabel "ограничения"
			//
			m2Restrictions_JLabel = new JLabel("M2:"); // инициализация элемента m2Restrictions_JLabel типа JLabel
			m2Restrictions_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			restrictions_JPanel.add(m2Restrictions_JLabel); // добавления элемента на панель
			return m2Restrictions_JLabel;
		}
		public static JLabel M2RestrictionsLabelParameters(JPanel restrictions_JPanel, JLabel m2Restrictions_JLabel) {
			return SetM2RestrictionsLabelParam(restrictions_JPanel, m2Restrictions_JLabel);
		}
		
		private static JTextField SetM2RestrictionsTextFieldParam(JPanel restrictions_JPanel, JTextField m2_TextField) {
			//
			// JTextField "ограничения"
			//
			m2_TextField = new JTextField(); // инициализация элемента m2_TextField типа JTextField
			m2_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			m2_TextField.setToolTipText("введите значение"); // установка сообщения, выводящегося при наведении на элемент
			m2_TextField.setColumns(3); // установка количества колонок в элементе
			restrictions_JPanel.add(m2_TextField); // добавление элемента на панель
			return m2_TextField;
		}
		public static JTextField M2RestrictionsTextFieldParameters(JPanel restrictions_JPanel, JTextField m2_TextField) {
			return SetM2RestrictionsTextFieldParam(restrictions_JPanel, m2_TextField);
		}
		
		private static Component SetRestrictionsStrutFourthParam(JPanel restrictions_JPanel, Component restrictions_strut_fourth) {
			//
			// Strut "ограничения"
			//
			restrictions_strut_fourth = Box.createHorizontalStrut(20); // инициализация распорки
			restrictions_JPanel.add(restrictions_strut_fourth); // добавление распорки на панель
			return restrictions_strut_fourth;
		}
		public static Component RestrictionsStrutFourthParameters(JPanel restrictions_JPanel, Component restrictions_strut_fourth) {
			return SetRestrictionsStrutFourthParam(restrictions_JPanel, restrictions_strut_fourth);
		}
		
		private static Component SetRestrictionsStrutFifthParam(JPanel restrictions_JPanel, Component restrictions_strut_fifth) {
			//
			// Strut "ограничения"
			//
			restrictions_strut_fifth = Box.createHorizontalStrut(20); // инициализация распорки
			restrictions_JPanel.add(restrictions_strut_fifth); // добавление распорки на панель
			return restrictions_strut_fifth;
		}
		public static Component RestrictionsStrutFifthParameters(JPanel restrictions_JPanel, Component restrictions_strut_fifth) {
			return SetRestrictionsStrutFifthParam(restrictions_JPanel, restrictions_strut_fifth);
		}
		
		private static Component SetRestrictionsStrutSixthParam(JPanel restrictions_JPanel, Component restrictions_strut_sixth) {
			//
			// Strut "ограничения"
			//
			restrictions_strut_sixth = Box.createHorizontalStrut(20); // инициализация распорки
			restrictions_JPanel.add(restrictions_strut_sixth); // добавление распорки на панель
			return restrictions_strut_sixth;
		}
		public static Component RestrictionsStrutSixthParameters(JPanel restrictions_JPanel, Component restrictions_strut_sixth) {
			return SetRestrictionsStrutSixthParam(restrictions_JPanel, restrictions_strut_sixth);
		}
		
		private static JLabel SetM3RestrictionsLabelParam(JPanel restrictions_JPanel, JLabel m3Restrictions_JLabel) {
			//
			// JLabel "ограничения"
			//
			m3Restrictions_JLabel = new JLabel("M3:"); // инициализация элемента m3Restrictions_JLabel типа JLabel
			m3Restrictions_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			restrictions_JPanel.add(m3Restrictions_JLabel); // добавления элемента на панель
			return m3Restrictions_JLabel;
		}
		public static JLabel M3RestrictionsLabelParameters(JPanel restrictions_JPanel, JLabel m3Restrictions_JLabel) {
			return SetM3RestrictionsLabelParam(restrictions_JPanel, m3Restrictions_JLabel);
		}
		
		private static JTextField SetM3RestrictionsTextFieldParam(JPanel restrictions_JPanel, JTextField m3_TextField) {
			//
			// JTextField "ограничения"
			//
			m3_TextField = new JTextField(); // инициализация элемента m3_TextField типа JTextField
			m3_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			m3_TextField.setToolTipText("введите значение"); // установка сообщения, выводящегося при наведении на элемент
			m3_TextField.setColumns(3); // установка количества колонок в элементе
			restrictions_JPanel.add(m3_TextField); // добавление элемента на панель
			return m3_TextField;
		}
		public static JTextField M3RestrictionsTextFieldParameters(JPanel restrictions_JPanel, JTextField m3_TextField) {
			return SetM3RestrictionsTextFieldParam(restrictions_JPanel, m3_TextField);
		}
		
		private static Component SetRestrictionsStrutSeventhParam(JPanel restrictions_JPanel, Component restrictions_strut_seventh) {
			//
			// Strut "ограничения"
			//
			restrictions_strut_seventh = Box.createHorizontalStrut(20); // инициализация распорки
			restrictions_JPanel.add(restrictions_strut_seventh); // добавление распорки на панель
			return restrictions_strut_seventh;
		}
		public static Component RestrictionsStrutSeventhParameters(JPanel restrictions_JPanel, Component restrictions_strut_seventh) {
			return SetRestrictionsStrutSeventhParam(restrictions_JPanel, restrictions_strut_seventh);
		}
		
		private static Component SetRestrictionsStrutEighthParam(JPanel restrictions_JPanel, Component restrictions_strut_eighth) {
			//
			// Strut "ограничения"
			//
			restrictions_strut_eighth = Box.createHorizontalStrut(20); // инициализация распорки
			restrictions_JPanel.add(restrictions_strut_eighth); // добавление распорки на панель
			return restrictions_strut_eighth;
		}
		public static Component RestrictionsStrutEighthParameters(JPanel restrictions_JPanel, Component restrictions_strut_eighth) {
			return SetRestrictionsStrutEighthParam(restrictions_JPanel, restrictions_strut_eighth);
		}
		
		private static Component SetRestrictionsStrutNinthParam(JPanel restrictions_JPanel, Component restrictions_strut_ninth) {
			//
			// Strut "ограничения"
			//
			restrictions_strut_ninth = Box.createHorizontalStrut(20); // инициализация распорки
			restrictions_JPanel.add(restrictions_strut_ninth); // добавление распорки на панель
			return restrictions_strut_ninth;
		}
		public static Component RestrictionsStrutNinthParameters(JPanel restrictions_JPanel, Component restrictions_strut_ninth) {
			return SetRestrictionsStrutNinthParam(restrictions_JPanel, restrictions_strut_ninth);
		}
		
		private static JLabel SetCoefficientsLabelParam(JFrame frame, JLabel coefficients_JLabel) {
			//
			// JLabel "коэффициенты"
			//
			coefficients_JLabel = new JLabel("Коэффициенты:"); // инициализация элемента coefficients_JLabel типа JLabel
			coefficients_JLabel.setFont(new Font("Lucida Grande", Font.BOLD, 13)); // установка шрифта, типа шрифта и его размера
			// Создание ограничений для поля:
			GridBagConstraints gbc_coefficients_label = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_coefficients_label.insets = new Insets(3, 12, 3, 5); // установка отступов
			gbc_coefficients_label.anchor = GridBagConstraints.WEST; // установка типа заполнения элементом ячейки
			gbc_coefficients_label.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_coefficients_label.gridy = 9; // установка координаты Y, занимаемой ячейки
			frame.getContentPane().add(coefficients_JLabel, gbc_coefficients_label); // добавление элемента с его ограничениями на форму
			return coefficients_JLabel;
		}
		public static JLabel CoefficientsLabelParameters(JFrame frame, JLabel coefficients_JLabel) {
			return SetCoefficientsLabelParam(frame, coefficients_JLabel);
		}
		
		private static JPanel SetCoefficientsPanelParam(JFrame frame, JPanel coefficients_JPanel) {
			//
			// JPanel "коэффициенты"
			//
			coefficients_JPanel = new JPanel(); // инициализация элемента coefficients_JPanel типа JPanel
			// Создание ограничений для панели:
			GridBagConstraints gbc_coefficients_panel = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_coefficients_panel.fill = GridBagConstraints.HORIZONTAL; // установка типа заполнения элементом ячейки
			gbc_coefficients_panel.insets = new Insets(0, 30, 0, 5);
			gbc_coefficients_panel.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_coefficients_panel.gridy = 10; // установка координаты Y, занимаемой ячейки
			coefficients_JPanel.setLayout(new GridLayout(3, 1, 0, 0)); // установка Layout панели как GridBagLayout с 3 строками и 1 столбцом, 
			   														   // а также без зазоров по горизонтали и вертикали
			frame.getContentPane().add(coefficients_JPanel, gbc_coefficients_panel); // добавление панели с её ограничениями на форму
			return coefficients_JPanel;
		}
		public static JPanel CoefficientsPanelParameters(JFrame frame, JPanel coefficients_JPanel) {
			return SetCoefficientsPanelParam(frame, coefficients_JPanel);
		}
		
		private static JLabel SetC0СoefficientsLabelParam(JPanel coefficients_JPanel, JLabel с0Сoefficients_JLabel) {
			//
			// JLabel "коэффициенты"
			//
			с0Сoefficients_JLabel = new JLabel("с0:"); // инициализация элемента с1Сoefficients_JLabel типа JLabel
			с0Сoefficients_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			coefficients_JPanel.add(с0Сoefficients_JLabel); // добавление элемента на панель
			return с0Сoefficients_JLabel;
		}
		public static JLabel C0СoefficientsLabelParameters(JPanel coefficients_JPanel, JLabel с1Сoefficients_JLabel) {
			return SetC0СoefficientsLabelParam(coefficients_JPanel, с1Сoefficients_JLabel);
		}
		
		private static JTextField SetC0СoefficientsTextFieldParam(JPanel coefficients_JPanel, JTextField c0_TextField) {
			//
			// JTextField "коэффициенты"
			//
			c0_TextField = new JTextField(); // инициализация элемента c1_TextField типа JTextField
			c0_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			c0_TextField.setToolTipText("введите значение"); // установка сообщения, выводящегося при наведении на элемент
			c0_TextField.setColumns(3); // установка количества колонок в элементе
			coefficients_JPanel.add(c0_TextField); // добавление элемента на панель
			return c0_TextField;
		}
		public static JTextField C0СoefficientsTextFieldParameters(JPanel coefficients_JPanel, JTextField c0_TextField) {
			return SetC0СoefficientsTextFieldParam(coefficients_JPanel, c0_TextField);
		}
		
		private static Component SetCoefficientsStrutFirstParam(JPanel coefficients_JPanel, Component coefficients_strut_first) {
			//
			// Strut "коэффициенты"
			//
			coefficients_strut_first = Box.createHorizontalStrut(20); // инициализация распорки
			coefficients_JPanel.add(coefficients_strut_first); // добавление распорки на панель
			return coefficients_strut_first;
		}
		public static Component CoefficientsStrutFirstParameters(JPanel coefficients_JPanel, Component coefficients_strut_first) {
			return SetCoefficientsStrutFirstParam(coefficients_JPanel, coefficients_strut_first);
		}
		
		private static Component SetCoefficientsStrutSecondParam(JPanel coefficients_JPanel, Component coefficients_strut_second) {
			//
			// Strut "коэффициенты"
			//
			coefficients_strut_second = Box.createHorizontalStrut(20); // инициализация распорки
			coefficients_JPanel.add(coefficients_strut_second); // добавление распорки на панель
			return coefficients_strut_second;
		}
		public static Component CoefficientsStrutSecondParameters(JPanel coefficients_JPanel, Component coefficients_strut_second) {
			return SetCoefficientsStrutSecondParam(coefficients_JPanel, coefficients_strut_second);
		}
		
		private static Component SetCoefficientsStrutThirdParam(JPanel coefficients_JPanel, Component coefficients_strut_third) {
			//
			// Strut "коэффициенты"
			//
			coefficients_strut_third = Box.createHorizontalStrut(20); // инициализация распорки
			coefficients_JPanel.add(coefficients_strut_third); // добавление распорки на панель
			return coefficients_strut_third;
		}
		public static Component CoefficientsStrutThirdParameters(JPanel coefficients_JPanel, Component coefficients_strut_third) {
			return SetCoefficientsStrutThirdParam(coefficients_JPanel, coefficients_strut_third);
		}
		
		private static JLabel SetC1СoefficientsLabelParam(JPanel coefficients_JPanel, JLabel с1Сoefficients_JLabel) {
			//
			// JLabel "коэффициенты"
			//
			с1Сoefficients_JLabel = new JLabel("с1:"); // инициализация элемента с2Сoefficients_JLabel типа JLabel
			с1Сoefficients_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			coefficients_JPanel.add(с1Сoefficients_JLabel); // добавление элемента на панель
			return с1Сoefficients_JLabel;
		}
		public static JLabel C1СoefficientsLabelParameters(JPanel coefficients_JPanel, JLabel с1Сoefficients_JLabel) {
			return SetC1СoefficientsLabelParam(coefficients_JPanel, с1Сoefficients_JLabel);
		}
		
		private static JTextField SetC1CoefficientsTextFieldParam(JPanel coefficients_JPanel, JTextField c1_TextField) {
			//
			// JTextField "коэффициенты"
			//
			c1_TextField = new JTextField(); // инициализация элемента c2_TextField типа JTextField
			c1_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			c1_TextField.setToolTipText("введите значение"); // установка сообщения, выводящегося при наведении на элемент
			c1_TextField.setColumns(3); // установка количества колонок в элементе
			coefficients_JPanel.add(c1_TextField); // добавление элемента на панель
			return c1_TextField;
		}
		public static JTextField C1CoefficientsTextFieldParameters(JPanel coefficients_JPanel, JTextField c1_TextField) {
			return SetC1CoefficientsTextFieldParam(coefficients_JPanel, c1_TextField);
		}
		
		private static Component SetCoefficientsStrutFourthParam(JPanel coefficients_JPanel, Component coefficients_strut_fourth) {
			//
			// Strut "коэффициенты"
			//
			coefficients_strut_fourth = Box.createHorizontalStrut(20); // инициализация распорки
			coefficients_JPanel.add(coefficients_strut_fourth); // добавление распорки на панель
			return coefficients_strut_fourth;
		}
		public static Component CoefficientsStrutFourthParameters(JPanel coefficients_JPanel, Component coefficients_strut_fourth) {
			return SetCoefficientsStrutFourthParam(coefficients_JPanel, coefficients_strut_fourth);
		}
		
		private static Component SetCoefficientsStrutFifthParam(JPanel coefficients_JPanel, Component coefficients_strut_fifth) {
			//
			// Strut "коэффициенты"
			//
			coefficients_strut_fifth = Box.createHorizontalStrut(20); // инициализация распорки
			coefficients_JPanel.add(coefficients_strut_fifth); // добавление распорки на панель
			return coefficients_strut_fifth;
		}
		public static Component CoefficientsStrutFifthParameters(JPanel coefficients_JPanel, Component coefficients_strut_fifth) {
			return SetCoefficientsStrutFifthParam(coefficients_JPanel, coefficients_strut_fifth);
		}
		
		private static Component SetCoefficientsStrutSixthParam(JPanel coefficients_JPanel, Component coefficients_strut_sixth) {
			//
			// Strut "коэффициенты"
			//
			coefficients_strut_sixth = Box.createHorizontalStrut(20); // инициализация распорки
			coefficients_JPanel.add(coefficients_strut_sixth); // добавление распорки на панель
			return coefficients_strut_sixth;
		}
		public static Component CoefficientsStrutSixthParameters(JPanel coefficients_JPanel, Component coefficients_strut_sixth) {
			return SetCoefficientsStrutSixthParam(coefficients_JPanel, coefficients_strut_sixth);
		}
		
		private static JLabel SetC2СoefficientsLabelParam(JPanel coefficients_JPanel, JLabel с2Сoefficients_JLabel) {
			//
			// JLabel "коэффициенты"
			//
			с2Сoefficients_JLabel = new JLabel("с2:"); // инициализация элемента с2Сoefficients_JLabel типа JLabel
			с2Сoefficients_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			coefficients_JPanel.add(с2Сoefficients_JLabel); // добавление элемента на панель
			return с2Сoefficients_JLabel;
		}
		public static JLabel C2СoefficientsLabelParameters(JPanel coefficients_JPanel, JLabel с2Сoefficients_JLabel) {
			return SetC2СoefficientsLabelParam(coefficients_JPanel, с2Сoefficients_JLabel);
		}
		
		private static JTextField SetC2CoefficientsTextFieldParam(JPanel coefficients_JPanel, JTextField c2_TextField) {
			//
			// JTextField "коэффициенты"
			//
			c2_TextField = new JTextField(); // инициализация элемента c3_TextField типа JTextField
			c2_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			c2_TextField.setToolTipText("введите значение"); // установка сообщения, выводящегося при наведении на элемент
			c2_TextField.setColumns(3); // установка количества колонок в элементе
			coefficients_JPanel.add(c2_TextField); // добавление элемента на панель
			return c2_TextField;
		}
		public static JTextField C2CoefficientsTextFieldParameters(JPanel coefficients_JPanel, JTextField c2_TextField) {
			return SetC2CoefficientsTextFieldParam(coefficients_JPanel, c2_TextField);
		}
		
		private static Component SetCoefficientsStrutSeventhParam(JPanel coefficients_JPanel, Component coefficients_strut_seventh) {
			//
			// Strut "коэффициенты"
			//
			coefficients_strut_seventh = Box.createHorizontalStrut(20); // инициализация распорки
			coefficients_JPanel.add(coefficients_strut_seventh); // добавление распорки на панель
			return coefficients_strut_seventh;
		}
		public static Component CoefficientsStrutSeventhParameters(JPanel coefficients_JPanel, Component coefficients_strut_seventh) {
			return SetCoefficientsStrutSeventhParam(coefficients_JPanel, coefficients_strut_seventh);
		}
		
		private static Component SetCoefficientsStrutEighthParam(JPanel coefficients_JPanel, Component coefficients_strut_eighth) {
			//
			// Strut "коэффициенты"
			//
			coefficients_strut_eighth = Box.createHorizontalStrut(20); // инициализация распорки
			coefficients_JPanel.add(coefficients_strut_eighth); // добавление распорки на панель
			return coefficients_strut_eighth;
		}
		public static Component CoefficientsStrutEighthParameters(JPanel coefficients_JPanel, Component coefficients_strut_eighth) {
			return SetCoefficientsStrutEighthParam(coefficients_JPanel, coefficients_strut_eighth);
		}
		
		private static Component SetCoefficientsStrutNinthParam(JPanel coefficients_JPanel, Component coefficients_strut_ninth) {
			//
			// Strut "коэффициенты"
			//
			coefficients_strut_ninth = Box.createHorizontalStrut(20); // инициализация распорки
			coefficients_JPanel.add(coefficients_strut_ninth); // добавление распорки на панель
			return coefficients_strut_ninth;
		}
		public static Component CoefficientsStrutNinthParameters(JPanel coefficients_JPanel, Component coefficients_strut_ninth) {
			return SetCoefficientsStrutNinthParam(coefficients_JPanel, coefficients_strut_ninth);
		}
		
		private static JLabel SetTimeLabelParam(JFrame frame, JLabel time_JLabel) {
			//
			// JLabel "время"
			//
			time_JLabel = new JLabel("Время:"); // инициализация элемента time_JLabel типа JLabel
			time_JLabel.setFont(new Font("Lucida Grande", Font.BOLD, 13)); // установка шрифта, типа шрифта и его размера
			time_JLabel.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			// Создание ограничений для поля:
			GridBagConstraints gbc_time_label = new GridBagConstraints(); // переменная для хранения ограничений
			gbc_time_label.insets = new Insets(3, 12, 3, 5); // установка отступов
			gbc_time_label.anchor = GridBagConstraints.WEST; // установка типа заполнения элементом ячейки
			gbc_time_label.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_time_label.gridy = 11; // установка координаты Y, занимаемой ячейки
			frame.getContentPane().add(time_JLabel, gbc_time_label); // добавление панели с её ограничениями на форму
			return time_JLabel;
		}
		public static JLabel TimeLabelParameters(JFrame frame, JLabel time_JLabel) {
			return SetTimeLabelParam(frame, time_JLabel);
		}
		
		private static JPanel SetTimePanelParam(JFrame frame, JPanel timePanel_JPanel, GridBagConstraints gbc_time_panel) {
			//
			//
			//
			timePanel_JPanel = new JPanel();
			FlowLayout flowLayout = (FlowLayout) timePanel_JPanel.getLayout();
			flowLayout.setVgap(0);
			gbc_time_panel = new GridBagConstraints();
			gbc_time_panel.anchor = GridBagConstraints.EAST;
			gbc_time_panel.fill = GridBagConstraints.HORIZONTAL;
			gbc_time_panel.insets = new Insets(0, 9, 0, 5);
			gbc_time_panel.gridx = 0;
			gbc_time_panel.gridy = 12;
			frame.getContentPane().add(timePanel_JPanel, gbc_time_panel);
			return timePanel_JPanel;
		}
		public static JPanel TimePanelParameters(JFrame frame, JPanel timePanel_JPanel, GridBagConstraints gbc_time_panel) {
			return SetTimePanelParam(frame, timePanel_JPanel, gbc_time_panel);
		}
		
		private static JTextField SetTimeLeftTextFieldParam(JPanel timePanel_JPanel, JTextField timeLeft_TextField) {
			//
			// JTextField "время"
			//
			timeLeft_TextField = new JTextField();
			timeLeft_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			timeLeft_TextField.setToolTipText("введите значение"); // установка сообщения, выводящегося при наведении на элемент
			timeLeft_TextField.setColumns(3);
			timePanel_JPanel.add(timeLeft_TextField);
			return timeLeft_TextField;
		}
		public static JTextField TimeLeftTextFieldParameters(JPanel timePanel_JPanel, JTextField timeLeft_TextField) {
			return SetTimeLeftTextFieldParam(timePanel_JPanel, timeLeft_TextField);
		}
		
		private static JTextField SetTimeRightTextFieldParam(JPanel timePanel_JPanel, JTextField timeRight_TextField) {
			//
			// JTextField "время"
			//
			timeRight_TextField = new JTextField();
			timeRight_TextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
			timeRight_TextField.setToolTipText("введите значение"); // установка сообщения, выводящегося при наведении на элемент
			timeRight_TextField.setColumns(3);
			timePanel_JPanel.add(timeRight_TextField);
			return timeRight_TextField;
		}
		public static JTextField TimeRightTextFieldParameters(JPanel timePanel_JPanel, JTextField timeRight_TextField) {
			return SetTimeRightTextFieldParam(timePanel_JPanel, timeRight_TextField);
		}
		
		private static JButton SetAnimationAndStopButtonParam(JPanel timePanel_JPanel, JButton animationAndStopButton_JButton) {
			//
			// JButton "Анимация/Остановка"
			//
			animationAndStopButton_JButton = new JButton("Анимация");
			animationAndStopButton_JButton.setHorizontalAlignment(SwingConstants.TRAILING); // установка расположения по горизонтали
			animationAndStopButton_JButton.setToolTipText("нажмите, чтобы построить графики в реальном времени\n"); // установка сообщения,
																													// выводящегося при 
																													// наведении на элемент
			animationAndStopButton_JButton.addActionListener(Handlers.SetHandlerAnimationAndStop());
			timePanel_JPanel.add(animationAndStopButton_JButton);
			return animationAndStopButton_JButton;
		}
		public static JButton AnimationAndStopButtonParameters(JPanel timePanel_JPanel, JButton animationAndStopButton_JButton) {
			return SetAnimationAndStopButtonParam(timePanel_JPanel, animationAndStopButton_JButton);
		}
		
		private static JSeparator SetSeparatorButtomParam(JFrame frame, JSeparator separatorBottom_Separator) {
			//
			// JSeparator "нижняя разделительная черта"
			//
			separatorBottom_Separator = new JSeparator(); // инициализация элемента "разделительная черта"
			separatorBottom_Separator.setForeground(new Color(255, 140, 0)); // установка цвета 
			// Создание ограничений для "разделительной черты":
			GridBagConstraints gbc_separator_bottom = new GridBagConstraints();
			gbc_separator_bottom.fill = GridBagConstraints.HORIZONTAL; // установка типа заполнения элементом ячейки
			gbc_separator_bottom.insets = new Insets(0, 0, 0, 5); // установка отступов
			gbc_separator_bottom.gridx = 0; // установка координаты X, занимаемой ячейки
			gbc_separator_bottom.gridy = 13; // установка координаты Y, занимаемой ячейки
			frame.getContentPane().add(separatorBottom_Separator, gbc_separator_bottom); // добавление "разделительной черты" 
																						 // с её ограничениями на форму
			return separatorBottom_Separator;
		}
		public static JSeparator SeparatorButtomParameters(JFrame frame, JSeparator separatorBottom_Separator) {
			return SetSeparatorButtomParam(frame, separatorBottom_Separator);
		}
		
		private static JLabel SetLogLabelParam(JFrame frame, JLabel logLabel) {
			//
			// JLabel "текущая информация log"
			//
			logLabel = new JLabel("Текущая информация log:");
			logLabel.setFont(new Font("Lucida Grande", Font.BOLD, 13));
			GridBagConstraints gbc_log_label = new GridBagConstraints();
			gbc_log_label .anchor = GridBagConstraints.WEST;
			gbc_log_label .insets = new Insets(0, 12, 3, 5);
			gbc_log_label .gridx = 0;
			gbc_log_label .gridy = 14;
			frame.getContentPane().add(logLabel, gbc_log_label );
			return logLabel;
		}
		public static JLabel LogLabelParameters(JFrame frame, JLabel logLabel) {
			return SetLogLabelParam(frame, logLabel);
		}
		
		private static JScrollPane SetScrollPaneParam(JFrame frame, JScrollPane scrollPane, JList<String> logList_JList) {
			//
			//
			//
			
			scrollPane = new JScrollPane();
			scrollPane.setToolTipText("текущая информация log");
			scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
			GridBagConstraints gbc_scrollPane = new GridBagConstraints();
			gbc_scrollPane.gridheight = 3;
			gbc_scrollPane.insets = new Insets(0, 6, 0, 5);
			gbc_scrollPane.fill = GridBagConstraints.BOTH;
			gbc_scrollPane.gridx = 0;
			gbc_scrollPane.gridy = 15;
			frame.getContentPane().add(scrollPane, gbc_scrollPane);


			logList_JList = new JList<String>();
			logList_JList.setToolTipText("текущая информация log");
			logList_JList.setEnabled(false);
			logList_JList.setModel(new AbstractListModel<String>() {
				private static final long serialVersionUID = 1L;
				String[] values = new String[] {"2312312312312312312312312332123123123123123123123123", "12312313", "2", "3", "4", "5", "6", "7", "8", "9"};
				public int getSize() {
					return values.length; 
				}
				public String getElementAt(int index) {
					return values[index];
				}
			});

			scrollPane.setViewportView(logList_JList);
			

			
			//logList_JList = new JList<String>(words);
			//logList_JList.setEnabled(false);
			//scrollPane.setViewportView(logList_JList);

			return scrollPane;
		}
		public static JScrollPane ScrollPaneParameters(JFrame frame, JScrollPane scrollPane, JList<String> list) {
			return SetScrollPaneParam(frame, scrollPane, list);
		}
		
		private static JList<String> SetListParam(JScrollPane scrollPane, JList<String> list) {
			//
			//
			//
			
			DefaultListModel dlm = new DefaultListModel();
			dlm.setSize(100);
			dlm.addElement("12312313213213213213213213213213213213213212131321321321312");
			// add data
			list = new JList(dlm);
			
			//panel.add(new JScrollPane(list));
			
			//list = new JList<String>();
			list.setToolTipText("текущая информация log");
			list.setEnabled(false);
			/*list.setModel(new AbstractListModel<String>() {
				private static final long serialVersionUID = 1L;
				String[] values = new String[] {"2312312312312312312312312332123123123123123123123123", "12312313", "2", "3", "4", "5", "6", "7", "8", "9"};
				public int getSize() {
					return values.length; 
				}
				public String getElementAt(int index) {
					return values[index];
				}
			});*/

			scrollPane.setViewportView(list);
			return list;
		}
		public static JList<String> ListParameters(JScrollPane scrollPane, JList<String> list) {
			return SetListParam(scrollPane, list);
		}
		
		private static JButton SetClearButtonParam(JFrame frame, JButton clearButton_JButton) {
			
			clearButton_JButton = new JButton("Очистить");
			clearButton_JButton.setToolTipText("нажмите, чтобы очистить информацию log");
			GridBagConstraints gbc_clear_button = new GridBagConstraints();
			gbc_clear_button.anchor = GridBagConstraints.EAST;
			gbc_clear_button.insets = new Insets(3, 0, 3, 5);
			gbc_clear_button.gridx = 0;
			gbc_clear_button.gridy = 18;
			frame.getContentPane().add(clearButton_JButton, gbc_clear_button);
			return clearButton_JButton;
		}
		public static JButton ClearButtonParameters(JFrame frame, JButton clearButton_JButton) {
			return SetClearButtonParam(frame, clearButton_JButton);
		}
		
		private static JButton SetPlotButtonParam(JFrame frame, JButton plotButton_JButton) {			
			plotButton_JButton = new JButton("Построить");
			plotButton_JButton.setToolTipText("нажмите, чтобы построить графики");
			GridBagConstraints gbc_plot_button = new GridBagConstraints();
			gbc_plot_button.anchor = GridBagConstraints.WEST;
			gbc_plot_button.fill = GridBagConstraints.BOTH;
			gbc_plot_button.insets = new Insets(0, 6, 3, 5);
			gbc_plot_button.gridx = 0;
			gbc_plot_button.gridy = 19;
			plotButton_JButton.addActionListener(Handlers.SetHandlerPlotButton());
			frame.getContentPane().add(plotButton_JButton, gbc_plot_button);
			return plotButton_JButton;
		}
		public static JButton PlotButtonParameters(JFrame frame, JButton plotButton_JButton) {
			return SetPlotButtonParam(frame, plotButton_JButton);
		}
		
		/*private static JProgressBar SetProgressBarParam(JFrame frame, JProgressBar progressBar_JProgressBar) {		
			progressBar_JProgressBar = new JProgressBar();
			progressBar_JProgressBar.setToolTipText("прогресс выполнения в процентах");
			progressBar_JProgressBar.setValue(50);
			progressBar_JProgressBar.setStringPainted(true);
			GridBagConstraints gbc_progressBar = new GridBagConstraints();
			gbc_progressBar.fill = GridBagConstraints.HORIZONTAL;
			gbc_progressBar.gridwidth = 4;
			gbc_progressBar.insets = new Insets(0, 6, 5, 6);
			gbc_progressBar.gridx = 0;
			gbc_progressBar.gridy = 19;
			frame.getContentPane().add(progressBar_JProgressBar, gbc_progressBar);
			return progressBar_JProgressBar;
		}
		public static JProgressBar ProgressBarParameters(JFrame frame, JProgressBar progressBar_JProgressBar) {
			return SetProgressBarParam(frame, progressBar_JProgressBar);
		}*/

		private static JMenuBar SetMenuBarParam(JFrame frame, JMenuBar menuBar_JMenuBar) {		
			menuBar_JMenuBar = new JMenuBar();
			frame.setJMenuBar(menuBar_JMenuBar);
			return menuBar_JMenuBar;
		}
		public static JMenuBar MenuBarParameters(JFrame frame, JMenuBar menuBar_JMenuBar) {
			return SetMenuBarParam(frame, menuBar_JMenuBar);
		}
		
		private static JMenu SetMenuFileParam(JMenuBar menuBar_JMenuBar, JMenu menuFile_JMenu) {
			menuFile_JMenu = new JMenu("Файл");
			menuBar_JMenuBar.add(menuFile_JMenu);
			return menuFile_JMenu;
		}
		public static JMenu MenuFileParameters(JMenuBar menuBar_JMenuBar, JMenu menuFile_JMenu) {
			return SetMenuFileParam(menuBar_JMenuBar, menuFile_JMenu);
		}
		
		private static JMenuItem SetMenuItemAboutProgramParam(JMenu menuFile_JMenu, JMenuItem aboutProgram_JMenuItem) {
			//
			//
			//
			aboutProgram_JMenuItem = new JMenuItem("О программе\n");
			//mntmNewMenuItem_1.setFont(new Font("Lucida Grande", Font.PLAIN, 14));
			aboutProgram_JMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_DOWN_MASK));
			aboutProgram_JMenuItem.addActionListener(Handlers.SetHandlerMenuItemShowAboutButton());
			menuFile_JMenu.addSeparator(); // добавление разделительной черты
			menuFile_JMenu.add(aboutProgram_JMenuItem);		
			return aboutProgram_JMenuItem;
		}
		public static JMenuItem MenuItemAboutProgramParameters(JMenu menuFile_JMenu, JMenuItem aboutProgram_JMenuItem) {
			return SetMenuItemAboutProgramParam(menuFile_JMenu, aboutProgram_JMenuItem);
		}
		
		private static JMenuItem SetMenuItemExitProgramParam(JMenu menuFile_JMenu, JMenuItem exitProgram_JMenuItem) {
			//
			//
			//
			exitProgram_JMenuItem = new JMenuItem("Выход");
			exitProgram_JMenuItem.addActionListener(Handlers.SetHandlerMenuItemExitButton());
			menuFile_JMenu.addSeparator(); // добавление разделительной черты
			menuFile_JMenu.add(exitProgram_JMenuItem);	
			return exitProgram_JMenuItem;
		}
		public static JMenuItem MenuItemExitProgramParameters(JMenu menuFile_JMenu, JMenuItem exitProgram_JMenuItem) {
			return SetMenuItemExitProgramParam(menuFile_JMenu, exitProgram_JMenuItem);
		}
		
		private static JMenu SetMenuReportParam(JMenuBar menuBar_JMenuBar, JMenu menuReport_JMenu) {
			menuReport_JMenu = new JMenu("Отчёт");
			menuReport_JMenu.addMenuListener(Handlers.SetHandlerMenuReportButton());
			menuBar_JMenuBar.add(menuReport_JMenu);
			return menuReport_JMenu;
		}
		public static JMenu MenuReportParameters(JMenuBar menuBar_JMenuBar, JMenu menuReport_JMenu) {
			return SetMenuReportParam(menuBar_JMenuBar, menuReport_JMenu);
		}
		
		private static JMenuItem SetMenuItemSaveParam(JMenu menuReport_JMenu, JMenuItem save_JMenuItem) {
			//
			//
			//
			save_JMenuItem = new JMenuItem("Сохранить");
			save_JMenuItem.addActionListener(Handlers.SetHandlerMenuItemSaveButton());
			save_JMenuItem.setEnabled(false);
			menuReport_JMenu.add(save_JMenuItem);
			return save_JMenuItem;
		}
		public static JMenuItem MenuItemSaveParameters(JMenu menuReport_JMenu, JMenuItem save_JMenuItem) {
			return SetMenuItemSaveParam(menuReport_JMenu, save_JMenuItem);
		}
		
		private static JMenuItem SetMenuItemSaveAsParam(JMenu menuReport_JMenu, JMenuItem saveAs_JMenuItem) {
			//
			//
			//
			saveAs_JMenuItem = new JMenuItem("Сохранить как");
			saveAs_JMenuItem.addActionListener(Handlers.SetHandlerMenuItemSaveAsButton());
			saveAs_JMenuItem.setEnabled(false);
			menuReport_JMenu.add(saveAs_JMenuItem);
			menuReport_JMenu.addSeparator();
			return saveAs_JMenuItem;
		}
		public static JMenuItem MenuItemSaveAsParameters(JMenu menuReport_JMenu, JMenuItem saveAs_JMenuItem) {
			return SetMenuItemSaveAsParam(menuReport_JMenu, saveAs_JMenuItem);
		}
		
		private static JMenuItem SetMenuItemPrintParam(JMenu menuReport_JMenu, JMenuItem print_JMenuItem) {
			//
			//
			//
			print_JMenuItem = new JMenuItem("Печать");
			print_JMenuItem.addActionListener(Handlers.SetHandlerMenuItemPrintButton());
			print_JMenuItem.setEnabled(false);
			menuReport_JMenu.add(print_JMenuItem);
			return print_JMenuItem;
		}
		public static JMenuItem MenuItemPrintParameters(JMenu menuReport_JMenu, JMenuItem print_JMenuItem) {
			return SetMenuItemPrintParam(menuReport_JMenu, print_JMenuItem);
		}
		
		private static JMenu SetMenuUserFunctionParam(JMenuBar menuBar_JMenuBar, JMenu menuUserFunction_JMenu) {
			menuUserFunction_JMenu = new JMenu("Пользовательская функция");
			menuUserFunction_JMenu.addMenuListener(Handlers.SetHandlerMenuUserFunctionButton());
			menuUserFunction_JMenu.addMouseListener(Handlers.SetHandlerMouseClickedMenuUserFunction());
			menuBar_JMenuBar.add(menuUserFunction_JMenu);
			return menuUserFunction_JMenu;
		}
		public static JMenu MenuUserFunctionParameters(JMenuBar menuBar_JMenuBar, JMenu menuUserFunction_JMenu) {
			return SetMenuUserFunctionParam(menuBar_JMenuBar, menuUserFunction_JMenu);
		}
		
		private static ChartPanel SetFirstPanelParam(JFrame frame, ChartPanel firstPanel_ChartPanel, JFreeChart firstChart_JFreeChart) {
			//
			//
			//
			firstPanel_ChartPanel = new ChartPanel(firstChart_JFreeChart);
			firstPanel_ChartPanel.setToolTipText("");
			firstPanel_ChartPanel.setVisible(true);
			firstPanel_ChartPanel.setEnabled(true);
			GridBagConstraints gbc_panel_1 = new GridBagConstraints();		
			gbc_panel_1.anchor = GridBagConstraints.NORTH;
			gbc_panel_1.insets = new Insets(3, 0, 5, 20);
			gbc_panel_1.gridx = 2;
			gbc_panel_1.gridy = 0;
			gbc_panel_1.gridwidth = 2;
			gbc_panel_1.gridheight = 7;
			gbc_panel_1.fill = GridBagConstraints.BOTH;
			frame.getContentPane().add(firstPanel_ChartPanel, gbc_panel_1);
			return firstPanel_ChartPanel;
		}
		public static ChartPanel FirstPanelParameters(JFrame frame, ChartPanel firstPanel_ChartPanel, JFreeChart firstChart_JFreeChart) {
			return SetFirstPanelParam(frame, firstPanel_ChartPanel, firstChart_JFreeChart);
		}
		
		private static ChartPanel SetSecondPanelParam(JFrame frame, ChartPanel secondPanel_ChartPanel, JFreeChart secondChart_JFreeChart) {
			//
			//
			//
			secondPanel_ChartPanel = new ChartPanel(secondChart_JFreeChart);
			secondPanel_ChartPanel.setToolTipText("");
			secondPanel_ChartPanel.setVisible(true);
			secondPanel_ChartPanel.setEnabled(true);
			GridBagConstraints gbc_panel_2 = new GridBagConstraints();
			gbc_panel_2.insets = new Insets(0, 0, 5, 20);
			gbc_panel_2.gridx = 2;
			gbc_panel_2.gridy = 7;
			gbc_panel_2.gridwidth = 2;
			gbc_panel_2.gridheight = 7;
			gbc_panel_2.fill = GridBagConstraints.BOTH;
			frame.getContentPane().add(secondPanel_ChartPanel, gbc_panel_2);
			return secondPanel_ChartPanel;
		}
		public static ChartPanel SecondPanelParameters(JFrame frame, ChartPanel secondPanel_ChartPanel, JFreeChart secondChart_JFreeChart) {
			return SetSecondPanelParam(frame, secondPanel_ChartPanel, secondChart_JFreeChart);
		}
		
		private static ChartPanel SetThirdPanelParam(JFrame frame, ChartPanel thirdPanel_ChartPanel, JFreeChart thirdChart_JFreeChart) {
			//
			//
			//
			thirdPanel_ChartPanel = new ChartPanel(thirdChart_JFreeChart);
			thirdPanel_ChartPanel.setToolTipText("");
			thirdPanel_ChartPanel.setVisible(true);
			thirdPanel_ChartPanel.setEnabled(true);
			GridBagConstraints gbc_panel_3 = new GridBagConstraints();
			gbc_panel_3.anchor = GridBagConstraints.EAST;
			gbc_panel_3.insets = new Insets(0, 0, 5, 20);
			gbc_panel_3.gridx = 2;
			gbc_panel_3.gridy = 14;
			gbc_panel_3.gridwidth = 2;
			gbc_panel_3.gridheight = 6;
			gbc_panel_3.fill = GridBagConstraints.BOTH;
			frame.getContentPane().add(thirdPanel_ChartPanel, gbc_panel_3);
			return thirdPanel_ChartPanel;
		}
		public static ChartPanel ThirdPanelParameters(JFrame frame, ChartPanel thirdPanel_ChartPanel, JFreeChart thirdChart_JFreeChart) {
			return SetThirdPanelParam(frame, thirdPanel_ChartPanel, thirdChart_JFreeChart);
		}
		
		/*public static String[] InputValuesPrameters(String[] values) {
			return values = Window_GUI.UpdateInputValues(values);
		}*/
		
		public static ChartPanel[] InputPanelsParameters(ChartPanel[] panels, ChartPanel firstPanel_ChartPanel, ChartPanel secondPanel_ChartPanel, ChartPanel thirdPanel_ChartPanel) {
		    return panels =  new ChartPanel[] {firstPanel_ChartPanel, secondPanel_ChartPanel, thirdPanel_ChartPanel};
		}
		
		/*public static FunctionData InputDataParameters(FunctionData data) {
			return data = Window_GUI.UpdateInputData(data);
		}*/

}

 
