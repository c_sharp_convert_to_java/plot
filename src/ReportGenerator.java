import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;

/* Класс, использующийся для генерации отчетов.
 * В частности, предоставляет статический метод generateReport, 
 * отвечающий за создание файла и вызов приватных методов записи информации в файл.
 */


/* todo: 
 * mode = boolean
 * model type in reports
*/

public class ReportGenerator {
	/* generateReport используется для создания отчета.
	 * Метод generateReport - перегружаемый.
	 * При использовании опций "Печать" и "Сохранить" никакие другие параметры не необходимы.
	 * Использование int в качестве первого параметра означает использование одной из этих опций, 0 для "Сохранить" и 1 для "Печать".
	 * При выборе опции "Сохранить как" помимо выбора имени файла пользователь также может выбрать формат.
	 * Доступные форматы - docx (используется по-умолчанию в других режимах) и комбинация txt + png.
	 * 
	 * В качестве параметров также необходимо передать информацию о функции, график которой построен, и непосредственно сами графики.
	 * 
	 * Подробнее о работе метода с файлами в разных режимах - в теле метода.
	 * После создания файла запись в файл производится с использованием метода writeReport.
	 */
	static public boolean generateReport (int mode, FunctionData function) {
		File f, img1, img2, img3;
		File [] files;
		try {
			
			
			switch (mode) {
			
			/* "Сохранить"
			 * Отчет сохраняется в документ "report.docx".
			 * Если документ существует, то отчет записывается в него, перезаписывая старые данные.
			 */
			
			case 0:	
				f = new File("reports"+File.separator+"report.docx");
				img1 = new File("temp"+File.separator+"temp1.png");
				img2= new File("temp"+File.separator+"temp2.png");
				img3 = new File("temp"+File.separator+"temp3.png");
				if (!f.getParentFile().exists())
					f.getParentFile().mkdirs();
				if (!img1.getParentFile().exists())
					img1.getParentFile().mkdirs();
				if (f.exists()) {
					f.delete();
					img1.delete();
					img2.delete();
					img3.delete();
				}
				f.createNewFile();
				img1.createNewFile();
				img2.createNewFile();
				img3.createNewFile();
				files = new File[]{f, img1, img2, img3};
				if (!writeReportDOCX(files, function))
					return false;
				break;
				
				
			/* "Печать"
			 * Удаляется "temp.docx",  если документ с таким именем существует.
			 * Отчет сохраняется в документ "temp.docx".
			 * Открывается системное окно печати.
			 * При закрытии программы файл "temp.docx" удаляется.
			 */
				
			case 1:
				f = new File("temp"+File.separator+"temp.docx");
				img1 = new File("temp"+File.separator+"temp1.png");
				img2= new File("temp"+File.separator+"temp2.png");
				img3 = new File("temp"+File.separator+"temp3.png");
				if (!f.getParentFile().exists())
					f.getParentFile().mkdirs();
				if (!img1.getParentFile().exists())
					img1.getParentFile().mkdirs();
				if (f.exists() || img1.exists() || img3.exists() || img2.exists()) {
					f.delete();
					img1.delete();
					img2.delete();
					img3.delete();
				}
				f.createNewFile();
				img1.createNewFile();
				img2.createNewFile();
				img3.createNewFile();
				files = new File[] {f, img1, img2, img3};	
				f.deleteOnExit();
				img1.deleteOnExit();
				img2.deleteOnExit();
				img3.deleteOnExit();
				if (!writeReportDOCX(files, function))
					return false;
				Desktop.getDesktop().print(f);
				break;
				
				
			default:
				CustomMessage.errorMessage("Внутренняя ошибка: выбран неверный параметр ReportGenerator.");
				return false;
			}
			
		}
			catch (IOException e) {
				CustomMessage.errorMessage(2);
				return false;
			}
		
		return true;
	}
	
	static public boolean generateReport (String name, int mode, FunctionData function, JFreeChart[] charts) {
		File f, img1, img2, img3;
		File [] files;
		/* "Сохранить как"
		 * Отчет сохраняется в документ с заданным именем и расширением.
		 * Если документ существует, возвращается ошибка.
		 */
		
		try {
			switch (mode) {
			// Формат - docx
			case 0:
				f = new File("reports"+File.separator+name+".docx");
				img1 = new File("temp"+File.separator+"temp1.png");
				img2= new File("temp"+File.separator+"temp2.png");
				img3 = new File("temp"+File.separator+"temp3.png");
				
				if (!f.getParentFile().exists())
					f.getParentFile().mkdirs();
				
				if (!img1.getParentFile().exists())
					img1.getParentFile().mkdirs();
				
				if (f.exists())
					if (CustomMessage.yesNoMessage("Файл уже существует.\nХотите перезаписать?")) {
						f.delete();
	
					}
					else
						return false;
				
				f.createNewFile();
				img1.createNewFile();
				img2.createNewFile();
				img3.createNewFile();
				img1.deleteOnExit();
				img2.deleteOnExit();
				img3.deleteOnExit();
				files = new File[] {f, img1, img2, img3};	
				if(!writeReportDOCX(files, function))
					return false;
				break;
			//Формат - txt + png
			case 1:
				f = new File("reports"+File.separator+name+".txt");
				img1 = new File("reports"+File.separator+name+"1.png");
				img2= new File("reports"+File.separator+name+"2.png");
				img3 = new File("reports"+File.separator+name+"3.png");
				if (!f.getParentFile().exists())
					f.getParentFile().mkdirs();
				
				if (f.exists() || img1.exists() || img3.exists() || img2.exists()) {
					if (CustomMessage.yesNoMessage("Файл уже существует.\nХотите перезаписать?")) {
						f.delete();
						img1.delete();
						img2.delete();
						img3.delete();
					}
					else
						return false;
				}
				f.createNewFile();
				img1.createNewFile();
				img2.createNewFile();
				img3.createNewFile();
				files = new File[] {f, img1, img2, img3};
				if(!writeReportTXT(files, function))
					return false;
				break;
			//Формат - pdf
			case 2:
				f = new File("reports"+File.separator+name+".pdf");
				img1 = new File("temp"+File.separator+"temp1.png");
				img2= new File("temp"+File.separator+"temp2.png");
				img3 = new File("temp"+File.separator+"temp3.png");
				
				if (!f.getParentFile().exists())
					f.getParentFile().mkdirs();
				
				if (!img1.getParentFile().exists())
					img1.getParentFile().mkdirs();
				
				if (f.exists())
					if (CustomMessage.yesNoMessage("Файл уже существует.\nХотите перезаписать?")) {
						f.delete();
	
					}
					else
						return false;
				
				f.createNewFile();
				img1.createNewFile();
				img2.createNewFile();
				img3.createNewFile();
				files = new File[] {f, img1, img2, img3};	
				if(!writeReportPDF(files, function))
					return false;
				break;
				
			default:
				CustomMessage.errorMessage("Внутренняя ошибка: выбран неверный параметр ReportGenerator.");
				return false;
			}
		}
		catch (IOException e) {
			CustomMessage.errorMessage(2);
			return false;
		}

		return true; 
	}
	
	/* Методы writeReportDOCX и writeReportTXT используются для непосредственной работы с файлом, созданным ранее в методе generateReport.
	 * При работе методов используется метод getCurrentTime класса CurrentTime.
	 */
	
	/* Метод writeReportDOCX использует библиотеки, предоставленные в рамках проекта Apache POI.
	 * В результате метода в файл записывается информация о функции (в виде таблиц), а также построенные графики в формате изображения.
	 */

	static private boolean writeReportDOCX(File[] files, FunctionData function) {
		try {
			CustomXWPFDocument document = new CustomXWPFDocument(/*new FileInputStream(files[])*/);
		    FileOutputStream out = new FileOutputStream (files[0]);
		    
			ChartUtilities.saveChartAsPNG(files[1], function.getPanel_1().getChart(), 600, 200);
			ChartUtilities.saveChartAsPNG(files[2], function.getPanel_2().getChart(), 600, 200);
			ChartUtilities.saveChartAsPNG(files[3], function.getPanel_3().getChart(), 600, 200);
			
			FileInputStream img1s = new FileInputStream(files[1]);
			String blipId1 = document.addPictureData(img1s, Document.PICTURE_TYPE_PNG);		
			
			FileInputStream img2s = new FileInputStream(files[2]);
			String blipId2 = document.addPictureData(img2s, Document.PICTURE_TYPE_PNG);		
			
			FileInputStream img3s = new FileInputStream(files[3]);
			String blipId3 = document.addPictureData(img3s, Document.PICTURE_TYPE_PNG);		

			int num = (document.getNextPicNameNumber(Document.PICTURE_TYPE_PNG));
			
			
		    XWPFParagraph p = document.createParagraph();
		    // p1.setAlignment(ParagraphAlignment.CENTER);
		    XWPFRun run1 = p.createRun();
		    run1.setBold(true);
		    run1.setItalic(true);
		    run1.setFontSize(16);
		    run1.setFontFamily("Times New Roman");
		    run1.setText("Отчет сгенерирован: " + CurrentTime.getCurrentTime());
		    run1.addBreak();
		    
		    XWPFRun run2 = p.createRun();
		    run2.setFontSize(14);
		    run2.setFontFamily("Times New Roman");
		    run2.setText("Информация о функции:");
		    
		    p = document.createParagraph();
		    XWPFRun newrun = p.createRun();
		    newrun.setFontSize(14);
		    newrun.setFontFamily("Times New Roman");
		    if (function.getCheckMethod())
				newrun.setText("Модель следования за лидером");
			else 
				newrun.setText("Модель пастуха");
		    newrun.addBreak();
		    
		    if (function.getFunc() != null) {
		    	p = document.createParagraph();
			    XWPFRun newerrun = p.createRun();
			    newerrun.setFontSize(14);
			    newerrun.setFontFamily("Times New Roman");
			    newerrun.setText("Пользовательская функция: " + function.getFunc());
			    newerrun.addBreak();
		    }
		    
		    XWPFRun run3 = p.createRun();
		    run3.setFontSize(14);
		    run3.setFontFamily("Times New Roman");
		    run3.setText("Число частиц");
		    
		    XWPFTable table = document.createTable();
		    table.setWidth(500);
		    XWPFTableRow tableRowOne = table.getRow(0);
		    tableRowOne.getCell(0).setText("N");
		    XWPFTableRow tableRowTwo = table.createRow();
		    tableRowTwo.getCell(0).setText(Double.toString(function.getN()));

		    
		    p = document.createParagraph();
		    XWPFRun run4 = p.createRun();
		    run4.addBreak();
		    run4.setFontSize(14);
		    run4.setFontFamily("Times New Roman");
		    run4.setText("Начальные условия");
		    
		    XWPFTable table2 = document.createTable();
		    table2.setWidth(1000);
		    XWPFTableRow table2RowOne = table2.getRow(0);
		    table2RowOne.getCell(0).setText("t0");
		    table2RowOne.addNewTableCell().setText("y0");
		    XWPFTableRow table2RowTwo = table2.createRow();
		    table2RowTwo.getCell(0).setText(Double.toString(function.getT0()));
		    table2RowTwo.getCell(1).setText(Double.toString(function.getY0()));
		    
		    p = document.createParagraph();
		    XWPFRun run5 = p.createRun();
		    run5.addBreak();
		    run5.setFontSize(14);
		    run5.setFontFamily("Times New Roman");
		    run5.setText("Ограничения");
		    
		    XWPFTable table3 = document.createTable();
		    table3.setWidth(1500);
		    XWPFTableRow table3RowOne = table3.getRow(0);
		    table3RowOne.getCell(0).setText("M1");
		    table3RowOne.addNewTableCell().setText("M2");
		    table3RowOne.addNewTableCell().setText("M3");
		    XWPFTableRow table3RowTwo = table3.createRow();
		    table3RowTwo.getCell(0).setText(Double.toString(function.getM1()));
		    table3RowTwo.getCell(1).setText(Double.toString(function.getM2()));
		    table3RowTwo.getCell(2).setText(Double.toString(function.getM3()));
		    
		    p = document.createParagraph();
		    XWPFRun run6 = p.createRun();
		    run6.addBreak();
		    run6.setFontSize(14);
		    run6.setFontFamily("Times New Roman");
		    run6.setText("Коэффициенты");
		    
		    XWPFTable table4 = document.createTable();
		    table4.setWidth(1500);
		    XWPFTableRow table4RowOne = table4.getRow(0);
		    table4RowOne.getCell(0).setText("c0");
		    table4RowOne.addNewTableCell().setText("c1");
		    table4RowOne.addNewTableCell().setText("c2");
		    
		    XWPFTableRow table4RowTwo = table4.createRow();
		    table4RowTwo.getCell(0).setText(Double.toString(function.getC0()));
		    table4RowTwo.getCell(1).setText(Double.toString(function.getC1()));
		    table4RowTwo.getCell(2).setText(Double.toString(function.getC2()));
		    
		    run6.addBreak();
		    
		    document.createPicture(blipId1,num, 600, 200);
		    p = document.createParagraph();
		    p.setAlignment(ParagraphAlignment.CENTER);
		    XWPFRun run7 = p.createRun();
		    run7.setText("График координат");
		    
		    
		    document.createPicture(blipId2,num+1, 600, 200);
		    p = document.createParagraph();
		    p.setAlignment(ParagraphAlignment.CENTER);
		    XWPFRun run8 = p.createRun();
		    run8.setText("График скорости");
		    
		    
		    document.createPicture(blipId3,num+2, 600, 200);
		    p = document.createParagraph();
		    p.setAlignment(ParagraphAlignment.CENTER);
		    XWPFRun run9 = p.createRun();
		    run9.setText("График ускорения");
		    
		    document.write(out);
		    
		    out.close();
		    document.close();
		    img1s.close();
		    img2s.close();
		    img3s.close();
		    
		    files[1].delete();
		    files[2].delete();
		    files[3].delete();
		} catch (IOException e) {
			CustomMessage.errorMessage(3);
			return false;
		}
		catch (InvalidFormatException e) {
			return false;
		}
		
		return true;
	}
	
	/* Метод writeReportTXT записывает отчет в txt-файл, переданный в качестве параметра.
	 * Затем создаются одноименные файлы с расширением .png, в которые сохраняются изображения графиков.
	 * Использование этого метода рекомендуется только в случае отсутствия возможности работы с docx файлами.
	 */
	static private boolean writeReportTXT (File[] files, FunctionData function) {
		try {
			String buffer;

			buffer = "Отчет сгенерирован: " + CurrentTime.getCurrentTime() + System.lineSeparator() +"Информация о функции:" + System.lineSeparator();
			if (function.getCheckMethod())
				buffer += "Модель следования за лидером" + System.lineSeparator();
			else 
				buffer += "Модель пастуха" + System.lineSeparator();
			if (function.getFunc()!= null) {
				buffer +="Пользовательская функция: " + function.getFunc() + System.lineSeparator();
			}
			buffer += "Число частиц" + System.lineSeparator();
			buffer += "N = " + Double.toString(function.getN()) + System.lineSeparator();
			buffer += "Начальные условия" + System.lineSeparator();
			buffer += "t0 = " + Double.toString(function.getT0()) + " y0 = " + Double.toString(function.getY0()) + System.lineSeparator();
			buffer += "Ограничения" + System.lineSeparator();
			buffer += "M1 = " + Double.toString(function.getM1()) + " M2 = " + Double.toString(function.getM2()) + " M3 = " + Double.toString(function.getM3()) + System.lineSeparator();
			buffer += "Коэффициенты" + System.lineSeparator();
			buffer += "C0 = " + Double.toString(function.getC0()) + " C1 = " + Double.toString(function.getC1()) + " C2 = " + Double.toString(function.getC2()) + System.lineSeparator();
			
			Files.write(files[0].toPath(), buffer.getBytes());
			
			ChartUtilities.saveChartAsPNG(files[1], function.getPanel_1().getChart(), 600, 200);
			ChartUtilities.saveChartAsPNG(files[2], function.getPanel_2().getChart(), 600, 200);
			ChartUtilities.saveChartAsPNG(files[3], function.getPanel_3().getChart(), 600, 200);
			
		} catch (IOException e) {
			CustomMessage.errorMessage(3);
			return false;
		}		
		
		return true;		
	}


	static private boolean writeReportPDF (File[] files, FunctionData function) {

		return PDFReportWriter.writeReportPDF(files, function);
	}
}
//	deprecated code

//		for (int i = 1; ;i++) {
//			if(f.exists()) { //проверка доступности имени файла
//			    number = Integer.toString(i);
//			    f.renameTo(new File("reports"+File.separator+"report"+number+".docx"));
//			}
//			else break;
//		}
//
