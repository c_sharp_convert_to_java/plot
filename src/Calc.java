//package kursovaya;

import java.util.ArrayList;

import org.jfree.chart.ChartPanel;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class Calc { //входные параметры (заполняются перед вызовом метода process)
/*    private int n;
    private int t0;
    private int t1;
    private int t2;
    private int y0;

    private double M0;
    private double M1;
    private double M2;
    private double M3;

    private double c0;
    private double c1;
    private double c2;

    private int model;

    public static final int PASTUH = 0;
    public static final int LIDER = 1;
*/
//выходные параметры (результаты вычислений)
	private static String func;

    public ArrayList<String> logList = new ArrayList<>();

    public static void process(FunctionData function) { // функция вычисления
        double[] X;
        double[][] Y1, Ypr1, Ypr2;
        
        if (function.getFunc() != null)
           	func = function.getFunc();
        else
        	func = null;
        
        int times = function.getT2() - function.getT1() + 1;

        X = new double[times];
        Y1 = new double[function.getN() + 1][];
        Ypr1 = new double[function.getN() + 1][];
        Ypr2 = new double[function.getN() + 1][];
        if (function.getCheckMethod())
        {
            int t;

            CubicSpline aa;
            Y1[0] = new double[times];
            Y1[1] = new double[times];

            int index = 0;
            for (t = function.getT1(); t <= function.getT2(); t++, index++)
            {
                X[index] = t;
                Y1[0][index] = f((double)t);

                double y = solveoderungekutta(function.getC0(), function.getC1(), function.getC2(), function.getT0(), function.getY0(), t);


                Y1[1][index] = y;




            }
            double[] X1;
            double[] Y2;

            //если начинаем не с нуля
            if (function.getT1() != 0)
            {
                X1 = new double[X.length + 1];
                X1[0] = 0;
                for (int j = 0; j < times; j++)
                {
                    X1[j + 1] = X[j];
                }
                Y2 = new double[times + 1];

            }
            else
            {
                X1 = new double[X.length];
                for (int j = 0; j < times; j++)
                {
                    X1[j] = X[j];
                }
                Y2 = new double[times];


            }
            for (int i = 2; i < function.getN(); i++)
            {
                Y1[i] = new double[times];
                if (function.getT1() != 0)
                {
                    Y2[0] = -12 * (i - 1);
                    for (int j = 0; j < times; j++)
                    {
                        Y2[j + 1] = Y1[i - 1][j];
                    }
                }
                else
                {
                    for (int j = 0; j < times; j++)
                    {
                        Y2[j] = Y1[i - 1][j];
                    }
                }
                aa = new CubicSpline();
                aa.BuildSpline(X1, Y2, X1.length);
                int index1 = 0;
                for (int j = function.getT1(); j <= function.getT2(); j++, index1++)
                {
                    double y1 = solveoderungekutta2(function.getC0(), function.getC1(), function.getC2(), 0, -12 * (i - 1), j, aa, 0);
                    Y1[i][index1] = y1;
                }
            }

            //подсчет производных ================

            //первая производная


            for (int i = 0; i < function.getN(); i++)
            {
                Ypr1[i] = new double[Y1[i].length];
                aa = new CubicSpline();
                aa.BuildSpline(X, Y1[i], X.length);
                int index2 = 0;
                for (int j = function.getT1(); j <= function.getT2(); j++, index2++)
                {
                    Ypr1[i][index2] = df6points1_spline(0.001, j, aa);
                        if (Ypr1[i][index2] > function.getM1())
                        	function.getLogList().addElement("Скорость частицы "+(i+1)+" в момент времени "+j+" больше значения M1="+function.getM1());
                        if(Ypr1[i][index2] < 0)
                        	function.getLogList().addElement("Скорость частицы "+(i+1)+" в момент времени "+j+" меньше нуля");

                }
            }

            //вторая производная

            for (int i = 0; i < function.getN(); i++)
            {
                Ypr2[i] = new double[Ypr1[i].length];
                aa = new CubicSpline();
                aa.BuildSpline(X, Ypr1[i], X.length);
                int index2 = 0;
                for (int j = function.getT1(); j <= function.getT2(); j++, index2++)
                {
                    Ypr2[i][index2] = df6points1_spline(0.0001, j, aa);

                }
            }
            ChartPanel[] panels = {function.getPanel_1(), function.getPanel_2(), function.getPanel_3()};
            Builder.build(function.getN(), function.getM1(), function.getM2(), function.getM3(), function.getCheckMethod(),
            		X, Y1, Ypr1, Ypr2, panels);
            return;
        }
        if (!function.getCheckMethod())
        {
            int t;

            CubicSpline aa;
            Y1[0] = new double[times];
            Y1[1] = new double[times];

            int index = 0;
            for (t = function.getT1(); t <= function.getT2(); t++, index++)
            {
                X[index] = t;
                Y1[0][index] = f((double)t);


                Y1[1][index] = f((double)t) + function.getC0() + function.getC1() * df6points1(0.01, X[index]) + function.getC2() * df6points1(0.01, X[index]) * df6points1(0.01, X[index]);


            }
            double[] X1;
            double[] Y2;

            //если начинаем не с нуля
            if (function.getT1() != 0)
            {
                X1 = new double[X.length + 1];
                X1[0] = 0;
                for (int j = 0; j < times; j++)
                {
                    X1[j + 1] = X[j];
                }
                Y2 = new double[times + 1];

            }
            else
            {
                X1 = new double[X.length];
                for (int j = 0; j < times; j++)
                {
                    X1[j] = X[j];
                }
                Y2 = new double[times];


            }
            for (int i = 2; i < function.getN(); i++)
            {
                Y1[i] = new double[times];
                if (function.getT1() != 0)
                {
                    Y2[0] = -10 * (i - 1);
                    for (int j = 0; j < times; j++)
                    {
                        Y2[j + 1] = Y1[i - 1][j];
                    }
                }
                else
                {
                    for (int j = 0; j < times; j++)
                    {
                        Y2[j] = Y1[i - 1][j];
                    }
                }
                aa = new CubicSpline();
                aa.BuildSpline(X1, Y2, X1.length);
                int index1 = 0;
                for (int j = function.getT1(); j <= function.getT2(); j++, index1++)
                {

                    Y1[i][index1] = aa.Interpolate(j) + function.getC0() + function.getC1() * df6points1_spline(0.001, j, aa) + function.getC2() * df6points1_spline(0.001, j, aa) * df6points1_spline(0.001, j, aa);

                }
            }

            //подсчет производных ================

            //первая производная


            for (int i = 0; i < function.getN(); i++)
            {
                Ypr1[i] = new double[Y1[i].length];
                aa = new CubicSpline();
                aa.BuildSpline(X, Y1[i], X.length);
                int index2 = 0;
                for (int j = function.getT1(); j <= function.getT2(); j++, index2++)
                {
                    Ypr1[i][index2] = df6points1_spline(0.001, j, aa);
                    if (Ypr1[i][index2] > function.getM1())
                        function.getLogList().addElement("Скорость частицы " + (i + 1) + " в момент времени " + j + " больше значения M1=" + function.getM1());
                    if (Ypr1[i][index2] < 0)
                    	function.getLogList().addElement("Скорость частицы " + (i + 1) + " в момент времени " + j + " меньше нуля");

                }
            }

            //вторая производная

            for (int i = 0; i < function.getN(); i++)
            {
                Ypr2[i] = new double[Ypr1[i].length];
                aa = new CubicSpline();
                aa.BuildSpline(X, Ypr1[i], X.length);
                int index2 = 0;
                for (int j = function.getT1(); j <= function.getT2(); j++, index2++)
                {
                    Ypr2[i][index2] = df6points1_spline(0.001, j, aa);

                }
            }
            ChartPanel[] panels = {function.getPanel_1(), function.getPanel_2(), function.getPanel_3()};
            Builder.build(function.getN(), function.getM1(), function.getM2(), function.getM3(), function.getCheckMethod(),
            		X, Y1, Ypr1, Ypr2, panels);
            return;
        }
    }

    
    private static double f (double x) {
    	if (func != null) {
    		try {
		    	Expression e = new ExpressionBuilder(func)
		    			.variable("x")
		    			.build()
		    			.setVariable("x", x);
		    	return e.evaluate();  	
    		}
    		catch (RuntimeException e) {
    			CustomMessage.errorMessage(4);
    			throw new RuntimeException("Ошибка при вычислении пользовательской функции");
    		}
    	}
    	else return x*17;
    }
    private static double solveoderungekutta(double c0, double c1, double c2, double _x, double _y0, double _x1)
    {
        double b = _x1;
        double x0 = _x;
        double y0 = _y0;
        double h1;
        double y = _y0;
        double h0 = 0.01;
        int n = (int)((b - x0) / h0 + 1);
        for (int i = 1; i < n; i++)
        {
            h1 = h0;
            y = r(c0, c1, c2, x0, y0, h1);


            x0 = x0 + h0;
            y0 = y;


        }
        return y;
    }

    private static double solveoderungekutta2(double c0, double c1, double c2, double _x, double _y0, double _x1, CubicSpline aa, int t1)
    {
        double b = _x1;
        double x;
        double x0 = _x;
        double y0 = _y0, y1;
        double h1;
        double y = _y0;
        double eps = 0.01, h0 = 0.01;
        int n = (int)((b - x0) / h0 + 1);
        int m;
        for (int i = 1; i < n; i++)
        {
            h1 = h0;
            m = 1;
            y = r2(c0, c1, c2, x0, y0, h1, m, aa);
            x0 = x0 + h0;
            y0 = y;
        }
        return y;
    }

    private static double r(double c0, double c1, double c2, double x, double y, double h)
    {
        double k1 = funcG(c0, c1, c2, x, y);
        double k2 = funcG(c0, c1, c2, x + h / 2, y + h * k1 / 2);
        double k3 = funcG(c0, c1, c2, x + h / 2, y + h * k2 / 2);
        double k4 = funcG(c0, c1, c2, x + h, y + h * k3);
        return y + h * (k1 + 2 * k2 + 2 * k3 + k4) / 6;
    }

    private static double r2(double c0, double c1, double c2, double x, double y, double h, double m, CubicSpline aa)
    {
        double k1 = funcG2(c0, c1, c2, x, y, aa);
        double k2 = funcG2(c0, c1, c2, x + h / 2, y + h * k1 / 2, aa);
        double k3 = funcG2(c0, c1, c2, x + h / 2, y + h * k2 / 2, aa);
        double k4 = funcG2(c0, c1, c2, x + h, y + h * k3, aa);
        return y + h * (k1 + 2 * k2 + 2 * k3 + k4) / 6;
    }

    private static double funcG(double c0, double c1, double c2, double x, double y)
    {
        return (-c1 + Math.sqrt(c1 * c1 - 4 * c2 * (c0 + y - f(x)))) / (2 * c2);
    }

    private static double funcG2(double c0, double c1, double c2, double x, double y, CubicSpline aa)
    {
        return (-c1 + Math.sqrt(c1 * c1 - 4 * c2 * (c0 + y - aa.Interpolate(x)))) / (2 * c2);
    }

    private static double df6points1(double h, double x)
    {
        return (9d / 580d * f(x - 2.5d * h) - 25d / 696d * f(x - 1.5d * h) - 225d / 232d * f(x - 0.5d * h) + 225d / 232d * f(x + 0.5d * h) + 25d / 696d * f(x + 1.5d * h) - 9d / 580d * f(x + 2.5d * h)) / h;
        //return (9.0 / 580.0 * f(x - 2.5f * h) - 25.0 / 6960.0 * f(x - 1.5f * h) - 225.0 / 232.0 * f(x - 0.5f * h) + 225.0 / 232.0 * f(x + 0.5f * h) + 25.0 / 696.0 * f(x + 1.5f * h) - 9.0 / 580.0 * f(x + 2.5f * h)) / h;
    }

    private static double df6points1_spline(double h, double x, CubicSpline aa)
    {
        return (9d / 580d * aa.Interpolate(x - 2.5d * h) - 25d / 696d * aa.Interpolate(x - 1.5d * h) - 225d / 232d * aa.Interpolate(x - 0.5d * h) + 225d / 232d * aa.Interpolate(x + 0.5d * h) + 25d / 696d * aa.Interpolate(x + 1.5d * h) - 9d / 580d * aa.Interpolate(x + 2.5d * h)) / h;
    }

    private static class CubicSpline
    {
        SplineTuple[] splines; // Сплайн

        // Структура, описывающая сплайн на каждом сегменте сетки
        private class SplineTuple
        {
            public double a, b, c, d, x;
        }

        // Построение сплайна
        // x - узлы сетки, должны быть упорядочены по возрастанию, кратные узлы запрещены
        // y - значения функции в узлах сетки
        // n - количество узлов сетки
        public void BuildSpline(double[] x, double[] y, int n)
        {
            // Инициализация массива сплайнов
            splines = new SplineTuple[n];
            for (int i = 0; i < n; ++i)
            {
                splines[i] = new SplineTuple();
                splines[i].x = x[i];
                splines[i].a = y[i];
            }
            splines[0].c = splines[n - 1].c = 0.0;

            // Решение СЛАУ относительно коэффициентов сплайнов c[i] методом прогонки для трехдиагональных матриц
            // Вычисление прогоночных коэффициентов - прямой ход метода прогонки
            double[] alpha = new double[n - 1];
            double[] beta = new double[n - 1];
            alpha[0] = beta[0] = 0.0;
            for (int i = 1; i < n - 1; ++i)
            {
                double hi = x[i] - x[i - 1];
                double hi1 = x[i + 1] - x[i];
                double A = hi;
                double C = 2.0 * (hi + hi1);
                double B = hi1;
                double F = 6.0 * ((y[i + 1] - y[i]) / hi1 - (y[i] - y[i - 1]) / hi);
                double z = (A * alpha[i - 1] + C);
                alpha[i] = -B / z;
                beta[i] = (F - A * beta[i - 1]) / z;
            }

            // Нахождение решения - обратный ход метода прогонки
            for (int i = n - 2; i > 0; --i)
            {
                splines[i].c = alpha[i] * splines[i + 1].c + beta[i];
            }

            // По известным коэффициентам c[i] находим значения b[i] и d[i]
            for (int i = n - 1; i > 0; --i)
            {
                double hi = x[i] - x[i - 1];
                splines[i].d = (splines[i].c - splines[i - 1].c) / hi;
                splines[i].b = hi * (2.0 * splines[i].c + splines[i - 1].c) / 6.0 + (y[i] - y[i - 1]) / hi;
            }
        }

        // Вычисление значения интерполированной функции в произвольной точке
        public double Interpolate(double x)
        {
            if (splines == null)
            {
                return Double.NaN; // Если сплайны ещё не построены - возвращаем NaN
            }

            int n = splines.length;
            SplineTuple s;

            if (x <= splines[0].x) // Если x меньше точки сетки x[0] - пользуемся первым эл-тов массива
            {
                s = splines[1];
            }
            else if (x >= splines[n - 1].x) // Если x больше точки сетки x[n - 1] - пользуемся последним эл-том массива
            {
                s = splines[n - 1];
            }
            else // Иначе x лежит между граничными точками сетки - производим бинарный поиск нужного эл-та массива
            {
                int i = 0;
                int j = n - 1;
                while (i + 1 < j)
                {
                    int k = i + (j - i) / 2;
                    if (x <= splines[k].x)
                    {
                        j = k;
                    }
                    else
                    {
                        i = k;
                    }
                }
                s = splines[j];
            }

            double dx = x - s.x;
            // Вычисляем значение сплайна в заданной точке по схеме Горнера (в принципе, "умный" компилятор применил бы схему Горнера сам, но ведь не все так умны, как кажутся)
            return s.a + (s.b + (s.c / 2.0 + s.d * dx / 6.0) * dx) * dx;
        }
    }

}
