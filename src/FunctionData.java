import javax.swing.DefaultListModel;
import javax.swing.JList;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

/* Класс информации о функции.
 * При инициализации объекта автоматически считываются данные о функции с формы.
 */
public class FunctionData {
	private double t0, y0, m1, m2, m3, c1, c2, c0;
	private int n, t1, t2;
	private static ChartPanel panel1, panel2, panel3;
	//private static JFreeChart chart1, chart2, chart3;
	private boolean checkMethod;
	private DefaultListModel<String> model = new DefaultListModel<>();
	private JList<String> logList = new JList<>( model );
	private String func;
	
/*	public FunctionData () {
		n  = Integer.parseInt(Window_GUI.getN_TextField());
		t0 = Double.parseDouble(Window_GUI.getT0_TextField());
		y0 = Double.parseDouble(Window_GUI.getY0_TextField());
		m1 = Double.parseDouble(Window_GUI.getM1_TextField());
		m2 = Double.parseDouble(Window_GUI.getM2_TextField());
		m3 = Double.parseDouble(Window_GUI.getM3_TextField());
		c0 = Double.parseDouble(Window_GUI.getC0_TextField());
		c1 = Double.parseDouble(Window_GUI.getC1_TextField());
		c2 = Double.parseDouble(Window_GUI.getC2_TextField());
		updateTime();
	}*/
	public FunctionData(String[] newInputValues, boolean newCheckBox,  ChartPanel[] panels, JList<String> newLogList) {
		n  = Integer.parseInt(newInputValues[0]);
		t0 = Double.parseDouble(newInputValues[1]);
		y0 = Double.parseDouble(newInputValues[2]);
		m1 = Double.parseDouble(newInputValues[3]);
		m2 = Double.parseDouble(newInputValues[4]);
		m3 = Double.parseDouble(newInputValues[5]);
		c0 = Double.parseDouble(newInputValues[6]);
		c1 = Double.parseDouble(newInputValues[7]);
		c2 = Double.parseDouble(newInputValues[8]);
		checkMethod = newCheckBox;
		panel1 = panels[0];
		panel2 = panels[1];
		panel3 = panels[2];
/*		chart1 = charts[0];
		chart2 = charts[1];
		chart3 = charts[2];*/
		logList = newLogList;
		updateTime();
		func = null;
	}
	
	public FunctionData(String[] newInputValues, boolean newCheckBox, String function, ChartPanel[] panels, JList<String> newLogList) {
		n  = Integer.parseInt(newInputValues[0]);
		t0 = Double.parseDouble(newInputValues[1]);
		y0 = Double.parseDouble(newInputValues[2]);
		m1 = Double.parseDouble(newInputValues[3]);
		m2 = Double.parseDouble(newInputValues[4]);
		m3 = Double.parseDouble(newInputValues[5]);
		c0 = Double.parseDouble(newInputValues[6]);
		c1 = Double.parseDouble(newInputValues[7]);
		c2 = Double.parseDouble(newInputValues[8]);
		checkMethod = newCheckBox;
		panel1 = panels[0];
		panel2 = panels[1];
		panel3 = panels[2];
/*		chart1 = charts[0];
		chart2 = charts[1];
		chart3 = charts[2];*/
		logList = newLogList;
		updateTime();
		func = function.replace(",",".");
	}
	
	public void updateTime() {
		t1 = Integer.parseInt(Window_GUI.getTimeLeft_TextField());
		t2 = Integer.parseInt(Window_GUI.getTimeRight_TextField());		
	}
	public int getN() {
		return n;
	}
	
	public double getT0() {
		return t0;
	}
	
	public double getY0() {
		return y0;
	}
	
	public double getM1() {
		return m1;
	}
	
	public double getM2() {
		return m2;
	}
	
	public double getM3() {
		return m3;
	}
	
	public double getC0() {
		return c0;
	}
	
	public double getC1() {
		return c1;
	}
	
	public double getC2() {
		return c2;
	}
	
	public int getT1() {
		return t1;
	}
	
	public int getT2() {
		return t2;
	}
	
	public boolean getCheckMethod() {
		return checkMethod;
	}
	
	public String getFunc() {
		return func;
	}
	
	public ChartPanel getPanel_1() {
		return panel1;
	}
	
	public ChartPanel getPanel_2() {
		return panel2;
	}
	
	public ChartPanel getPanel_3() {
		return panel3;
	}
	
	
/*	public JFreeChart getPanel_1() {
		return chart1;
	}
	
	public JFreeChart getPanel_2() {
		return chart2;
	}
	
	public JFreeChart getPanel_3() {
		return chart3;
	}*/
	
	public DefaultListModel<String> getLogList() {
		return model;
	}
}

