//===============================================
//          Импортирование библиотек:
//===============================================

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent; // импортирование библиотеки для обработки событий компонентов JButton и JTextField
import java.awt.event.ActionListener; // импортирование библиотеки для реагирования на события компонентов
import java.awt.event.MouseEvent; // импортирование библиотеки для работы с событиями мыши
import java.awt.event.MouseListener; // импортирование библиотеки для прослушивания событий мыши
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton; // импортирование библиотеки для работы с компонентом JButton
import javax.swing.JComboBox; // импортирование библиотеки для работы с компонентом JComboBox
import javax.swing.JFrame; // импортирование библиотеки для работы с компонентом JFrame
import javax.swing.JPanel; // импортирование библиотеки для работы с компонентом JPanel
import javax.swing.JTextField; // импортирование библиотеки для работы с компонентом JTextField
import javax.swing.SwingConstants; // импортирование библиотеки для расположения и ориентации компонентов на экране

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.DefaultXYDataset;

//===============================================
//          Класс формы "Сохранить как":       
//===============================================

public class SaveAsJFrame_Create {
		
	private static JFrame saveAs_JFrame;
	private static JButton okButton_JButton;
	private static JButton cancelButton_JButton;
	private static JPanel fileNamePanel_JPanel;
	private static JPanel fileFormatPanel_JPanel;
	private static JTextField fileNameTextField_JTextField;
	private static JComboBox<String> saveFormatComboBox_JComboBox;
	
/*	private static int isDocx(Object format) {
		if(format==".docx" ? true : false) return 0;
		else if(format==".txt + .png" ? true : false) return 1;
		else return 2;
	}*/
	private static int isDocx(Object format) {
		if(format.equals(".docx")) return 0;
		else if(format.equals(".txt + .png")) return 1;
		else return 2;
	}
	
	public JFrame getSaveAs_JFrame() {
		return saveAs_JFrame;
	}
		
	public void runBlockMainFrame() {
		blockMainFrame();
	}
		
	public SaveAsJFrame_Create() {
		initialize();
	}
		
	private void initialize() {
		//
		// JFrame "сохранить как"
		//
		saveAs_JFrame = new JFrame(); // инициализация элемента saveAs_JFrame типа JFrame
		saveAs_JFrame.setTitle("Сохранить как"); // установка заголовка
		saveAs_JFrame.setBounds(400,200,500,125); // установка расположения и размеров окна
		saveAs_JFrame.setResizable(false);
		//saveAs_JFrame.setAutoRequestFocus(true);
		//saveAs_JFrame.setDefaultLookAndFeelDecorated(true);
		//saveAs_JFrame.pack(); 	
		saveAs_JFrame.setEnabled(true); // установка того, что форма активна
		//saveAs_JFrame.setAlwaysOnTop(true);	// установка расположения формы (всегда поверх остальных окон)
		saveAs_JFrame.setFocusable(true); // установка фокуса на форму
		saveAs_JFrame.addWindowListener(Handlers.SetHandlerJFrameSaveAsExitButton()); // установка обработчика кнопки закрыть
		saveAs_JFrame.addWindowListener(HandlerSaveAsFrameSetFocus);
		//
		// GridBagLayout "режим размещения элементов"
		//
		GridBagLayout gbc_save_as = new GridBagLayout(); // инициализация режима размещения элементов Layout Manager
		gbc_save_as.columnWidths = new int[]{50, 50, 50, 50, 50, 50, 50, 50, 50}; // задание количества и ширины столбцов
		gbc_save_as.rowHeights = new int[]{25,25,25,25}; // задание количества и высоты строк
		gbc_save_as.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0}; // задание параметра автоматического растяжения для каждого столбца
		gbc_save_as.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0}; // задание параметра автоматического растяжения для каждой строки
		saveAs_JFrame.getContentPane().setLayout(gbc_save_as); // установка Layout формы как GridBagLayout
		//
		// JPanel "имя файла"
		//
		fileNamePanel_JPanel = new JPanel();
		GridBagConstraints gbc_file_name_panel = new GridBagConstraints();
		gbc_file_name_panel.insets = new Insets(0, 10, 0, 10);
		gbc_file_name_panel.fill = GridBagConstraints.BOTH;
		gbc_file_name_panel.gridx = 0;
		gbc_file_name_panel.gridy = 1;
		gbc_file_name_panel.gridwidth = 7;
		saveAs_JFrame.getContentPane().add(fileNamePanel_JPanel, gbc_file_name_panel);
		//
		// JTextField "имя файла"
		//
		fileNameTextField_JTextField = new JTextField("Введите имя сохраняемого файла");
		fileNameTextField_JTextField.setFont(new Font("Lucida Grande", Font.PLAIN, 12)); // установка шрифта, типа шрифта и его размера
		fileNameTextField_JTextField.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
		// Создание ограничений для поля:
		GridBagConstraints gbc_file_name = new GridBagConstraints(); // переменная для хранения ограничений
		gbc_file_name.insets = new Insets(0, 20, 0, 10); // установка отступов
		gbc_file_name.anchor = GridBagConstraints.WEST; // установка типа заполнения элементом ячейки
		gbc_file_name.gridx = 0; // установка координаты X, занимаемой ячейки
		gbc_file_name.gridy = 1; // установка координаты Y, занимаемой ячейки
		gbc_file_name.gridwidth = 7;
		fileNameTextField_JTextField.setToolTipText("введите имя файла\n"); // установка сообщения, выводящегося при наведении на элемент
		fileNameTextField_JTextField.setColumns(31); // установка количества колонок в элементе
		fileNameTextField_JTextField.addMouseListener(new mouseClickedTextField());
		fileNamePanel_JPanel.add(fileNameTextField_JTextField, gbc_file_name); // добавление элемента с его ограничениями на форму
		//
		// JPanel "формат файла"
		//
		fileFormatPanel_JPanel = new JPanel();
		GridBagConstraints gbc_file_format_panel = new GridBagConstraints();
		gbc_file_format_panel.insets = new Insets(0, 0, 0, 3);
		gbc_file_format_panel.fill = GridBagConstraints.BOTH;
		gbc_file_format_panel.gridx = 7;
		gbc_file_format_panel.gridy = 1;
		gbc_file_format_panel.gridwidth = 2;
		saveAs_JFrame.getContentPane().add(fileFormatPanel_JPanel, gbc_file_format_panel);
		//
		// JComboBox "формат файла"
		//
		String[] items = { ".docx", ".txt + .png", ".pdf"};
		saveFormatComboBox_JComboBox = new JComboBox<String>(items);
		// Создание ограничений для поля:
		GridBagConstraints gbc_save_format = new GridBagConstraints(); // переменная для хранения ограничений
		gbc_save_format.insets = new Insets(0, 0, 0, 3); // установка отступов
		gbc_save_format.anchor = GridBagConstraints.BOTH; // установка типа заполнения элементом ячейки
		gbc_save_format.gridx = 7; // установка координаты X, занимаемой ячейки
		gbc_save_format.gridy = 1; // установка координаты Y, занимаемой ячейки
		gbc_save_format.gridwidth = 2;
		//saveFormatComboBox_JComboBox.setSize(50, 50); // установка количества колонок в элементе
		saveFormatComboBox_JComboBox.setToolTipText("выберите формат сохраняемого файла\n"); // установка сообщения, выводящегося при наведении на элемент
		saveFormatComboBox_JComboBox.addMouseListener(new mouseClickedTextField());		
		fileFormatPanel_JPanel.add(saveFormatComboBox_JComboBox, gbc_save_format);
		//
		// JButton "cancel"
		//
		cancelButton_JButton = new JButton("Cancel");
		cancelButton_JButton.setFocusable(false);
		cancelButton_JButton.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
		// Создание ограничений для кнопки:
		GridBagConstraints gbc_cancel_button = new GridBagConstraints();
		gbc_cancel_button.anchor = GridBagConstraints.CENTER;
		gbc_cancel_button.fill = GridBagConstraints.BOTH;
		gbc_cancel_button.insets = new Insets(0, 0, 20, 0);
		gbc_cancel_button.gridx = 4;
		gbc_cancel_button.gridy = 3;
		gbc_cancel_button.gridwidth = 3;
		cancelButton_JButton.setToolTipText("нажмите, чтобы отменить операцию сохранения\n"); // установка сообщения, 
																			// выводящегося при 
																			// наведении на элемент
		cancelButton_JButton.addActionListener(new cancelButton());
		cancelButton_JButton.addMouseListener(new mouseClickedCancelButton());
		saveAs_JFrame.getContentPane().add(cancelButton_JButton, gbc_cancel_button);
		//
		// JButton "ок"
		//
		okButton_JButton = new JButton("ОК");
		okButton_JButton.setFocusable(false);
		okButton_JButton.setHorizontalAlignment(SwingConstants.CENTER); // установка расположения по горизонтали
		// Создание ограничений для кнопки:
		GridBagConstraints gbc_ok_button = new GridBagConstraints();
		gbc_ok_button.anchor = GridBagConstraints.CENTER;
		gbc_ok_button.fill = GridBagConstraints.BOTH;
		gbc_ok_button.insets = new Insets(0, 0, 20, 3);
		gbc_ok_button.gridx = 7;
		gbc_ok_button.gridy = 3;
		gbc_ok_button.gridwidth = 2;
		okButton_JButton.setToolTipText("нажмите, чтобы сохранить файл\n"); // установка сообщения, 
																			// выводящегося при 
																			// наведении на элемент
		okButton_JButton.addActionListener(new okButton());
		okButton_JButton.addMouseListener(new mouseClickedOkButton());
		saveAs_JFrame.getContentPane().add(okButton_JButton, gbc_ok_button);
	}
	
	private void blockMainFrame() {
		//frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
		//frame.getRootPane().setFocusable(false);
		//frame.getRootPane().setEnabled(false);
		//frame.getContentPane().setFocusable(false);
		//frame.getContentPane().setEnabled(false);
		Window_GUI.getFrame_JFrame().setEnabled(false);
		//frame.addMouseListener(new CustomListener());
		//Window_GUI.getFrame_JFrame().addMouseMotionListener(Window_GUI.getMyMove());
		Window_GUI.getFrame_JFrame().setFocusableWindowState(false);
		//frame.setUndecorated(true);
	}
	
	// Обработчик кнопки "Ок":											// вернуть полученное имя файла и расширение
	private static class okButton implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			
			if(Window_GUI.getUser_CheckBox()) {
				ReportGenerator.generateReport(fileNameTextField_JTextField.getText(), isDocx(saveFormatComboBox_JComboBox.getSelectedItem()), Window_GUI.UpdateInputData(Window_GUI.getData(), 1), Window_GUI.getCharts());
			}
			else {
				ReportGenerator.generateReport(fileNameTextField_JTextField.getText(), isDocx(saveFormatComboBox_JComboBox.getSelectedItem()), Window_GUI.UpdateInputData(Window_GUI.getData(), 0), Window_GUI.getCharts());
			}
			
			Window_GUI.getFrame_JFrame().setEnabled(true);
			Window_GUI.getFrame_JFrame().setFocusable(true);
			Window_GUI.getFrame_JFrame().setFocusableWindowState(true); 
			saveAs_JFrame.dispose();
			//JOptionPane.showMessageDialog(null, saveFormatComboBox_JComboBox.getSelectedItem(), "Значения параметров и ограничений", JOptionPane.PLAIN_MESSAGE);
			
		}
	} 
	
	// Обработчик кнопки "Cancel":											// закрыть окно
	private static class cancelButton implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			Window_GUI.getFrame_JFrame().setEnabled(true);
			Window_GUI.getFrame_JFrame().setFocusable(true);
			Window_GUI.getFrame_JFrame().setFocusableWindowState(true); 
			saveAs_JFrame.dispose();
			
		}
	} 
	
	private static class mouseClickedOkButton implements MouseListener {
		@Override 
        public void mouseClicked(MouseEvent e) {
			okButton_JButton.setSelected(false);
        }

        public void mouseEntered(MouseEvent e) {
        	okButton_JButton.setSelected(false);
        }

        public void mouseExited(MouseEvent e) {
        	okButton_JButton.setSelected(false);
        }

        public void mousePressed(MouseEvent e) {
        	okButton_JButton.setSelected(true);
        }

        public void mouseReleased(MouseEvent e) {
        	okButton_JButton.setSelected(false);
        }
   }
	private static class mouseClickedCancelButton implements MouseListener {
		@Override 
        public void mouseClicked(MouseEvent e) {
			cancelButton_JButton.setSelected(false);
        }

        public void mouseEntered(MouseEvent e) {
        	cancelButton_JButton.setSelected(false);
        }

        public void mouseExited(MouseEvent e) {
        	cancelButton_JButton.setSelected(false);
        }

        public void mousePressed(MouseEvent e) {
        	cancelButton_JButton.setSelected(true);
        }

        public void mouseReleased(MouseEvent e) {
        	cancelButton_JButton.setSelected(false);
        }
   }
	
	private static class mouseClickedTextField implements MouseListener {
		@Override 
        public void mouseClicked(MouseEvent e) {
        	fileNameTextField_JTextField.setText(null);
        	saveAs_JFrame.addMouseListener(new mouseClickedSaveAsForm());
        }

        public void mouseEntered(MouseEvent e) {
        	fileNameTextField_JTextField.setFocusable(true);
        }

        public void mouseExited(MouseEvent e) {
        	
        }

        public void mousePressed(MouseEvent e) {
        	fileNameTextField_JTextField.setText(null);
        	saveAs_JFrame.addMouseListener(new mouseClickedSaveAsForm());
        }

        public void mouseReleased(MouseEvent e) {
        	
        }
   }
	
	private static class mouseClickedSaveAsForm implements MouseListener {
		 
        public void mouseClicked(MouseEvent e) {
        	if(fileNameTextField_JTextField.getText().isEmpty()) {
        	fileNameTextField_JTextField.setText("Введите имя сохраняемого файла");
        	}
        	fileNameTextField_JTextField.setFocusable(false);
        }

        public void mouseEntered(MouseEvent e) {

        }

        public void mouseExited(MouseEvent e) {

        }

        public void mousePressed(MouseEvent e) {

        }

        public void mouseReleased(MouseEvent e) {

        }
    }	
	
	private static WindowAdapter HandlerSaveAsFrameSetFocus = new WindowAdapter() {
		@Override
		public void windowOpened(WindowEvent event) {
			saveAs_JFrame.setVisible(true);
			saveAs_JFrame.requestFocusInWindow();
		}
	};
   
}
