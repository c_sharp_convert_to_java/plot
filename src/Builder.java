import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.DefaultXYDataset;

public class Builder {
/*
    public int n;

    public double M0;
    public double M1;
    public double M2;
    public double M3;

    public int model;

    public static final int PASTUH = 1;
    public static final int LIDER = 0;

    public double[] X;
    public double[][] Y1, Ypr1, Ypr2;

    public JFreeChart chart1;
    public JFreeChart chart2;
    public JFreeChart chart3;*/

    public static void build(int n, double M1, double M2, double M3, boolean checkBox, double[] X, 
    		double [][] Y1, double [][] Ypr1, double [][] Ypr2, ChartPanel panels[] ) {
        JFreeChart chart1;
        JFreeChart chart2;
        JFreeChart chart3;
        
        double[][][] spl = new double[n][][];


        for (int i = 0; i < n; i++)
        {
            spl[i] = new double[][] { X, Y1[i] };
        }

        DefaultXYDataset dataset = new DefaultXYDataset();

        if (checkBox)
        {
            dataset.addSeries("lider", spl[0]);
            dataset.addSeries("outsider1", spl[1]);

            for (int i = 2; i < n; i++)
            {
                dataset.addSeries("outsider" + i, spl[i]);
            }
        }
        else
        {
            dataset.addSeries("pastuh", spl[0]);
            dataset.addSeries("1", spl[1]);

            for (int i = 2; i < n; i++)
            {
                dataset.addSeries(Integer.toString(i), spl[i]);
            }
        }

        chart1 = ChartFactory.createXYLineChart("", "Время","Координата", dataset);

        DefaultXYDataset dataset2 = new DefaultXYDataset();

        double[] aM0 = new double[Ypr1[0].length];
        double[] aM1 = new double[Ypr1[0].length];
        for (int i = 0; i < Ypr1[0].length; i++)
        {
            aM1[i] = M1;
            aM0[i] = 0;
        }

        double[][][] splpr = new double[n][][];
        double[][] pM0 = new double[][] {X, aM0};
        double[][] pM1 = new double[][] {X, aM1};
        for (int i = 0; i < n; i++)
        {
            splpr[i] = new double[][]{X, Ypr1[i]};
        }

        if (checkBox)
        {
            dataset2.addSeries("M0", pM0);
            dataset2.addSeries("M1", pM1);

            for (int i = 0; i < n; i++)
            {
                dataset2.addSeries(Integer.toString(i), splpr[i]);
                //myPane2.AddCurve(i.ToString(), splpr[i], Color.Blue, SymbolType.Circle).Line.Width = 2.0f;
            }
        }
        else
        {
            dataset2.addSeries("M0", pM0);
            dataset2.addSeries("M1", pM1);

            for (int i = 0; i < n - 1; i++)
            {
                dataset2.addSeries(Integer.toString(i), splpr[i]);
            }
        }

        chart2 = ChartFactory.createXYLineChart("", "Время","Скорость", dataset2);

        double[] aM2 = new double[Ypr2[0].length];
        double[] aM3 = new double[Ypr2[0].length];
        for (int i = 0; i < Ypr2[0].length; i++)
        {
            aM2[i] = M2;
            aM3[i] = M3;
        }
        double[][] pM2 = new double[][]{X, aM2};
        double[][] pM3 = new double[][]{X, aM3};

        double[][][] splpr2 = new double[n][][];
        for (int i = 0; i < n; i++)
        {
            if (X == null)
                throw new RuntimeException();

            if (Ypr2[i] == null)
                throw new RuntimeException(Integer.toString(i));
            splpr2[i] = new double[][]{X, Ypr2[i]};
        }
        DefaultXYDataset dataset3 = new DefaultXYDataset();

        if (checkBox)
        {
            dataset3.addSeries("M2", pM2);
            dataset3.addSeries("M3", pM3);

            for (int i = 0; i < n; i++)
            {
                dataset3.addSeries("outsider" + i, splpr2[i]);
                //myPane3.AddCurve("outsider" + i, splpr2[i], Color.Blue, SymbolType.Circle).Line.Width = 2.0f;
            }
        }
        else
        {
            dataset3.addSeries("M2", pM2);
            dataset3.addSeries("M3", pM3);

            for (int i = 0; i < n; i++)
            {
                dataset3.addSeries(Integer.toString(i), splpr2[i]);
            }
        }
        chart3 = ChartFactory.createXYLineChart("", "Время","Ускорение", dataset3);
        
		if (panels[0] ==null) 
			panels[0] = new ChartPanel(chart1);
		
		else 
			panels[0].setChart(chart1);
		
		//panels[0].setBounds(200, 10,700,250);
			
		if (panels[1] ==null) 
			panels[1] = new ChartPanel(chart2);
		
		else 
			panels[1].setChart(chart2);
		
		//panels[1].setBounds(200, 270,700,140);
		
		if (panels[2] ==null) 
			panels[2] = new ChartPanel(chart3);
		
		else 
			panels[2].setChart(chart3);
		
		//panels[2].setBounds(200, 420,700,140);
		
    }
}
