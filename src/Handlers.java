

//import java.awt.EventQueue; // для обработки событий элементов 
//import javax.swing.JFrame; // для работы с формой
//import java.awt.Dimension;

import java.awt.GridBagConstraints;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.EventListener;
//import java.util.regex.Matcher;  
import java.util.regex.Pattern; // для создания шаблонов для проверки 
								// строки на соответствие этому шаблону

//import javax.swing.JComboBox;
//import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.event.MenuListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.MenuEvent;

//import javax.swing.JRadioButton;

//import javax.swing.JToolBar;

//import com.jgoodies.forms.factories.DefaultComponentFactory;
//import javax.swing.border.BevelBorder;


//import Window_GUI.CustomListener;
//import Window_GUI.MyMove;




public class Handlers {
	
	private static JFrame frame = Window_GUI.getFrame_JFrame();
	private static FunctionData data;
	private static boolean stateButton = false;
	
//=================================================================
//         Раздел обработчиков для элементов меню "Файл":
//=================================================================

  //==========================================================
  //             Обработчик элемента "О программе":
  //==========================================================
	
		private static class HandlerMenuItemShowAboutButton implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			String message = "Курсовой проект по языку Java";
			message += "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
			message += "Версия 1.1";
			JOptionPane.showMessageDialog(null, message, "О программе", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
  //==========================================================
  //                Обработчик элемента "Выход":
  //==========================================================
	
		private static class HandlerMenuItemExitButton implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			System.exit(0);
		}
	}
	
//=================================================================
//         Раздел обработчиков для элементов меню "Отчёт":
//=================================================================
			
		private static class HandlerMenuReportButton implements MenuListener {

		    @Override
		    public void menuSelected(MenuEvent e) {
				//if(Window_GUI.getMenuReport_JMenu().isSelected()) {
					if(/*!completenessСheck() ||*/ !stateButton) updateStatesMenuItems(1);
					else {updateStatesMenuItems(0);}
				//}
		    }

		    @Override
		    public void menuDeselected(MenuEvent e) {
		        // to do...
		    }

		    @Override
		    public void menuCanceled(MenuEvent e) {
		        // to do...
		    }
		}
		
		private static void updateStatesMenuItems(int param) {
			switch(param) {
				case 0:
					Window_GUI.getSave_JMenuItem().setEnabled(true);
					Window_GUI.getSaveAs_JMenuItem().setEnabled(true);
					Window_GUI.getPrint_JMenuItem().setEnabled(true);
					break;
				case 1:
					Window_GUI.getSave_JMenuItem().setEnabled(false);
					Window_GUI.getSaveAs_JMenuItem().setEnabled(false);
					Window_GUI.getPrint_JMenuItem().setEnabled(false);
					break;
			}
		}
		
		/*private static boolean isButtonPress(boolean param) {
			return (param==true) ? true : false;
		}*/
				
		
  //========================================================== 
  //              Обработчик элемента "Показать":
  //==========================================================
	
		private static class HandlerMenuItemShowButton implements ActionListener { // сделать, чтобы пока не закрыт диалог, фокус был только на нём
			@Override
			public void actionPerformed(ActionEvent event) {
				JFrame showDialog_report = new JFrame(); // инициализация элемента showDialog типа
				//JDialog showDialog_report = new JDialog(frame, "",true); // инициализация элемента showDialog типа 
				//showDialog.addMouseListener(new CustomListener()); // установка обработчика событий мыши
				showDialog_report.setTitle("Демонстарция отчёта"); // задание заголовка окна
				showDialog_report.setBounds(350,100,600,500); // установка расположения и размеров окна
				showDialog_report.setVisible(true);
				showDialog_report.setEnabled(true); // установка "активации" окна
				//showDialog_report.setAlwaysOnTop(true); // установка расположения "всегда поверх остальных окон"
				showDialog_report.addWindowListener(JFrameExitButton); // установка обработчика событий окна
				//showDialog_report.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
				showDialog_report.setResizable(false);
				showDialog_report.setFocusable(true);
				//showDialog_report.setAutoRequestFocus(true);
				
				frame.getContentPane().setFocusable(false);
				frame.getRootPane().setFocusable(false);
				frame.getRootPane().setEnabled(false);    //отключает работу любых обработчиков frame
				frame.getContentPane().setEnabled(false);
				
				//frame.addMouseListener(new CustomListener());
				//frame.addMouseMotionListener(new MyMove());
				
				frame.setFocusableWindowState(false);
				frame.setEnabled(false);	//отключает работу любых обработчиков frame
				frame.setFocusable(false);
				frame.setFocusableWindowState(false);
				frame.setUndecorated(true);
				    
			}
		}
		
		// Обработчик кнопки "закрыть" дочернего окна "Показать":	
		private static WindowAdapter JFrameExitButton = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {
				frame.setEnabled(true);
				frame.setFocusable(true);
				frame.setFocusableWindowState(true);  
			}
		};
	
  //==========================================================
  //             Обработчик элемента "Сохранить":
  //==========================================================

		private static class HandlerMenuItemSaveButton implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent event) {
				
				ReportGenerator.generateReport(0, data);
			}
		}
		
  //==========================================================
  //           Обработчик элемента "Сохранить как":
  //==========================================================
		
		private static class HandlerMenuItemSaveAsButton implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent event) {
				try {
					SaveAsJFrame_Create saveAsJFrame = new SaveAsJFrame_Create();
					saveAsJFrame.getSaveAs_JFrame().setVisible(true);
					saveAsJFrame.runBlockMainFrame();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		// Обработчик кнопки "закрыть" дочернего окна "Сохранить как":	
		private static WindowAdapter JFrameSaveAsExitButton = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {
				frame.setEnabled(true);
				frame.setFocusable(true);
				frame.setFocusableWindowState(true);  
			}
		};
	
  //==========================================================
  //               Обработчик элемента "Печать":
  //==========================================================
		
		private static class HandlerMenuItemPrintButton implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent event) {
				ReportGenerator.generateReport(1, data);
				
			}
		} 
		
//=================================================================
//      Раздел обработчика для меню "Пользовательская функция":
//=================================================================
			
		private static class HandlerMenuUserFunctionButton implements MenuListener {

		    @Override
		    public void menuSelected(MenuEvent event) {
				try {
					UserFunctionJFrame_Create inputUserFunction_JFrame = new UserFunctionJFrame_Create();
					//inputUserFunction_JFrame.getInputUserFunction_JFrame().setVisible(true);
					inputUserFunction_JFrame.runBlockMainFrame();
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }

		    @Override
		    public void menuDeselected(MenuEvent e) {
		        // to do...
		    }

		    @Override
		    public void menuCanceled(MenuEvent e) {
		        // to do...
		    }
		}
		
		private static class HandlerMouseClickedMenuUserFunction implements MouseListener {
			@Override 
	        public void mouseClicked(MouseEvent e) {
				Window_GUI.getMenuUserFunction_JMenu().setFocusable(false); // чтобы не был выделен постоянно элемент бара.setSelected(false);
	        }

	        public void mouseEntered(MouseEvent e) {
	        	Window_GUI.getMenuUserFunction_JMenu().setFocusable(false); // чтобы не был выделен постоянно элемент бара.setSelected(false);
	        }

	        public void mouseExited(MouseEvent e) {
	        	Window_GUI.getMenuUserFunction_JMenu().setFocusable(false); // чтобы не был выделен постоянно элемент бара.setSelected(false);
	        }

	        public void mousePressed(MouseEvent e) {
	        	Window_GUI.getMenuUserFunction_JMenu().setFocusable(false); // чтобы не был выделен постоянно элемент бара.setSelected(true);
	        }

	        public void mouseReleased(MouseEvent e) {
	        	Window_GUI.getMenuUserFunction_JMenu().setFocusable(false); // чтобы не был выделен постоянно элемент бара.setSelected(false);
	        }
	    }
		
		// Обработчик кнопки "закрыть" дочернего окна "Пользовательская функция":	
		private static WindowAdapter JFrameUserFunctionExitButton = new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {
				frame.setEnabled(true);
				frame.setFocusable(true);
				frame.setFocusableWindowState(true);  
			}
		};
		
//=================================================================
//         Раздел обработчиков для элементов полей формы:
//=================================================================
		
  //==========================================================
  //               Обработчик полей "c1" и "c2":
  //==========================================================
		
		/*private static class HandlerInputAndChangeC1TextField implements DocumentListener {
			@Override

	        public void insertUpdate(DocumentEvent e) {
				if(!(Double.parseDouble(Window_GUI.getC1_TextField()) > 0  
						&& Double.parseDouble(Window_GUI.getC1_TextField()) < 1)) {
					String message  = "Внимание!\nПроверьте правильность заполнения c1.";
					JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
				}
	        } 
	        @Override
	        public void removeUpdate(DocumentEvent e) {
	               
	        }
	 
	        @Override
	        public void changedUpdate(DocumentEvent e) {
				if(!(Double.parseDouble(Window_GUI.getC1_TextField()) > 0  
						&& Double.parseDouble(Window_GUI.getC1_TextField()) < 1)) {
					String message  = "Внимание!\nПроверьте правильность заполнения c1.";
					JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
				}
            }
		
		}*/
		
//=================================================================
//              Раздел обработчиков для кнопок формы:
//=================================================================
	
  //==========================================================
  //          Обработчик кнопки "Анимация/Остановить":
  //==========================================================
		
		private static class HandlerAnimationAndStopButton implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent event) {
				if(Window_GUI.getAnimationAndStop_Button().getText() == "Анимация") {
					if(Window_GUI.getUser_CheckBox())
					{
						if(!completenessСheck()) {
							String message  = "Внимание!\nПроверьте правильность введённых данных.";
							JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
							//stateButton = false;
							//Window_GUI.getShow_JMenuItem().setEnabled(false);
							//Window_GUI.getSave_JMenuItem().setEnabled(false);
							//Window_GUI.getSaveAs_JMenuItem().setEnabled(false);
							//Window_GUI.getPrint_JMenuItem().setEnabled(false);
						}
						else {
							Window_GUI.getAnimationAndStop_Button().setText("Остановить");
							updateGbcTimePanel(1);
							data = Window_GUI.UpdateInputData(Window_GUI.getData(), 1);
							Animation.startAnimation(data);
							stateButton = true;
							//Window_GUI.getShow_JMenuItem().setEnabled(true);
							//Window_GUI.getSave_JMenuItem().setEnabled(true);
							//Window_GUI.getSaveAs_JMenuItem().setEnabled(true);
							//Window_GUI.getPrint_JMenuItem().setEnabled(true);
						}
					}
					else
					{
						if(!completenessСheck()) {
							String message  = "Внимание!\nПроверьте правильность введённых данных.";
							JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
							//stateButton = false;
							//Window_GUI.getShow_JMenuItem().setEnabled(false);
							//Window_GUI.getSave_JMenuItem().setEnabled(false);
							//Window_GUI.getSaveAs_JMenuItem().setEnabled(false);
							//Window_GUI.getPrint_JMenuItem().setEnabled(false);
						}
						else {
							Window_GUI.getAnimationAndStop_Button().setText("Остановить");
							updateGbcTimePanel(1);
							data = Window_GUI.UpdateInputData(Window_GUI.getData(), 0);
							Animation.startAnimation(data);
							stateButton = true;
							//Window_GUI.getShow_JMenuItem().setEnabled(true);
							//Window_GUI.getSave_JMenuItem().setEnabled(true);
							//Window_GUI.getSaveAs_JMenuItem().setEnabled(true);
							//Window_GUI.getPrint_JMenuItem().setEnabled(true);
						}
					}
				}
				else {
					Window_GUI.getAnimationAndStop_Button().setText("Анимация");
					updateGbcTimePanel(0);
					Animation.stopAnimation();
				}
			}
		}

		// Обработчик смены расположения кнопки для сохранения целостности интерфейса:
		private static void updateGbcTimePanel(int param) {
			GridBagConstraints gbc = new GridBagConstraints();
			if(Window_GUI.getOS().equals("Mac")) {
				switch(param) {
				case 0: 
					gbc.insets = new Insets(0, 9, 0, 5);
					//setAnimationAndStopButtonParamLocation(gbc);
					//frame.getContentPane().add(Window_GUI.getTimePanel(), gbc);
					break;
				case 1:
					gbc.insets = new Insets(0, 17, 0, 5);
					//setAnimationAndStopButtonParamLocation(gbc);
					//frame.getContentPane().add(Window_GUI.getTimePanel(), gbc);
					//frame.getRootPane().add(Window_GUI.getTimePanel(), Window_GUI.getGbcTimePanel());
				
					break;
				}
			}
			
			if(Window_GUI.getOS().equals("Win")) {
				switch(param) {
				case 0: 
					gbc.insets = new Insets(0, 9, 0, 5);
					//frame.getContentPane().add(Window_GUI.getTimePanel(), Window_GUI.getGbcTimePanel());
					break;
				case 1:
					gbc.insets = new Insets(0, 18, 0, 5);
					//frame.getContentPane().add(Window_GUI.getTimePanel(),Window_GUI.getGbcTimePanel());
					break;
				}
			}
			setAnimationAndStopButtonParamLocation(gbc);
			frame.getContentPane().add(Window_GUI.getTimePanel(), gbc);
		}
		
		// Метод для обработчика смены расположения кнопки
		private static void setAnimationAndStopButtonParamLocation(GridBagConstraints gbc) {
			gbc.anchor = GridBagConstraints.EAST;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.gridx = 0;
			gbc.gridy = 12;
		}
		
  //==========================================================
  //              Обработчик кнопки "Построить":
  //==========================================================
	
	private static class HandlerPlotButton implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			if(Window_GUI.getUser_CheckBox())
			{
				if(!completenessСheck())
				{
					String message  = "Внимание!\nПроверьте правильность введённых данных.";
					JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
					//stateButton = false;
					//Window_GUI.getShow_JMenuItem().setEnabled(false);
					//Window_GUI.getSave_JMenuItem().setEnabled(false);
					//Window_GUI.getSaveAs_JMenuItem().setEnabled(false);
					//Window_GUI.getPrint_JMenuItem().setEnabled(false);
				}
				else
				{
					data = Window_GUI.UpdateInputData(Window_GUI.getData(), 1);
					Calc.process(data);
					stateButton = true;
					//Window_GUI.getShow_JMenuItem().setEnabled(true);
					//Window_GUI.getSave_JMenuItem().setEnabled(true);
					//Window_GUI.getSaveAs_JMenuItem().setEnabled(true);
					//Window_GUI.getPrint_JMenuItem().setEnabled(true);
					//JOptionPane.showMessageDialog(null, getN_TextField(), "Значения параметров и ограничений", JOptionPane.PLAIN_MESSAGE);
				}				
			}
			else
			{
				if(!completenessСheck())
				{
					String message  = "Внимание!\nПроверьте правильность введённых данных.";
					JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
					//stateButton = false;
					//Window_GUI.getShow_JMenuItem().setEnabled(false);
					//Window_GUI.getSave_JMenuItem().setEnabled(false);
					//Window_GUI.getSaveAs_JMenuItem().setEnabled(false);
					//Window_GUI.getPrint_JMenuItem().setEnabled(false);
				}
				else
				{
					data = Window_GUI.UpdateInputData(Window_GUI.getData(), 0);
					Calc.process(data);
					stateButton = true;
					//Window_GUI.getShow_JMenuItem().setEnabled(true);
					//Window_GUI.getSave_JMenuItem().setEnabled(true);
					//Window_GUI.getSaveAs_JMenuItem().setEnabled(true);
					//Window_GUI.getPrint_JMenuItem().setEnabled(true);
					//JOptionPane.showMessageDialog(null, getN_TextField(), "Значения параметров и ограничений", JOptionPane.PLAIN_MESSAGE);
				}
			}
		}
	}
	
  //==========================================================
  //           Общий метод для обработчиков кнопок 
  //          "Анимация/Остановить" и "Построить":
  //==========================================================
	
	private static boolean completenessСheck() {
		if(Window_GUI.getN_TextField().isEmpty()
				&& Window_GUI.getT0_TextField().isEmpty()
				&& Window_GUI.getY0_TextField().isEmpty()
				&& Window_GUI.getM1_TextField().isEmpty()
				&& Window_GUI.getM2_TextField().isEmpty()
				&& Window_GUI.getM3_TextField().isEmpty()
				&& Window_GUI.getC0_TextField().isEmpty()
				&& Window_GUI.getC1_TextField().isEmpty()
				&& Window_GUI.getC2_TextField().isEmpty()
				&& Window_GUI.getTimeLeft_TextField().isEmpty()
				&& Window_GUI.getTimeRight_TextField().isEmpty())
		{
			return false;
		}
		else if(!(Pattern.compile("(\\d+)").matcher(Window_GUI.getN_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getT0_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getY0_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getM1_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getM2_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getM3_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getC0_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getC1_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getC2_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getTimeLeft_TextField()).find()
			&& Pattern.compile("(\\d+)").matcher(Window_GUI.getTimeRight_TextField()).find()))
			//Pattern.compile("(\\d+)|(\\.)").matcher(Window_GUI.getTimeRight_TextField()).find()))
		{
			return false;
		}
		else if( !(Pattern.compile("(\\d+)").matcher(Window_GUI.getN_TextField()).matches()))
		{
			String message  = "Внимание!\nПроверьте правильность заполнения N.";
			JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		else if(!((Double.parseDouble(Window_GUI.getC1_TextField()) > 0  
				&& Double.parseDouble(Window_GUI.getC1_TextField()) < 1)
				&& (Double.parseDouble(Window_GUI.getC2_TextField()) > 0  
						&& Double.parseDouble(Window_GUI.getC2_TextField()) < 1))) {
			String message  = "Внимание!\nПроверьте правильность заполнения c1 и с2.";
			JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		else if(!(Pattern.compile("(\\d+)|(\\-\\d+)").matcher(Window_GUI.getTimeLeft_TextField()).matches()) || !(Pattern.compile("(\\d+)").matcher(Window_GUI.getTimeRight_TextField()).matches())
				|| !(Integer.parseInt(Window_GUI.getTimeLeft_TextField()) < Integer.parseInt(Window_GUI.getTimeRight_TextField()))) {
				String message  = "Внимание!\nПроверьте правильность заполнения раздела \"Время\".";
				JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
				return false;
		}
		else if(UserFunctionJFrame_Create.getUserFunction().isEmpty() && Window_GUI.getUser_CheckBox()) {
			String message  = "Внимание!\nВведите функцию.";
			JOptionPane.showMessageDialog(null, message, "Ошибка", JOptionPane.ERROR_MESSAGE);
			return false;
		}
		else
		{
			return true;
		}
	}
	
//=================================================================
//=================================================================
	

	
//================================================================
// Раздел методов, возвращающих экземпляры обработчиков объектов:
//================================================================
	
 //==========================================================
 //               Подраздел для меню "Отчёт":
 //==========================================================
	
	public static MenuListener SetHandlerMenuReportButton() {
		return new HandlerMenuReportButton();
	}
	
 //==========================================================
 //       Подраздел для меню "Пользовательская функция":
 //==========================================================
	public static MenuListener SetHandlerMenuUserFunctionButton() {
		return new HandlerMenuUserFunctionButton();
	}
	public static MouseListener SetHandlerMouseClickedMenuUserFunction() {
		return new HandlerMouseClickedMenuUserFunction();
	}
	public static WindowAdapter SetHandlerJFrameUserFunctionExitButton() {
		return JFrameUserFunctionExitButton;
	}
	
 //==========================================================
 //           Подраздел для элементов меню "Файл":
 //==========================================================
	
	public static ActionListener SetHandlerMenuItemShowAboutButton() {
		return new HandlerMenuItemShowAboutButton();
	}
	public static ActionListener SetHandlerMenuItemExitButton() {
		return new HandlerMenuItemExitButton();
	}

 //==========================================================
 //           Подраздел для элементов меню "Отчёт":
 //==========================================================
		
	public static ActionListener SetHandlerMenuItemShowButton() {
		return new HandlerMenuItemShowButton();
	}
	
	public static ActionListener SetHandlerMenuItemSaveButton() {
		return new HandlerMenuItemSaveButton();
	}
	
	public static ActionListener SetHandlerMenuItemSaveAsButton() {
		return new HandlerMenuItemSaveAsButton();
	}
	public static WindowAdapter SetHandlerJFrameSaveAsExitButton() {
		return JFrameSaveAsExitButton;
	}
	
	public static ActionListener SetHandlerMenuItemPrintButton() {
		return new HandlerMenuItemPrintButton();
	}
	
 //==========================================================
 //           Подраздел для элементов полей формы:
 //==========================================================
 
	/*public static DocumentListener SetHandlerInputAndChangeC1TextField() {
		return new HandlerInputAndChangeC1TextField();
	}*/
	
 //==========================================================
 //               Подраздел для кнопок формы:
 //==========================================================

	public static ActionListener SetHandlerAnimationAndStop() {
		return new HandlerAnimationAndStopButton();
	}
	
	public static ActionListener SetHandlerPlotButton() {
		return new HandlerPlotButton();
	}
	
//==========================================================================
//==========================================================================
	
}






///////////////////////////////////////////////////////////////////////////////////////////////////////////////

//showDialog.addWindowListener(new WindowAdapter() {
//	@Override
//	public void windowClosing(WindowEvent e) {
//		frame.setEnabled(true);
//		frame.setFocusable(true);
//		frame.setFocusableWindowState(true);
//	}
	
//});   

/*
//Обработчик мыши главного окна:	
	private static class MyMove implements MouseMotionListener {
			@Override
			public void mouseMoved(MouseEvent event) {
				// to do...
			}
			public void mouseDragged(MouseEvent event) {
			frame.setCursor(Cursor.getPredefinedCursor (Cursor.WAIT_CURSOR));
			frame.repaint();			
		}
	};
	

		
	private static class CustomListener implements MouseListener {
		 
     public void mouseClicked(MouseEvent e) {
     	
     	frame.setEnabled(true);
			frame.setFocusable(true);
			frame.setFocusableWindowState(true);
			frame.setUndecorated(false);
			
     }

     public void mouseEntered(MouseEvent e) {}

     public void mouseExited(MouseEvent e) {}

     public void mousePressed(MouseEvent e) {}

     public void mouseReleased(MouseEvent e) {}
}
*/
