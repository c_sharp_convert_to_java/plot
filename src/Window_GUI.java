

//import java.awt.EventQueue; // для обработки событий элементов 
//import javax.swing.JFrame; // для работы с формой
import java.awt.Color;
import java.awt.Component;
//import java.awt.Component.*;
import java.awt.Cursor;
import java.awt.Dimension;
//import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
//import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
//import java.awt.event.WindowListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
//import java.util.regex.Matcher;  
import java.util.regex.Pattern; // для создания шаблонов для проверки 
								// строки на соответствие этому шаблону
//import report_generator.ReportGenerator;

import javax.swing.AbstractListModel;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
//import javax.swing.JComboBox;
//import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
//import javax.swing.JRadioButton;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
//import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
//import com.jgoodies.forms.factories.DefaultComponentFactory;
//import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;


import org.jfree.chart.*;
import org.jfree.data.xy.DefaultXYDataset;
import java.awt.FlowLayout;



public class Window_GUI {
	
	private static String os;
	
	private static ChartPanel firstPanel_ChartPanel, secondPanel_ChartPanel, thirdPanel_ChartPanel;
	private static ChartPanel[] panels;
    private static JFreeChart firstChart_JFreeChart, secondChart_JFreeChart, thirdChart_JFreeChart;
    private static JFreeChart[] charts;
    private static FunctionData data;
    private static String[] values;
    private static JList<String> logList_JList;
    private static GridBagConstraints gbc_time_panel;
	
    //==============================================

	private static JFrame frame; // создание элемента frame типа JFrame
	private static GridBagLayout gridBagLayout; // создание режима размещения элементов Layout Manager
	
	
	// "Блок" создания элементов типа JPanel:
	
	private JPanel modelsPanel_JPanel; // создание панели для объединения checkBox элементов
	private JPanel numberParticles_JPanel; // создание панели для объединения элементов раздела "число частиц"
	private JPanel entryConditions_JPanel; // создание панели для объединения элементов раздела "входные данные"
	private JPanel restrictions_JPanel; // создание панели для объединения раздела "ограничения"
	private JPanel coefficients_JPanel; // создание панели для объединения раздела "коэффициенты"
	private static JPanel timePanel_JPanel;
	private JPanel userPanel_JPanel;

	
	// "Блок" создания элементов типа JCheckBox:
	
	private static JCheckBox leader_CheckBox; // метод следования за лидером
	private static JCheckBox shepherd_CheckBox; // метод пастуха
	private static JCheckBox user_CheckBox; // пользовательская функция
	
	
	private final ButtonGroup modelsGroup_ButtonGroup = new ButtonGroup(); // создание группы
    																	   // modelsGroup_ButtonGroup типа ButtonGroup
	
	// "Блок" создания элементов типа JSeparator:
	
	private JSeparator separatorTop_Separator; // создание элемента "разделительная черта"
	private JSeparator separatorBottom_Separator; // создание элемента "разделительная черта"
	
	
	// "Блок" создания элементов типа Component (распорки):
	
	private Component numberParticles_strut_first; // создание распорки "число частиц"
	private Component numberParticles_strut_second; // создание распорки "число частиц"
	private Component numberParticles_strut_third; // создание распорки "число частиц"
	private Component entry_conditions_strut_first; // создание распорки "начальные условия"
	private Component entry_conditions_strut_second; // создание распорки "начальные условия"
	private Component entry_conditions_strut_third; // создание распорки "начальные условия"
	private Component entry_conditions_strut_fourth; // создание распорки "начальные условия"
	private Component entry_conditions_strut_fifth; // создание распорки "начальные условия"
	private Component entry_conditions_strut_sixth; // создание распорки "начальные условия"
	private Component restrictions_strut_first; // создание распорки "ограничения"
	private Component restrictions_strut_second; // создание распорки "ограничения"
	private Component restrictions_strut_third; // создание распорки "ограничения"
	private Component restrictions_strut_fourth; // создание распорки "ограничения"
	private Component restrictions_strut_fifth; // создание распорки "ограничения"
	private Component restrictions_strut_sixth; // создание распорки "ограничения"
	private Component restrictions_strut_seventh; // создание распорки "ограничения"
	private Component restrictions_strut_eighth; // создание распорки "ограничения"
	private Component restrictions_strut_ninth; // создание распорки "ограничения"
	private Component coefficients_strut_first; // создание распорки "коэффициенты"
	private Component coefficients_strut_second; // создание распорки "коэффициенты"
	private Component coefficients_strut_third; // создание распорки "коэффициенты"
	private Component coefficients_strut_fourth; // создание распорки "коэффициенты"
	private Component coefficients_strut_fifth; // создание распорки "коэффициенты"
	private Component coefficients_strut_sixth; // создание распорки "коэффициенты"
	private Component coefficients_strut_seventh; // создание распорки "коэффициенты"
	private Component coefficients_strut_eighth; // создание распорки "коэффициенты"
	private Component coefficients_strut_ninth; // создание распорки "коэффициенты"
	
	
	// "Блок" создания элементов типа JLabel 
	
	private JLabel numberParticles_JLabel; // создание элемента numberParticles_JLabel типа JLabel
	private JLabel nNumberParticlesLabel_JLabel; // создание элемента nNumberParticlesLabel_JLabel типа JLabel
	private JLabel entryConditions_JLabel; // создание элемента entryConditions_JLabel типа JLabel
	private JLabel t0EntryConditions_JLabel; // создание элемента c0EntryConditions_JLabel типа JLabel
	private JLabel y0EntryConditions_JLabel; // создание элемента y0EntryConditions_JLabel типа JLabel
	private JLabel restrictions_JLabel; // создание элемента restrictions_JLabel типа JLabel
	private JLabel m1Restrictions_JLabel; // создание элемента mRestrictions_JLabel типа JLabel
	private JLabel m2Restrictions_JLabel; // создание элемента mRestrictions_JLabel типа JLabel
	private JLabel m3Restrictions_JLabel; // создание элемента mRestrictions_JLabel типа JLabel
	private JLabel coefficients_JLabel; // создание элемента coefficients_JLabel типа JLabel
	private JLabel с0Сoefficients_JLabel; // создание элемента с0Сoefficients_JLabel типа JLabel
	private JLabel с1Сoefficients_JLabel; // создание элемента с1Сoefficients_JLabel типа JLabel
	private JLabel с2Сoefficients_JLabel; // создание элемента с2Сoefficients_JLabel типа JLabel
	private JLabel time_JLabel; // создание элемента time_JLabel типа JLabel
	private JLabel logLabel;
	
	
	// "Блок" создания элементов типа JTextField:
	
	private static JTextField n_TextField; // N
	private static JTextField t0_TextField; // T0
	private static JTextField y0_TextField; // Y0
	private static JTextField m1_TextField; // M1
	private static JTextField m2_TextField; // M2
	private static JTextField m3_TextField; // M3
	private static JTextField c0_TextField; // C0
	private static JTextField c1_TextField; // C1
	private static JTextField c2_TextField; // C2
	private static JTextField timeLeft_TextField; // левое поле времени
	private static JTextField timeRight_TextField; // правое поле времени
	
	
	// "Блок" создания элементов типа JButton:
	private static JButton animationAndStopButton_JButton; // создание элемента animationAndStopButton_JButton типа JButton
	private static JButton clearButton_JButton;
	private static JButton plotButton_JButton;
	
	public static JButton getPlotButton() {
		return plotButton_JButton;
	}

	
	private JScrollPane scrollPane;
	
	//private JProgressBar progressBar_JProgressBar;
	
	private JMenuBar menuBar_JMenuBar;
	
	private static JMenu menuFile_JMenu;
	private static JMenu menuReport_JMenu;
	private static JMenu menuUserFunction_JMenu;
	
	private static JMenuItem aboutProgram_JMenuItem;
	private static JMenuItem exitProgram_JMenuItem;
	private static JMenuItem show_JMenuItem;
	private static JMenuItem save_JMenuItem;
	private static JMenuItem saveAs_JMenuItem;
	private static JMenuItem print_JMenuItem;
	
	public static JMenu getMenuReport_JMenu() {
		return menuReport_JMenu;
	}
	
	public static JMenuItem getShow_JMenuItem() {
		return show_JMenuItem;
	}
	public static JMenuItem getSave_JMenuItem() {
		return save_JMenuItem;
	}
	public static JMenuItem getSaveAs_JMenuItem() {
		return saveAs_JMenuItem;
	}
	public static JMenuItem getPrint_JMenuItem() {
		return print_JMenuItem;
	}

	
	public static JMenu getMenuUserFunction_JMenu() {
		return menuUserFunction_JMenu;
	}
	
	
	// "Блок" создания методов для получения значений 
	// всех переменных типа JTextField:
	
	public static String getN_TextField() { // получить значение N
		return n_TextField.getText();
	}
	public static String getT0_TextField() { // получить значение T0
		return t0_TextField.getText();
	}
	public static String getY0_TextField() { // получить значение Y0
		return y0_TextField.getText();
	}
	public static String getM1_TextField() { // получиьт значение M1
		return m1_TextField.getText();
	}
	public static String getM2_TextField() { // получить значение M2
		return m2_TextField.getText();
	}
	public static String getM3_TextField() { // получить значение M3
		return m3_TextField.getText();
	}
	public static String getC0_TextField() { // получиьт значение C0
		return c0_TextField.getText();
	}
	public static String getC1_TextField() { // получить значение C1
		return c1_TextField.getText();
	}
	public static String getC2_TextField() { // получить значение C2
		return c2_TextField.getText();
	}
	public static String getTimeLeft_TextField() { // получить значение левого поля времени
		return timeLeft_TextField.getText();
	}
	public static String getTimeRight_TextField() { // получить значение правого поля времени
		return timeRight_TextField.getText();
	}
	public static void setTimeLeft_TextField(String newValue) {
		timeLeft_TextField.setText(newValue);
	}
	public static void setTimeRight_TextField(String newValue) {
		timeRight_TextField.setText(newValue);
	}
	
	
	// "Блок" создания методов для получения значений 
	// всех переменных типа JCheckBox:
	
	//public static boolean getLeader_CheckBox() { // выбран или нет метод следования за лидером
	//	return leader_CheckBox.isSelected();
	//}
	//public static boolean getShepherd_CheckBox() { // выбран или нет метод пастуха
	//	return shepherd_CheckBox.isSelected();
	//}
	public static boolean getMethod_CheckBox() {
		return leader_CheckBox.isSelected() == true ? true : false;
	}
	
	public static boolean getUser_CheckBox() {
		return user_CheckBox.isSelected() == true ? true : false;
	}
	
	
	public static JButton getAnimationAndStop_Button() {
		return animationAndStopButton_JButton;
	}
	
	public static JFrame getFrame_JFrame() {
		return frame;
	}
	
	//public static WindowAdapter getSaveAsWindowAdapter() {
		//return saveAs_WindowAdapter;
	//}
	
	//public static MyMove getMyMove() {
		//return new MyMove();
	//}
	

	
	public static JFreeChart[] getCharts() {
		return charts;
	}
	
	public static FunctionData getFunctionData() {
		return data;
	}
	
	public static JPanel getTimePanel() {
		return timePanel_JPanel;
	}
	
	public static GridBagConstraints getGbcTimePanel() {
		return gbc_time_panel;
	}
	
	public static String[] UpdateInputValues(String[] values) {
		values = new String[]{
			getN_TextField(),
			getT0_TextField(),
			getY0_TextField(),
			getM1_TextField(),
			getM2_TextField(),
			getM3_TextField(),
			getC0_TextField(),
			getC1_TextField(),
			getC2_TextField()
		};
		return values;
	}
	
	public static FunctionData UpdateInputData(FunctionData data, int param) {
		switch(param) {
			case 0:
		
				data = new FunctionData(UpdateInputValues(values), getMethod_CheckBox(), panels, logList_JList);
				break;
			case 1:
				data = new FunctionData(UpdateInputValues(values), getMethod_CheckBox(), UserFunctionJFrame_Create.getUserFunction(), panels, logList_JList);
				break;
		}
		return data;
	}
	
	/*public static String[] getValues() {
		return values;
	}*/
	public static FunctionData getData() {
		return data;
	}

	public static String getOS() {
		return os;
	}


	// Запуск приложения
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window_GUI window  = new Window_GUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	

	// Создание приложения
	
	public Window_GUI() {
		initialize();
	}

	// Инициализация компонентов формы:
	
	private void initialize() {
		
		os = System.getProperty("os.name").substring(0, 3);
		if(os.equals("Mac")) {
			
			frame = MacOsParam.FrameParameters(frame, gridBagLayout);
			
			modelsPanel_JPanel = MacOsParam.ModelsPanelParameters(frame, modelsPanel_JPanel);
			leader_CheckBox = MacOsParam.LeaderCheckBoxParameters(modelsPanel_JPanel, leader_CheckBox, modelsGroup_ButtonGroup);
			shepherd_CheckBox = MacOsParam.ShepherdCheckBoxParameters(modelsPanel_JPanel, shepherd_CheckBox, modelsGroup_ButtonGroup);
			userPanel_JPanel = MacOsParam.UserPanelParameters(frame, userPanel_JPanel);
			user_CheckBox = MacOsParam.UserCheckBoxParameters(userPanel_JPanel, user_CheckBox /*modelsGroup_ButtonGroup*/);
			
			separatorTop_Separator = MacOsParam.SeparatorTopParam(frame, separatorTop_Separator);
			
			numberParticles_JLabel = MacOsParam.NumberParticlesLabelParameters(frame, numberParticles_JLabel);
			numberParticles_JPanel = MacOsParam.NumberParticlesPanelParameters(frame, numberParticles_JPanel);
			nNumberParticlesLabel_JLabel = MacOsParam.NNumberParticlesLabelParameters(numberParticles_JPanel, nNumberParticlesLabel_JLabel);
			n_TextField = MacOsParam.NTextFieldParameters(numberParticles_JPanel, n_TextField);
			numberParticles_strut_first = MacOsParam.NumberParticlesStrutFirstParameters(numberParticles_JPanel, numberParticles_strut_first);
			numberParticles_strut_second = MacOsParam.NumberParticlesStrutSecondParameters(numberParticles_JPanel, numberParticles_strut_second);
			numberParticles_strut_third = MacOsParam.NumberParticlesStrutThirdParameters(numberParticles_JPanel, numberParticles_strut_third);
			
			entryConditions_JLabel = MacOsParam.EntryConditionsLabelParameters(frame, entryConditions_JLabel);
			entryConditions_JPanel = MacOsParam.EntryConditionsPanelParameters(frame, entryConditions_JPanel);
			t0EntryConditions_JLabel = MacOsParam.T0EntryConditionsLabelParameters(entryConditions_JPanel, t0EntryConditions_JLabel);
			t0_TextField = MacOsParam.T0EntryConditionsTextFieldParameters(entryConditions_JPanel, t0_TextField);
			entry_conditions_strut_first = MacOsParam.EntryConditionsStrutFirstParameters(entryConditions_JPanel, entry_conditions_strut_first);
			entry_conditions_strut_second = MacOsParam.EntryConditionsStrutSecondParameters(entryConditions_JPanel, entry_conditions_strut_second);
			entry_conditions_strut_third = MacOsParam.EntryConditionsStrutThirdParameters(entryConditions_JPanel, entry_conditions_strut_third);
			y0EntryConditions_JLabel = MacOsParam.Y0EntryConditionsLabelParameters(entryConditions_JPanel, y0EntryConditions_JLabel);
			y0_TextField = MacOsParam.Y0EntryConditionsTextFieldParameters(entryConditions_JPanel, y0_TextField);
			entry_conditions_strut_fourth = MacOsParam.EntryConditionsStrutFourthParameters(entryConditions_JPanel, entry_conditions_strut_fourth);
			entry_conditions_strut_fifth = MacOsParam.EntryConditionsStrutFifthParameters(entryConditions_JPanel, entry_conditions_strut_fifth);
			entry_conditions_strut_sixth = MacOsParam.EntryConditionsStrutSixthParameters(entryConditions_JPanel, entry_conditions_strut_sixth);
			
			restrictions_JLabel = MacOsParam.RestrictionsLabelParameters(frame, m1Restrictions_JLabel);
			restrictions_JPanel = MacOsParam.RestrictionsPanelParameters(frame, restrictions_JPanel);
			m1Restrictions_JLabel = MacOsParam.M1RestrictionsLabelParameters(restrictions_JPanel, m1Restrictions_JLabel);
			m1_TextField = MacOsParam.M1RestrictionsTextFieldParameters(restrictions_JPanel, m1_TextField);
			restrictions_strut_first = MacOsParam.RestrictionsStrutFirstParameters(restrictions_JPanel, restrictions_strut_first);
			restrictions_strut_second = MacOsParam.RestrictionsStrutSecondParameters(restrictions_JPanel, restrictions_strut_second);
			restrictions_strut_third = MacOsParam.RestrictionsStrutThirdParameters(restrictions_JPanel, restrictions_strut_third);
			m2Restrictions_JLabel = MacOsParam.M2RestrictionsLabelParameters(restrictions_JPanel, m2Restrictions_JLabel);
			m2_TextField = MacOsParam.M2RestrictionsTextFieldParameters(restrictions_JPanel, m2_TextField);
			restrictions_strut_fourth = MacOsParam.RestrictionsStrutFourthParameters(restrictions_JPanel, restrictions_strut_fourth);
			restrictions_strut_fifth = MacOsParam.RestrictionsStrutFifthParameters(restrictions_JPanel, restrictions_strut_fifth);
			restrictions_strut_sixth = MacOsParam.RestrictionsStrutSixthParameters(restrictions_JPanel, restrictions_strut_sixth);
			m3Restrictions_JLabel = MacOsParam.M3RestrictionsLabelParameters(restrictions_JPanel, m3Restrictions_JLabel);
			m3_TextField = MacOsParam.M3RestrictionsTextFieldParameters(restrictions_JPanel, m3_TextField);
			restrictions_strut_seventh = MacOsParam.RestrictionsStrutSeventhParameters(restrictions_JPanel, restrictions_strut_seventh);
			restrictions_strut_eighth = MacOsParam.RestrictionsStrutEighthParameters(restrictions_JPanel, restrictions_strut_eighth);
			restrictions_strut_ninth = MacOsParam.RestrictionsStrutNinthParameters(restrictions_JPanel, restrictions_strut_ninth);
			
			coefficients_JLabel = MacOsParam.CoefficientsLabelParameters(frame, coefficients_JLabel);
			coefficients_JPanel = MacOsParam.CoefficientsPanelParameters(frame, coefficients_JPanel);
			с0Сoefficients_JLabel = MacOsParam.C0СoefficientsLabelParameters(coefficients_JPanel, с0Сoefficients_JLabel);
			c0_TextField = MacOsParam.C0СoefficientsTextFieldParameters(coefficients_JPanel, c0_TextField);
			coefficients_strut_first = MacOsParam.CoefficientsStrutFirstParameters(coefficients_JPanel, coefficients_strut_first);
			coefficients_strut_second = MacOsParam.CoefficientsStrutSecondParameters(coefficients_JPanel, coefficients_strut_second);
			coefficients_strut_third = MacOsParam.CoefficientsStrutThirdParameters(coefficients_JPanel, coefficients_strut_third);
			с1Сoefficients_JLabel = MacOsParam.C1СoefficientsLabelParameters(coefficients_JPanel, с1Сoefficients_JLabel);
			c1_TextField = MacOsParam.C1CoefficientsTextFieldParameters(coefficients_JPanel, c1_TextField);
			coefficients_strut_fourth = MacOsParam.CoefficientsStrutFourthParameters(coefficients_JPanel, coefficients_strut_fourth);
			coefficients_strut_fifth = MacOsParam.CoefficientsStrutFifthParameters(coefficients_JPanel, coefficients_strut_fifth);
			coefficients_strut_sixth = MacOsParam.CoefficientsStrutSixthParameters(coefficients_JPanel, coefficients_strut_sixth);
			с2Сoefficients_JLabel = MacOsParam.C2СoefficientsLabelParameters(coefficients_JPanel, с2Сoefficients_JLabel);
			c2_TextField = MacOsParam.C2CoefficientsTextFieldParameters(coefficients_JPanel, c2_TextField);
			coefficients_strut_seventh = MacOsParam.CoefficientsStrutSeventhParameters(coefficients_JPanel, coefficients_strut_seventh);
			coefficients_strut_eighth = MacOsParam.CoefficientsStrutEighthParameters(coefficients_JPanel, coefficients_strut_eighth);
			coefficients_strut_ninth = MacOsParam.CoefficientsStrutNinthParameters(coefficients_JPanel, coefficients_strut_ninth);
			
			time_JLabel = MacOsParam.TimeLabelParameters(frame, time_JLabel);
			timePanel_JPanel = MacOsParam.TimePanelParameters(frame, timePanel_JPanel, gbc_time_panel);
			timeLeft_TextField = MacOsParam.TimeLeftTextFieldParameters(timePanel_JPanel, timeLeft_TextField);
			timeRight_TextField = MacOsParam.TimeRightTextFieldParameters(timePanel_JPanel, timeRight_TextField);
			animationAndStopButton_JButton = MacOsParam.AnimationAndStopButtonParameters(timePanel_JPanel, animationAndStopButton_JButton);
			
			separatorBottom_Separator = MacOsParam.SeparatorButtomParameters(frame, separatorBottom_Separator);
			
			logLabel = MacOsParam.LogLabelParameters(frame, logLabel);
			scrollPane = MacOsParam.ScrollPaneParameters(frame, scrollPane, logList_JList);
			//list = MacOsParam.ListParameters(scrollPane, list);
			
			clearButton_JButton = MacOsParam.ClearButtonParameters(frame, clearButton_JButton);
			
			plotButton_JButton = MacOsParam.PlotButtonParameters(frame, plotButton_JButton);
			
			//progressBar_JProgressBar = MacOsParam.ProgressBarParameters(frame, progressBar_JProgressBar);
			
			menuBar_JMenuBar = MacOsParam.MenuBarParameters(frame, menuBar_JMenuBar);
			menuFile_JMenu = MacOsParam.MenuFileParameters(menuBar_JMenuBar, menuFile_JMenu);
			aboutProgram_JMenuItem = MacOsParam.MenuItemAboutProgramParameters(menuFile_JMenu, aboutProgram_JMenuItem);
			exitProgram_JMenuItem = MacOsParam.MenuItemExitProgramParameters(menuFile_JMenu, exitProgram_JMenuItem);
			
			menuReport_JMenu = MacOsParam.MenuReportParameters(menuBar_JMenuBar, menuReport_JMenu);
			save_JMenuItem = MacOsParam.MenuItemSaveParameters(menuReport_JMenu, save_JMenuItem);
			saveAs_JMenuItem = MacOsParam.MenuItemSaveAsParameters(menuReport_JMenu, saveAs_JMenuItem);
			print_JMenuItem = MacOsParam.MenuItemPrintParameters(menuReport_JMenu, print_JMenuItem);
			
			menuUserFunction_JMenu = MacOsParam.MenuUserFunctionParameters(menuBar_JMenuBar, menuUserFunction_JMenu);
			
			firstPanel_ChartPanel = MacOsParam.FirstPanelParameters(frame, firstPanel_ChartPanel, firstChart_JFreeChart);
			secondPanel_ChartPanel = MacOsParam.SecondPanelParameters(frame, secondPanel_ChartPanel, secondChart_JFreeChart);
			thirdPanel_ChartPanel = MacOsParam.ThirdPanelParameters(frame, thirdPanel_ChartPanel, thirdChart_JFreeChart);
			
			//values = MacOsParam.InputValuesPrameters(values); // вынести в этот файл, а в файлах параметров использовать
			
			//data = MacOsParam.InputDataParameters(data);
			
			panels = MacOsParam.InputPanelsParameters(panels, firstPanel_ChartPanel, secondPanel_ChartPanel, thirdPanel_ChartPanel);
			
		}
		
		if(os.equals("Win")) {
			
			frame = WindowsParam.FrameParameters(frame, gridBagLayout);
			
			modelsPanel_JPanel = WindowsParam.ModelsPanelParameters(frame, modelsPanel_JPanel);
			leader_CheckBox = WindowsParam.LeaderCheckBoxParameters(modelsPanel_JPanel, leader_CheckBox, modelsGroup_ButtonGroup);
			shepherd_CheckBox = WindowsParam.ShepherdCheckBoxParameters(modelsPanel_JPanel, shepherd_CheckBox, modelsGroup_ButtonGroup);
			userPanel_JPanel =  WindowsParam.UserPanelParameters(frame, userPanel_JPanel);
			user_CheckBox =  WindowsParam.UserCheckBoxParameters(userPanel_JPanel, user_CheckBox /*modelsGroup_ButtonGroup*/);
			
			
			separatorTop_Separator = WindowsParam.SeparatorTopParam(frame, separatorTop_Separator);
			
			numberParticles_JLabel = WindowsParam.NumberParticlesLabelParameters(frame, numberParticles_JLabel);
			numberParticles_JPanel = WindowsParam.NumberParticlesPanelParameters(frame, numberParticles_JPanel);
			nNumberParticlesLabel_JLabel = WindowsParam.NNumberParticlesLabelParameters(numberParticles_JPanel, nNumberParticlesLabel_JLabel);
			n_TextField = WindowsParam.NTextFieldParameters(numberParticles_JPanel, n_TextField);
			numberParticles_strut_first = WindowsParam.NumberParticlesStrutFirstParameters(numberParticles_JPanel, numberParticles_strut_first);
			numberParticles_strut_second = WindowsParam.NumberParticlesStrutSecondParameters(numberParticles_JPanel, numberParticles_strut_second);
			numberParticles_strut_third = WindowsParam.NumberParticlesStrutThirdParameters(numberParticles_JPanel, numberParticles_strut_third);
			
			entryConditions_JLabel = WindowsParam.EntryConditionsLabelParameters(frame, entryConditions_JLabel);
			entryConditions_JPanel = WindowsParam.EntryConditionsPanelParameters(frame, entryConditions_JPanel);
			t0EntryConditions_JLabel = WindowsParam.T0EntryConditionsLabelParameters(entryConditions_JPanel, t0EntryConditions_JLabel);
			t0_TextField = WindowsParam.T0EntryConditionsTextFieldParameters(entryConditions_JPanel, t0_TextField);
			entry_conditions_strut_first = WindowsParam.EntryConditionsStrutFirstParameters(entryConditions_JPanel, entry_conditions_strut_first);
			entry_conditions_strut_second = WindowsParam.EntryConditionsStrutSecondParameters(entryConditions_JPanel, entry_conditions_strut_second);
			entry_conditions_strut_third = WindowsParam.EntryConditionsStrutThirdParameters(entryConditions_JPanel, entry_conditions_strut_third);
			y0EntryConditions_JLabel = WindowsParam.Y0EntryConditionsLabelParameters(entryConditions_JPanel, y0EntryConditions_JLabel);
			y0_TextField = WindowsParam.Y0EntryConditionsTextFieldParameters(entryConditions_JPanel, y0_TextField);
			entry_conditions_strut_fourth = WindowsParam.EntryConditionsStrutFourthParameters(entryConditions_JPanel, entry_conditions_strut_fourth);
			entry_conditions_strut_fifth = WindowsParam.EntryConditionsStrutFifthParameters(entryConditions_JPanel, entry_conditions_strut_fifth);
			entry_conditions_strut_sixth = WindowsParam.EntryConditionsStrutSixthParameters(entryConditions_JPanel, entry_conditions_strut_sixth);
			
			restrictions_JLabel = WindowsParam.RestrictionsLabelParameters(frame, m1Restrictions_JLabel);
			restrictions_JPanel = WindowsParam.RestrictionsPanelParameters(frame, restrictions_JPanel);
			m1Restrictions_JLabel = WindowsParam.M1RestrictionsLabelParameters(restrictions_JPanel, m1Restrictions_JLabel);
			m1_TextField = WindowsParam.M1RestrictionsTextFieldParameters(restrictions_JPanel, m1_TextField);
			restrictions_strut_first = WindowsParam.RestrictionsStrutFirstParameters(restrictions_JPanel, restrictions_strut_first);
			restrictions_strut_second = WindowsParam.RestrictionsStrutSecondParameters(restrictions_JPanel, restrictions_strut_second);
			restrictions_strut_third = WindowsParam.RestrictionsStrutThirdParameters(restrictions_JPanel, restrictions_strut_third);
			m2Restrictions_JLabel = WindowsParam.M2RestrictionsLabelParameters(restrictions_JPanel, m2Restrictions_JLabel);
			m2_TextField = WindowsParam.M2RestrictionsTextFieldParameters(restrictions_JPanel, m2_TextField);
			restrictions_strut_fourth = WindowsParam.RestrictionsStrutFourthParameters(restrictions_JPanel, restrictions_strut_fourth);
			restrictions_strut_fifth = WindowsParam.RestrictionsStrutFifthParameters(restrictions_JPanel, restrictions_strut_fifth);
			restrictions_strut_sixth = WindowsParam.RestrictionsStrutSixthParameters(restrictions_JPanel, restrictions_strut_sixth);
			m3Restrictions_JLabel = WindowsParam.M3RestrictionsLabelParameters(restrictions_JPanel, m3Restrictions_JLabel);
			m3_TextField = WindowsParam.M3RestrictionsTextFieldParameters(restrictions_JPanel, m3_TextField);
			restrictions_strut_seventh = WindowsParam.RestrictionsStrutSeventhParameters(restrictions_JPanel, restrictions_strut_seventh);
			restrictions_strut_eighth = WindowsParam.RestrictionsStrutEighthParameters(restrictions_JPanel, restrictions_strut_eighth);
			restrictions_strut_ninth = WindowsParam.RestrictionsStrutNinthParameters(restrictions_JPanel, restrictions_strut_ninth);
			
			coefficients_JLabel = WindowsParam.CoefficientsLabelParameters(frame, coefficients_JLabel);
			coefficients_JPanel = WindowsParam.CoefficientsPanelParameters(frame, coefficients_JPanel);
			с0Сoefficients_JLabel = WindowsParam.C0СoefficientsLabelParameters(coefficients_JPanel, с0Сoefficients_JLabel);
			c0_TextField = WindowsParam.C0СoefficientsTextFieldParameters(coefficients_JPanel, c0_TextField);
			coefficients_strut_first = WindowsParam.CoefficientsStrutFirstParameters(coefficients_JPanel, coefficients_strut_first);
			coefficients_strut_second = WindowsParam.CoefficientsStrutSecondParameters(coefficients_JPanel, coefficients_strut_second);
			coefficients_strut_third = WindowsParam.CoefficientsStrutThirdParameters(coefficients_JPanel, coefficients_strut_third);
			с1Сoefficients_JLabel = WindowsParam.C1СoefficientsLabelParameters(coefficients_JPanel, с1Сoefficients_JLabel);
			c1_TextField = WindowsParam.C1CoefficientsTextFieldParameters(coefficients_JPanel, c1_TextField);
			coefficients_strut_fourth = WindowsParam.CoefficientsStrutFourthParameters(coefficients_JPanel, coefficients_strut_fourth);
			coefficients_strut_fifth = WindowsParam.CoefficientsStrutFifthParameters(coefficients_JPanel, coefficients_strut_fifth);
			coefficients_strut_sixth = WindowsParam.CoefficientsStrutSixthParameters(coefficients_JPanel, coefficients_strut_sixth);
			с2Сoefficients_JLabel = WindowsParam.C2СoefficientsLabelParameters(coefficients_JPanel, с2Сoefficients_JLabel);
			c2_TextField = WindowsParam.C2CoefficientsTextFieldParameters(coefficients_JPanel, c2_TextField);
			coefficients_strut_seventh = WindowsParam.CoefficientsStrutSeventhParameters(coefficients_JPanel, coefficients_strut_seventh);
			coefficients_strut_eighth = WindowsParam.CoefficientsStrutEighthParameters(coefficients_JPanel, coefficients_strut_eighth);
			coefficients_strut_ninth = WindowsParam.CoefficientsStrutNinthParameters(coefficients_JPanel, coefficients_strut_ninth);
			
			time_JLabel = WindowsParam.TimeLabelParameters(frame, time_JLabel);
			timePanel_JPanel = WindowsParam.TimePanelParameters(frame, timePanel_JPanel, gbc_time_panel);
			timeLeft_TextField = WindowsParam.TimeLeftTextFieldParameters(timePanel_JPanel, timeLeft_TextField);
			timeRight_TextField = WindowsParam.TimeRightTextFieldParameters(timePanel_JPanel, timeRight_TextField);
			animationAndStopButton_JButton = WindowsParam.AnimationAndStopButtonParameters(timePanel_JPanel, animationAndStopButton_JButton);
			
			separatorBottom_Separator =WindowsParam.SeparatorButtomParameters(frame, separatorBottom_Separator);
			
			logLabel = WindowsParam.LogLabelParameters(frame, logLabel);
			scrollPane = WindowsParam.ScrollPaneParameters(frame, scrollPane, logList_JList);
			//list = WindowsParam.ListParameters(scrollPane, list);
			
			clearButton_JButton = WindowsParam.ClearButtonParameters(frame, clearButton_JButton);
			
			plotButton_JButton = WindowsParam.PlotButtonParameters(frame, plotButton_JButton);
			
			//progressBar_JProgressBar = WindowsParam.ProgressBarParameters(frame, progressBar_JProgressBar);
			
			menuBar_JMenuBar = WindowsParam.MenuBarParameters(frame, menuBar_JMenuBar);
			menuFile_JMenu = WindowsParam.MenuFileParameters(menuBar_JMenuBar, menuFile_JMenu);
			aboutProgram_JMenuItem = WindowsParam.MenuItemAboutProgramParameters(menuFile_JMenu, aboutProgram_JMenuItem);
			exitProgram_JMenuItem = WindowsParam.MenuItemExitProgramParameters(menuFile_JMenu, exitProgram_JMenuItem);
			
			menuReport_JMenu = WindowsParam.MenuReportParameters(menuBar_JMenuBar, menuReport_JMenu);
			save_JMenuItem = WindowsParam.MenuItemSaveParameters(menuReport_JMenu, save_JMenuItem);
			saveAs_JMenuItem = WindowsParam.MenuItemSaveAsParameters(menuReport_JMenu, saveAs_JMenuItem);
			print_JMenuItem = WindowsParam.MenuItemPrintParameters(menuReport_JMenu, print_JMenuItem);
			
			menuUserFunction_JMenu = WindowsParam.MenuUserFunctionParameters(menuBar_JMenuBar, menuUserFunction_JMenu);
			
			firstPanel_ChartPanel = WindowsParam.FirstPanelParameters(frame, firstPanel_ChartPanel, firstChart_JFreeChart);
			secondPanel_ChartPanel = WindowsParam.SecondPanelParameters(frame, secondPanel_ChartPanel, secondChart_JFreeChart);
			thirdPanel_ChartPanel = WindowsParam.ThirdPanelParameters(frame, thirdPanel_ChartPanel, thirdChart_JFreeChart);
			
			//values = WindowsParam.InputValuesPrameters(values); // вынести в этот файл, а в файлах параметров использовать
			
			//data = WindowsParam.InputDataParameters(data);
			
			panels = WindowsParam.InputPanelsParameters(panels, firstPanel_ChartPanel, secondPanel_ChartPanel, thirdPanel_ChartPanel);
		}
		
		
		
		
		
	
	    
	}
}

