import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/* Класс, использующийся для получения текущего системного времени.
 * Возвращает строку, хранящую информацию о времени.
 */
public class CurrentTime {
    public static String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        Date date=cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String formattedDate=dateFormat.format(date);
        return (formattedDate);
    }
}
